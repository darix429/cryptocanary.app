<?php
session_start();
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
if (isset($_SESSION['user_id'])) {
	$user_id = filter_var($_SESSION['user_id'], FILTER_SANITIZE_NUMBER_INT);
	$username = filter_var($_SESSION['username'], FILTER_SANITIZE_STRING);
	$userEmail = filter_var($_SESSION['userEmail'], FILTER_SANITIZE_STRING);
	$avatar = '/images/avatars/'.$user_id.'/'.$_SESSION['avatar'].'.jpg';
	$chirps = $_SESSION['chirps'];
} else {
	$user_id = $username = $avatar = $chirps = '';
}
$message = filter_input(INPUT_GET,'message',FILTER_SANITIZE_STRING);
