<?php
require_once('init.php');
require('../db.php');
try {
	$stmt = $conn->prepare('SELECT COUNT(product_review.product_id), product_review.product_id,product_name, short_desc, ecosystem, entity_category.category, product.logo, product.approved_flag FROM product_review
INNER JOIN product
ON product_review.product_id = product.product_id
INNER JOIN entity_category
ON product.category = entity_category.entity_category_id
WHERE product_review.update_time >= (CURDATE() - INTERVAL 50 DAY)
AND product.approved_flag = 1
GROUP BY product_id
ORDER BY product_id DESC
LIMIT 10');
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$dataIndex = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
// Total Amount of product we have on our platform
try {
	$stmt1 = $conn->prepare('SELECT COUNT(`product_id`) FROM product WHERE product.approved_flag = 1');
	$stmt1->execute();
	$stmt1->setFetchMode(PDO::FETCH_NUM);
	$productCount = $stmt1->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
// Total amount of product submitted in the last 7 days
try {
	$stmt2 = $conn->prepare('SELECT COUNT(`product_id`) FROM product WHERE create_time >= DATE(NOW()) - INTERVAL 7 DAY AND product.approved_flag = 1');
	$stmt2->execute();
	$stmt2->setFetchMode(PDO::FETCH_NUM);
	$sevenDaysCount = $stmt2->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$conn = null;
// SEO Title and description
$title = 'Discover Crypto Projects, Products, And Services | CryptoCanary Discover';
$description = 'Community recommended crypto products that you can actually use. Discover coins, DApps, DeFi protocols, blockchain games, mining devices, wallets, etc...';
$img = "https://cryptocanary.app/images/Cryptocanary-logo-v2.png";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php');?>
</head>
<body style="background-color: #fff;">
	<?phpif (strpos(__DIR__,'/var/www/html/') !== false) { include('/var/www/html/inc/googleTag.php');} ?>
	<div id="Map">
		<?php
	// Banner to inform user about the chirp system. Deactivated for now..
	// if (isset($_SESSION['user_id']) AND $_SESSION['user_id'] >0) {echo '<div id="chirp" class="text-center" style="background-color:#df8822; padding: 5px">Introducing Chirps - earn rewards for your contribution! <a href="/chirp/">Learn More...</a></div>';}
		include('nav.php');
		?>
		<div id="intro">
			<div class="container">
				<div class="row">
					<div class="col-12 col-md-8 pt-5">
						<h1 style="font-weight:800; font-size: 3rem;">Discover<br><u><span id="word"></span></u></h1>
						<p class="pt-3" style="font-size: 1.5rem;"><?php echo $productCount['0']['0']; ?> community recommended crypto products that you can actually use.</p>
						<div class="pt-3 pb-3">
							<p>
								<?php
								if ($user_id<1) {
									echo '<a href="/signup/" class="btn btn-lg btn-primary">Join Now</a> ';
								}
								?>
								<a href="browse.php" class="btn btn-lg btn-outline-dark">Browse Products</a></p>
							</div>
						</div>
						<div class="col-12 col-md-4 pt-5">
							<p><img src="/img/heroRocketIcon.svg" class="img-fluid" alt="Image"></p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="7days" style="background-color: #e6e4e4; padding: 50px 0px;">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<h4 class="pt-3 pb-3">We've had <?php echo $sevenDaysCount['0']['0']; ?> new products added in the past week</h4>
						<p><a href="browse.php" class="btn btn-primary">Discover Them</a></p>
					</div>
				</div>
			</div>
		</div>
		<div id="latest">
			<div class="container pt-5 pb-5">
				<div class="latest">
					<h2>Active Products:</h2>
					<div class="row">
						<div class="col-lg-9 pb-4">
							<table class="table table-striped">
								<thead>
									<tr>
										<th>#</th><th>Name</th><th>Category</th><th>Ecosystem</th>
									</tr>
								</thead>
								<tbody>
									<?php
									foreach ($dataIndex as $key => $rowIndex) {
										$reviewAmount = $rowIndex["COUNT(product_review.product_id)"];
										$product_id = $rowIndex['product_id'];
										$product_name = $rowIndex['product_name'];
										$category = $rowIndex['category'];
										$ecosystem = $rowIndex['ecosystem'];
										$short_desc = $rowIndex['short_desc'];
										$logo = '/images/products/'.$product_id.'/'.$rowIndex['logo'];
										echo '<tr><td><a href="/discover/'.urlencode($product_name).'"><img src="'.$logo.'" class="img-fluid" style="max-height: 60px;" alt="'.$product_name.'"></a></td><td><a href="/discover/'.urlencode($product_name).'">'.$product_name.'</a><br><small class="hidden-md-down">'.$short_desc.'</small></td><td>'.$category.'</td><td>'.$ecosystem.'</td></tr>';
									}
									?>
								</tbody>
							</table>
							<p><a href="browse.php" class="btn btn-outline-primary">See more...</a></p>
						</div>
						<div class="col-lg-3">
							<div class="pb-5">
								<form action="/newsletter/" method="POST" role="form">
									<h4>Don't miss out</h4>
									<p>Get cool product recs and special discounts delivered straight to your inbox!</p>
									<div class="form-group">
										<input type="email" name="email" id="email" class="form-control" placeholder="Your email address" required="required" minlenght="5">
									</div>
									<button type="submit" class="btn btn-block btn-primary">Sign Me Up!</button>
								</form>
							</div>
							<form action="/add/" method="POST" role="form">
								<h4>Are we missing anything?</h4>
								<a href="/add/" class="btn btn-block btn-primary">Add New Product</a>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div id="why" style="background-color: #e6e4e4;">
				<div class="container">
					<h2 class="text-center">Why join CryptoCanary?</h2>
					<div class="row text-center">
						<div class="col-md-4 p-3">
							<div class="border border-secondary rounded p-3" style="background-color: #fff">
								<p class="text-center"><img src="/img/discoverCoolStuff.svg" class="img-fluid" alt="Image" style="max-height: 200px;"></p>
								<p>Discover cool products from our up-to-date product feed</p>
							</div>
						</div>
						<div class="col-md-4 p-3">
							<div class="border border-secondary rounded p-3" style="background-color: #fff">
								<p class="text-center"><img src="/img/staySafe.svg" class="img-fluid" alt="Image" style="max-height: 200px;"></p>
								<p>Stay safe by reading community-driven reviews</p>
							</div>
						</div>
						<div class="col-md-4 p-3">
							<div class="border border-secondary rounded p-3" style="background-color: #fff">
								<p class="text-center"><img src="/img/saveTime.svg" class="img-fluid" alt="Image" style="max-height: 200px;"></p>
								<p>Save time by signing up for our weekly newsletter</p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12 text-center">
							<p><a href="/signup/" class="btn btn-lg btn-primary">Join Now</a></p>
						</div>
					</div>
				</div>
			</div>
			<div id="saying">
				<div class="container">
					<div class="row" style="align-items: center">
						<div class="col-md-6 col-lg-5 text-center">
							<h2 class="warmWelcome">A warm welcome from one of our co-founders</h2>
						</div>
						<div class="col-md-6 col-lg-7">
							<div class="embed-responsive embed-responsive-16by9">
								<iframe class="embed-responsive-item" src="https://player.vimeo.com/video/381241135"></iframe>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php include('footer.php'); ?>
		<?php include('../inc/endScripts.php'); ?>
		<script>
			$(document).ready(()=>{
				var texts = ["crypto debit cards", "DApps", "blockchain games", "mining devices", "crypto podcasts","DeFi protocols", "hardware wallets", "exchanges", "trading bots","portfolio trackers","all things crypto"];
				var running = false;
				var roll = (t,i)=>{
					running = true;
					$("#word").html(texts[i]).hide().fadeIn(t, ()=>{
						i++;
						if(i != texts.length) {
							roll(t/1.2, i);
						} else {
							running = false;
						}
					});
				}
				$(".roll-span").click(()=>{
					if (running == false)
						roll(2000, 0);
				});
				roll(1000, 0);
			});
		</script>
		<script type="text/javascript">
			$('.kv-rtl-theme-svg-star2').rating({
				hoverOnClear: false,
				theme: 'krajee-svg',
				showClear: false,
				disabled: true,
				readonly: true,
				showCaption: false
			});
		</script>
	</body>
	</html>
