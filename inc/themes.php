<?php
$themes = array ('cerulean','cosmo','cyborg','darkly','flatly','journal','litera','lumen','lux','materia','minty','pulse','sandstone','simplex','sketchy','slate','solar','spacelab','superhero','united','yeti');
$themes = array(
'1' => 'cerulean'
'2' => 'cosmo'
'3' => 'cyborg'
'4' => 'darkly'
'5' => 'flatly'
'6' => 'journal'
'7' => 'litera'
'8' => 'lumen'
'9' => 'lux'
'10' => 'materia'
'11' => 'minty'
'12' => 'pulse'
'13' => 'sandstone'
'14' => 'simplex'
'15' => 'sketchy'
'16' => 'slate'
'17' => 'solar'
'18' => 'spacelab'
'19' => 'superhero'
'20' => 'united'
'21' => 'yeti'
);
