<?php
if (isset($_SESSION['LoggedIn'])) {
	$sessionLoggedIN = 'LoggedIn';
} else {
	$sessionLoggedIN = '';
}
require('../db.php');
try {
	$stmt = $conn->prepare('SELECT upvote_tally, downvote_tally, comments FROM `product_review` WHERE `review_id` = :review_id');
	// Bind Parameter
	$stmt->bindParam(':review_id', $review_id, PDO::PARAM_INT);
	$stmt->execute();
	$data = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
// get the username/user_id Lookup information
try {
	$stmt3 = $conn->prepare('SELECT `user_id`,`username` FROM `user` ORDER BY `user_id` ASC');
	$stmt3->execute();
	$stmt3->setFetchMode(PDO::FETCH_ASSOC);
	$lookupTable = $stmt3->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$conn = null;
$upvote_tally = $data[0]['upvote_tally'];
$downvote_tally = $data[0]['downvote_tally'];
$row['comments'] = $data[0]['comments'];
if (isset($data[0]['comments'])) {
	$jsonComments = array_reverse(json_decode($data[0]['comments'], true));
	$jsonCount = count($jsonComments);
}
require('voting.php');
