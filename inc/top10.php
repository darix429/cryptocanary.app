<nav class="nav nav-pills nav-fill pt-3 pb-3" id="nav-tab" role="tablist">
	<a class="nav-item nav-link active text-uppercase" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Newly Added</a>
	<a class="nav-item nav-link text-uppercase" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Recent Activities</a>
	<a class="nav-item nav-link text-uppercase" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Shadiest</a>
</nav>
<div class="tab-content pt-3 pb-3" id="nav-tabContent">
	<div class="tab-pane show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
		<h2>Newly Added</h2>
		<hr>
		<?php
		$count=0;
		foreach ($data as $key => $row) {
			$count++;
			if ($count % 2 == 1){echo '<div class="row pb-3 pt-3">';}
			echo '<div class="col-md-6">
				<div class="media">
					<img class="d-flex align-self-start mr-3 col-2" src="/images/entities/'.$row['entity_id'].'/'.$row['image'].'" alt="'.$row['entity_name'].'">
					<div class="media-body">
					<table style="width:100%"><tr><td><h4>'.$row['entity_name'].' ('.$row['symbol'].')</h4></td><td class="text-primary"><i class="fa fa-commenting-o" aria-hidden="true"></i> 2 reviews</td><td class="text-warning">😀 64% shady</td></tr></table>
						<p>'.$row['entity_desc'].'</p>
					</div>
				</div>
			</div>';
			if ($count % 2 == 0){echo '</div>';}
		}
		?>
	</div>
	<div class="tab-pane" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
		<h2>Recent Activities</h2>
	</div>
	<div class="tab-pane" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
		<h2>Shadiest</h2>
	</div>
</div>
<div class="row">
	<div class="col-12">
		<p class="text-center">items 1-10 of 75</p>
	</div>
</div>
