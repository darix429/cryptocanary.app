<?php
$search= filter_input(INPUT_GET,'search',FILTER_SANITIZE_STRING);
$dissect = explode('(', $search);
$entity_name = urlencode(trim($dissect[0]));
header("Location: ../review/$entity_name");
