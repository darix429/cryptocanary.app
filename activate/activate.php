<?php
session_start();
ob_start();
date_default_timezone_set('America/Lima');
$tokenVerif =  md5('cryptocanary'.date('Ymd'));
// sanitization
$email = strtolower(filter_input(INPUT_POST,'email',FILTER_SANITIZE_EMAIL, FILTER_FLAG_EMAIL_UNICODE));
$name = filter_input(INPUT_POST,'name',FILTER_SANITIZE_STRING);
$username = strtolower(trim(filter_input(INPUT_POST,'username',FILTER_SANITIZE_STRING)));
$password = filter_input(INPUT_POST,'password',FILTER_SANITIZE_STRING);
$token = filter_input(INPUT_POST,'token',FILTER_SANITIZE_STRING);
$outputFile = 'activations.txt';
$log = date('Y-m-d H:i:s').','.$email.','.$username.','.$name.PHP_EOL;
file_put_contents($outputFile,$log,FILE_APPEND);

// initial check
if ($token != $tokenVerif) {
	echo '<div id="result"><div class="alert alert-danger" role="alert" id="activateError"><h3>Error!</h3><p>Something went wrong.</p><p>Try Again</p></div></div><script>$(document).ready(function(){$(window).scrollTop(0);});</script>';
	exit();
}
if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	echo '<div id="result"><div class="alert alert-danger" role="alert" id="activateError"><h3>Error!</h3><p>Your email address: '.$email.' is not valid.</p><p>Try Again</p></div></div><script>$(document).ready(function(){$(window).scrollTop(0);});</script>';
	exit();
}
if (strlen($name) <2) {
	echo '<div id="result"><div class="alert alert-danger" role="alert" id="activateError"><h3>Error!</h3><p>Your name: '.$name.' need to have more than 3 characters.</p><p>Try Again</p></div></div><script>$(document).ready(function(){$(window).scrollTop(0);});</script>';
	exit();
}
if (strlen($username) <2) {
	echo '<div id="result"><div class="alert alert-danger" role="alert" id="activateError"><h3>Error!</h3><p>Your username: '.$username.' need to have more than 3 characters.</p><p>Try Again</p></div></div><script>$(document).ready(function(){$(window).scrollTop(0);});</script>';
	exit();
}
if (strlen($password) <5) {
	echo '<div id="result"><div class="alert alert-danger" role="alert" id="activateError"><h3>Error!</h3><p>Your password: '.$password.' need to have more than 6 characters.</p><p>Try Again</p></div></div><script>$(document).ready(function(){$(window).scrollTop(0);});</script>';
	exit();
}
// check if the username is already taken.
require('../db.php');
try {
	$stmt1 = $conn->prepare('SELECT `username` FROM `user` WHERE `username` = :username');
	$stmt1->bindParam(':username', $username, PDO::PARAM_STR);
	$stmt1->execute();
	$count1 = $stmt1->rowCount();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
if ($count1 >0) {
	$conn = null;
	echo '<div id="result"><div class="alert alert-danger" role="alert" id="activateError"><h3>Error!</h3><p>Username: '.$username.' is already taken.</p><p>Try Again</p></div></div><script>$(document).ready(function(){$(window).scrollTop(0);});</script>';
	exit();
}
// check if the email is already activated.
try {
	$stmt2 = $conn->prepare('SELECT `email` FROM `user` WHERE `email` LIKE :email');
	$stmt2->bindParam(':email', $email, PDO::PARAM_STR);
	$stmt2->execute();
	$count2 = $stmt2->rowCount();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
if ($count2 >0) {
	$conn = null;
	echo '<div id="result"><div class="alert alert-danger" role="alert" id="activateError"><h3>Error!</h3><p>An account with the following email address: '.$email.' was already activated.</p><p>Please Login</p></div></div><script>$(document).ready(function(){$(window).scrollTop(0);});</script>';
	exit();
} else {
	$avatar = 'random-avatar-'.rand(1,19);
	$create_time = date('Y-m-d H:i:s');
	// Create the user account
	// hash the user password with BCRYPT
	$hash = password_hash($password, PASSWORD_BCRYPT);
	try {
		$stmt3 = $conn->prepare("INSERT INTO `user` (`name`, `username`, `email`, `password`, `avatar`, `create_time`) VALUES (:name, :username, :email, :password, :avatar, :create_time)");
		$stmt3->bindParam(':name', $name, PDO::PARAM_STR);
		$stmt3->bindParam(':username', $username, PDO::PARAM_STR);
		$stmt3->bindParam(':email', $email, PDO::PARAM_STR);
		$stmt3->bindParam(':password', $hash, PDO::PARAM_STR);
		$stmt3->bindParam(':avatar', $avatar, PDO::PARAM_STR);
		$stmt3->bindParam(':create_time', $create_time, PDO::PARAM_STR);
		$stmt3->execute();
		$last_id = $conn->lastInsertId();
	}
	catch(PDOException $e) {
		echo 'Error: ' . $e->getMessage();
	}
	// send welcome email
	$welcome_template = "welcome2.html";
	if (strpos($_SESSION["http_referer"], "discover") !== false) {
		$welcome_template = "welcome-via-discover.html";
	}
	require_once '../class/util.php';
	$util = Util::getInstance();
	$emailData = array(
		"email"=>$email,
		"template"=>$welcome_template,
		"subject"=>"Welcome to the CryptoCanary Flock! Here's how to get started."
	);
	$util->queueEmail($emailData);
	unset($_SESSION["http_referer"]);
}


$conn = null;
echo '<div class="alert alert-success" role="alert" id="activateSuccess"><h3>Hooray!</h3><p>Your account with the following email address:<br><strong>'.$email.'</strong><br>was successfully activated.</p><p><a href="/login/" class="btn btn-primary">Click here to Login</a></p></div><script>$(document).ready(function(){$(window).scrollTop(0);});</script>';
ob_end_flush();
