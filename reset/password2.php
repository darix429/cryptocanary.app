<?php
date_default_timezone_set('America/Lima');
$tokenVerif =  md5('cryptocanary'.date('Ymd'));
session_start();
// sanitize the password input field send by the user
$reset = filter_input(INPUT_POST,'reset',FILTER_SANITIZE_STRING);
$email = strtolower(filter_input(INPUT_POST,'email',FILTER_SANITIZE_EMAIL, FILTER_FLAG_EMAIL_UNICODE));
$token = filter_input(INPUT_POST,'token',FILTER_SANITIZE_STRING);
$newPassword = filter_input(INPUT_POST,'newPassword',FILTER_SANITIZE_STRING);
// sanitize the session
$resetCode = $_SESSION['resetCode'];
$semail = $_SESSION['email'];
// make some initial checks
if ($token != $tokenVerif) {
	exit('<div class="alert alert-danger" role="alert" id="resetError"><h3>Error!</h3><p>Wrong token.<br>Try Again!</p></div>');
}
if ($reset != $resetCode) {
	exit('<div class="alert alert-danger" role="alert" id="resetError"><h3>Error!</h3><p>Wrong password reset code.<br>Try Again!</p></div>');
}
if ($email != $semail) {
	exit('<div class="alert alert-danger" role="alert" id="resetError"><h3>Error!</h3><p>We don\'t have this email in our database.<br>Try Again!</p></div>');
}
$newHash = password_hash($newPassword, PASSWORD_BCRYPT);
require('../db.php');
try {
	$stmt = $conn->prepare('UPDATE `user` SET `password` = :password WHERE `email` = :email');
	$stmt->bindParam(':password', $newHash, PDO::PARAM_STR);
	$stmt->bindParam(':email', $email, PDO::PARAM_STR);
	$stmt->execute();
}
catch(PDOException $e) {
	echo 'Error: '.$e->getMessage();
}
$conn = null;
echo '<div class="alert alert-success" role="alert" id="resetSuccess"><h3>Great!</h3><p>Password reset went successfully.</p><p><a href="../login/" class="btn btn-primary">Please login</a></p></div><script>$(document).ready(function(){$(window).scrollTop(0);});</script>';
