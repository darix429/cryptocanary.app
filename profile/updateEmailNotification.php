<?php
session_start();
$success = filter_input(INPUT_GET,'success',FILTER_SANITIZE_STRING);
$user_id = $_SESSION['user_id'];
if ($user_id < 1) {
	header('Location: ../login/');
	exit();
}
$notifyComment = $notifyReview = $notifySubmitNewCrypto = 0;
$notifyComment = filter_input(INPUT_POST,'notifyComment',FILTER_SANITIZE_STRING);
$notifyReview = filter_input(INPUT_POST,'notifyReview',FILTER_SANITIZE_STRING);
// $notifySubmitNewCrypto = filter_input(INPUT_POST,'notifySubmitNewCrypto',FILTER_SANITIZE_STRING);
if ($notifyComment == 'on') { $notifyComment = 1;} else {$notifyComment = 0;}
if ($notifyReview == 'on') { $notifyReview = 1;} else {$notifyReview = 0;}
// if ($notifySubmitNewCrypto == 'on') { $notifySubmitNewCrypto = 1;} else {$notifySubmitNewCrypto = 0;}
$notifySubmitNewCrypto = 0;
// echo $notifyComment.' - '.$notifyReview.' - '.$notifySubmitNewCrypto;
require ('../db.php');
try {
	$stmt = $conn->prepare('UPDATE `user` SET `notifyComment` = :notifyComment, `notifyReview` = :notifyReview, `notifySubmitNewCrypto` = :notifySubmitNewCrypto WHERE `user_id` = :user_id');
	$stmt->bindParam(':notifyComment', $notifyComment, PDO::PARAM_INT);
	$stmt->bindParam(':notifyReview', $notifyReview, PDO::PARAM_INT);
	$stmt->bindParam(':notifySubmitNewCrypto', $notifySubmitNewCrypto, PDO::PARAM_INT);
	$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
	$stmt->execute();
}
catch(PDOException $e) {
	echo 'Error: '.$e->getMessage();
}
$conn = null;
header('Location: emailNotification.php');
