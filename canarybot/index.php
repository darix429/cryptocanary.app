<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>CanaryBot</title>
    <meta name="description" content="CanaryBot">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <div class="container">
        <div class="row align-items-center header">
            <img src="/img/favicon.svg" draggable="false" class="logo">
            <div>
                <h3>CanaryBot  <i class="far fa-comment"></i></h3>
            </div>
            <div class="text-right col" style="width: 30px;"><i class="fas fa-dot-circle online-indicator" title="Admin bot is away" ></i></div>
        </div>
        <div class="row chat-box">
            <div class="col">
                <div class="canarybot-status text-center">
                    <!-- CanaryBot is away but leave your messages and we'll look into it when we get back! -->
                </div>
                <hr>
                <div class="chat-messages">
                    <div class="chat-balloon bot">Hi I'm CanaryBot, I'm here to help beginners in the crypto space.</div>
                    <div class="chat-balloon bot">You can start by asking any question.</div>
                </div>
                <div class="chat-box">
                    <textarea name="chat-input" class="form-control" style='resize: none' placeholder="type here..."></textarea>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://kit.fontawesome.com/e94e85d680.js" crossorigin="anonymous"></script>
    <script type="text/javascript" src="script.min.js"></script>
</body>
</html>
