<?php
if (isset($_SESSION['profile']['new'])) {
	echo '<div class="alert alert-success" role="alert" id="activateSuccess"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h3>Hooray!</h3><p>Your account was successfully activated.</p></div>';
	unset($_SESSION['profile']['new']);
}
// if (isset($_SESSION['user_id'])) {
// 	$user_id = (int)$_SESSION['user_id'];
// 	if ($user_id > 1) {
// 		if(rand(0,10)>8) {
// 			if (strpos(__DIR__,'/Applications/MAMP/') !== false) {
// 				require('/Applications/MAMP/htdocs/cryptocanary/db.php');
// 			} else {
// 				require('/var/www/html/db.php');
// 			}
// 			try {
// 				$stmt15 = $conn->prepare('SELECT `avatar`, `bio`, `youtube`,`twitter`,`facebook`,`linkedin` FROM `user` WHERE `user_id` = :user_id LIMIT 1');
// 				$stmt15->bindParam(':user_id', $user_id, PDO::PARAM_INT);
// 				$stmt15->execute();
// 				$stmt15->setFetchMode(PDO::FETCH_ASSOC);
// 				$notificationData = $stmt15->fetchAll();
// 			}
// 			catch(PDOException $e) {
// 				echo 'Error: ' . $e->getMessage();
// 			}
// 			$conn = null;
// 			if (!isset($notificationData[0]['avatar'])) {
// 				echo '<div class="alert alert-primary alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><p>You have not set an avatar for your profile picture yet. You should add an avatar to gain 5 chirps.</p></div>';
// 			}
// 			if (strlen($notificationData[0]['bio']) < 1) {
// 				echo '<div class="alert alert-primary alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><p>You have not written a bio snippet yet. You should write a bio to gain 5 chirps.</p></div>';
// 			}
// 			if (!isset($notificationData[0]['facebook']) AND !isset($notificationData[0]['youtube']) AND !isset($notificationData[0]['twitter']) AND !isset($notificationData[0]['linkedin'])) {
// 				echo '<div class="alert alert-primary alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><p>You have not shared any of your Social Media link yet. If you want to gain followers you should share them on your profile page.</p></div>';
// 			}
// 		}
// 	}
// }
