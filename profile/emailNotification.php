<?php
session_start();
$success = filter_input(INPUT_GET,'success',FILTER_SANITIZE_STRING);
$user_id = $_SESSION['user_id'];
$requestURI = $_SERVER['REQUEST_URI'];
if ($user_id < 1) {
	header('Location: /login/?requestURI='.$requestURI);
	exit();
}
require ('../db.php');
try {
	$stmt = $conn->prepare('SELECT `notifyComment`, `notifyReview`, `notifySubmitNewCrypto` FROM `user` WHERE `user_id` = :user_id LIMIT 1');
	$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$data = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: '.$e->getMessage();
}
$conn = null;
$notifyComment = $notifyReview = $notifySubmitNewCrypto = '';
if ($data['0']['notifyComment'] == 1) {
	$notifyComment = 'checked';
}
if ($data['0']['notifyReview'] == 1) {
	$notifyReview = 'checked';
}
if ($data['0']['notifySubmitNewCrypto'] == 1) {
	$notifySubmitNewCrypto = 'checked';
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php'); ?>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css">
</head>
<body>
	<?php include('../inc/nav.php'); ?>
	<main class="container pt-5 pb-5">
		<p><a href="index.php" class="btn btn-outline-primary"><i class="fas fa-arrow-left"></i> Back To Profile</a></p>
		<h1>My Email Notification:</h1>
		<?php
		if (strlen($success)>3) {
			echo '<div class="alert alert-success alert-dismissible fade show" role="alert" id="profileUpdateSuccess"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h3>Congratulation!</h3><p>Your profile picture has been uploaded successfully.</p></div>';
		}
		?>
		<div id="result"></div>
		<form action="updateEmailNotification.php" method="POST" role="form">
			<legend>When would you like to be notified by email?</legend>
			<div class="form-group">
				<div class="row">
					<div class="col-8"><label class="form-control-label" for="notifyComment">When someone comments under one of my reviews</label></div>
					<div class="col-4"><input type="checkbox" name="notifyComment" class="bootstrapToggle" <?php echo $notifyComment; ?> data-toggle="toggle" data-on="Yes" data-off="No"></div>
				</div>
			</div>
			<div class="form-group">
				<div class="row">
					<div class="col-8"><label class="form-control-label" for="notifyReview">When someone writes a new review under a project I’ve previously reviewed</label></div>
					<div class="col-4"><input type="checkbox" name="notifyReview" class="bootstrapToggle" <?php echo $notifyReview; ?> data-toggle="toggle" data-on="Yes" data-off="No"></div>
				</div>
			</div>
			<button type="submit" class="btn btn-primary">Save Changes</button> <i id="spinner" class="fa fa-spinner fa-spin" style="display:none"></i>
		</form>
	</main>
	<?php include('../inc/footer.php'); ?>
	<?php include('../inc/endScripts.php'); ?>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/js/bootstrap4-toggle.min.js"></script>
</body>
</html>
