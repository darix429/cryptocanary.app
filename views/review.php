<?php
session_start();
$team_quality = $info_quality = $track_record = 0;
include('inc/getRelativeTime.php');
$error = filter_input(INPUT_GET,'error',FILTER_SANITIZE_STRING);
$message = filter_input(INPUT_GET,'message',FILTER_SANITIZE_STRING);
if (isset($_SESSION['jsonData'])) {$jsonData = $_SESSION['jsonData'];}
$sort = filter_input(INPUT_GET,'sort',FILTER_SANITIZE_STRING);
switch ($sort) {
	case 'upvote':
	$sorting = '`review`.upvote_tally DESC';
	break;
	case 'downvote':
	$sorting = '`review`.downvote_tally DESC';
	break;
	case 'date';
	$sorting = '`review`.update_time DESC';
	break;
	case 'latest';
	$sorting = '`review`.update_time DESC';
	break;
	case 'earliest';
	$sorting = '`review`.update_time ASC';
	break;
	default:
	$sorting = '`review`.review_id DESC';
	break;
}
$coinloreData = '';

// Start of all database request for this page
require('db.php');
try {
	$stmt = $conn->prepare('SELECT * FROM `entity` WHERE `entity_name` LIKE :name LIMIT 1');
	$stmt->bindParam(':name', $name, PDO::PARAM_STR);
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$entity = $stmt->fetchAll();
	$id = $entity['0']['entity_id'];
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
// Get all the review from the Entity
try {
	$stmt1 = $conn->prepare("SELECT review.review_id, review.user_id, review.entity_id, review.review, review.rating, review.team_quality, review.info_quality, review.track_record, review.update_time, review.upvote_tally, review.downvote_tally, review.comments, `user`.username, `user`.avatar, `user`.score FROM review INNER JOIN `user` ON review.user_id = `user`.user_id WHERE `review`.entity_id = :id AND `review`.delete_time IS NULL ORDER BY $sorting");
	$stmt1->bindParam(':id', $id, PDO::PARAM_INT);
	$stmt1->execute();
	$stmt1->setFetchMode(PDO::FETCH_ASSOC);
	$countData = $stmt1->rowCount();
	$data = $stmt1->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}

$entity_name = trim($entity['0']['entity_name']);
$symbol = $entity['0']['symbol'];
$entity_id = $entity['0']['entity_id'];
// Get the coinlore ID from symbol
try {
	$stmt2 = $conn->prepare('SELECT `coinloreID` FROM `coinlore` WHERE `symbol` = :symbol');
	$stmt2->bindParam(':symbol', $symbol, PDO::PARAM_STR);
	$stmt2->execute();
	$stmt2->setFetchMode(PDO::FETCH_NUM);
	$coinlore = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
// get the username/user_id Lookup information
try {
	$stmt3 = $conn->prepare('SELECT `user_id`,`username` FROM `user` ORDER BY `user_id` ASC');
	$stmt3->execute();
	$stmt3->setFetchMode(PDO::FETCH_ASSOC);
	$lookupTable = $stmt3->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$conn = null;
// end of all the database request

// function to convert 10,901,204,081.85 to 10.9M (more human readable long number)
function convert($numbers) {
	$readable = array("","K","M","B");
	$index=0;
	while($numbers > 1000){
		$numbers /= 1000;
		$index++;
	}
	return(round($numbers, 1).' '.$readable[$index]);
}

if (isset($coinlore[0][0])) {
	$coinloreID = $coinlore[0][0];
} else {
	$coinloreID = '';
}
if (empty($entity)) {
	header("Location: ../review/empty.php?search=$name");
	exit();
}
$logo = $entity[0]['entity_id'].'/'.$entity[0]['image'];
$entity_desc = $entity[0]['entity_desc'];
$overall_rating = $entity[0]['overall_rating'];
$average_team_quality = number_format($entity[0]['average_team_quality'],1);
$average_info_quality = number_format($entity[0]['average_info_quality'],1);
$average_track_record = number_format($entity[0]['average_track_record'],1);
if (strlen($entity[0]['tag1'])>3) {
	$tag1 = $entity[0]['tag1'];
} else {
	$tag1 = $entity_name.' ('.$symbol.') -  Reviews &amp; Ratings';
}
$links = json_decode($entity[0]['links']);
$websiteURL = $links-> website;
$count = $totalRating = 0;
$confidence = "N/A";
if ($overall_rating != null){
	$confidence = number_format((($overall_rating)), 0);
	if ($confidence <= 25) {$pbcolor = 'bg-danger';}
	if ($confidence > 25 AND $confidence <=50) {$pbcolor = 'bg-warning';}
	if ($confidence > 50 AND $confidence <=75) {$pbcolor = 'bg-info';}
	if ($confidence > 75) {$pbcolor = 'bg-success';}
}
//echo 'Confidence: '.$confidence;
// SEO purpose
$title = $entity_name.' ('.$symbol.') Review - How Trustworthy Is '.ucfirst($entity_name).'?';
$description = 'View '.$entity_name.' ('.$symbol.') reviews, ratings, and overall community sentiment score. Use this information, along with your own research, to determine if '.$entity_name.' is shady or legit.';
$uri = '/review/'.urlencode($entity_name);
include('inc/getMarketInfo.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<script type="text/javascript">
		window.dataLayer = window.dataLayer || [];
		window.dataLayer.push({
			'event': 'formSubmission',
			'formType': 'Search',
			'searchText': '<?php echo $entity_name;?>',
			'formPosition': 'NavBar'
		});
	</script>
	<?php include('inc/head.php'); ?>
	<style>
		th, td {
			padding: 2px 15px;
			text-align: left;
		}
	</style>
	<script type="text/javascript">var uri = window.location.toString();if (uri.indexOf("?") > 0) {var clean_uri = uri.substring(0, uri.indexOf("?"));window.history.replaceState({}, document.title, clean_uri);}</script>
</head>
<body>
	<?php include('inc/nav.php'); ?>
	<div class="greyBG">
		<div class="container pt-5">
			<?php
			include('inc/notification.php');
			if (isset($message)) {
				echo '<div class="alert alert-success" role="alert" id="reviewAddSuccess">'.$message.'</div>';
			}
			if ($confidence>50) {
				// echo '<p class="text-center"><i class="fas fa-check success"></i> Trusted Project</p>';
			}
			?>
			<div class="row ">
				<div class="d-sm-flex flex-fill justify-content-center pb-3">
					<div class="align-self-center text-center"><img src="/images/entities/<?php echo $logo; ?>" alt="<?php echo $entity_name; ?>" class="img-fluid pl-2 pr-2" style="height:50px;"></div>
					<div class="align-self-center text-center"><h1 class="pl-2 pr-2"><?php echo $entity_name;?></h1></div>
					<div class="align-self-center text-center"><p><span class="badge badge-secondary pl-4 pr-4" style="background-color:#b7b4b3;font-size:25px;line-height:30px; border-radius: 25px;"><?php echo $symbol; ?></span></p></div>
				</div>
			</div>
			<div class="row">
				<div class="d-sm-flex">
					<div class="p-2 border-right border-dark">
						<small>WEBSITE</small><br><a href="<?php echo $websiteURL.'?ref=cryptocanary'; ?>"  target="_blank" class="text-dark"><?php echo $websiteURL; ?></a>
					</div>
					<div class="p-2">
						<small>PRICE (USD)</small><br><?php
						if (isset($coinlorePrice_usd) AND $coinlorePrice_usd > 0) {echo '$ '.number_format($coinlorePrice_usd,2);}
						else { echo 'N/A';}?>
					</div>
					<div class="p-2">
						<small>MARKET CAP (USD)</small><br><?php
						if (isset($coinloreMarket_cap_usd) AND $coinloreMarket_cap_usd > 0) {echo '$ '.convert($coinloreMarket_cap_usd); }
						else { echo 'N/A';}
						?>
					</div>
					<div class="p-2">
						<small>DAILY VOLUME (USD)</small><br><?php
						if (isset($coinloreVolume_24) AND $coinloreVolume_24 > 0) {echo '$ '.convert($coinloreVolume_24); }
						else { echo 'N/A';}
						// echo ($coinloreVolume_24 == 0 || $coinloreVolume_24 == null)? "N/A": "$coinloreVolume_24"; ?>
					</div>
					<div class="p-2 border-right border-dark">
						<small>NUMBER OF EXCHANGES</small><br><?php echo ($numberOfExchanges == 0 || $numberOfExchanges == null)? "N/A": $numberOfExchanges ?>
					</div>
					<div class="p-2 flex-grow-1">
						<small>SOCIAL MEDIA</small><br><?php
						foreach ($links as $key => $value) {
							if (is_array($value)) {
								$value = $value[0];
							}
							if ($key =='website') {
								echo '';
							} else{
								echo '<a href="'.trim(strtolower($value)).'" target="_blank"><i class="fab fa-'.trim(strtolower($key)).'"></i></a>&nbsp;';
							}
						}
						?>
					</div>
				</div>
			</div>
			<hr>
		</div>
	</div>
	<div class="container pt-3 pb-3">
		<?php
		if (!empty($jsonData)) {
			$jsonDataArray = json_decode($jsonData, true);
			$team_quality = $jsonDataArray['teamQuality'];
			$info_quality = $jsonDataArray['infoQuality'];
			$track_record = $jsonDataArray['trackRecord'];
			$jsonUserReview = $jsonDataArray['review'];
		}
		if (strlen($error)>1) {
			echo '
			<div class="row">
			<div class="col-12">
			<div class="alert alert-danger" role="alert"><h3>Error!</h3><p>'.$error.'<br>Try Again.</p></div>
			</div>
			</div>
			';
		}
		?>
		<div class="row">
			<div class="col-12">
				<?php include('inc/overallRatingBar.php'); ?>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-9" id="reviewText">
				<?php
				if (strpos(__DIR__,'/Applications/MAMP/') !== false) {
					include('/Applications/MAMP/htdocs/cryptocanary/review/review.php');
				} else {
					include('/var/www/html/review/review.php');
				}
				include('inc/horizontalBanner.php');
				?>
				<div id="reviews">
					<p class="text-right">Sort by: <a href="<?php echo $uri; ?>&sort=latest">Latest</a> / <a href="<?php echo $uri; ?>&sort=earliest">Earliest</a> / <a href="<?php echo $uri;?>&sort=upvote">Upvotes</a> / <a href="<?php echo $uri;?>&sort=downvote">Downvotes</a></p>
					<?php
					$iNumber = 1;
					if ($countData <1) {
						echo '<div class="alert alert-success" role="alert"><p>So... there aren\'t any reviews here yet.</p><p>But here are 3 reasons why you should write one:</p><ol>
						<li>You can set the tone for this project and kickstart the conversation.</li><li>You can help newbies make better decisions by sharing your experiences.</li><li>You\'d earn Canary Chirps for writing this review!</li></ol><p>What are you waiting for? Write one now!</p></div><div><p class="text-center"><img src="/img/empty_state.svg" class="img-fluid" alt="No review"></p></div>';
					}
					foreach ($data as $key => $row) {
						$reviewDate = date('F d, Y H:i:s', strtotime($row['update_time']));
						$review = nl2br($row['review']);
						if (strlen($review) > 400) {
							$review = $review.' ';
							$limit= strlen($review);
							for ($i=400; $i <$limit ; $i++) {
								$letterTest = substr($review, $i, 1);
								if ($letterTest == ' ') {
									$textLimit = $i;
									break;
								}
							}
							$leftOver = substr($review, $textLimit);
							if (strlen($leftOver)>1){
								$review = substr($review, 0, $textLimit).'<button class="btn btn-link" id="seeMore'.$iNumber.'" onClick="$(\'#more'.$iNumber.'\').show();$(\'#seeMore'.$iNumber.'\').hide()"> Click here to see more...</button><span id="more'.$iNumber.'" style="display:none;">'.substr($review, $textLimit).'</span>';

							}
						}
						$review_id = $row['review_id'];
						$upvote_tally = $row['upvote_tally'];
						$downvote_tally = $row['downvote_tally'];
						$team_quality = $row['team_quality'];
						$info_quality = $row['info_quality'];
						$track_record = $row['track_record'];
						$username = $row['username'];
						$rating = 5-(int)$row['rating'];
						if ($team_quality < 1 AND $info_quality < 1 AND $track_record < 1) {
							$team_quality = $info_quality = $track_record = $rating;
						}
						$user_id = $row['user_id'];
						$avatar = $row['avatar'];
						$randomAvatar = '/images/avatars/random/avatar'.rand(1,19).'.png';
						if(strpos($avatar, 'random') !== false) { $avatar = $randomAvatar;}
						elseif(strlen($avatar) <1) { $avatar = $randomAvatar;}
						else { $avatar = '/images/avatars/'.$user_id.'/'.$avatar;}
						echo '
						<div class="row pb-3" id="#review-'.$review_id.'">
						<div class="col-12 whiteBG mt-3 mp-3 pt-2 rounded-lg">';
						echo '
						<div class="d-sm-flex">
						<div class="p-2 flex-fill">
						<a href="/user/'.urlencode($username).'"><img src="'.$avatar.'" class="img-fluid" alt="avatar" style="height:50px;"></a> <a href="/user/'.urlencode($username).'">'.$username.'</a> <span data-toggle="tooltip" title="These are Chirps (reputation points). The more you gain, the more features and prizes you’ll unlock!" class="text-dark"><span class="fa-layers fa-fw" style="font-size: 1.8em;"><i class="fas fa-comment-alt" style="color: #ffa81e;"></i><span class="fa-layers-text" data-fa-transform="shrink-8 up-1" style="font-weight:900; font-size: 1.5rem;">'.$row['score'].'</span></span></span>
						</div>
						<div class="p-2 flex-fill">
						<u>Team Quality</u><br><input type="text" id="team_quality" name="team_quality" class="kv-rtl-theme-svg-star2 rating-loading" value="'.$team_quality.'" data-size="xs">
						</div>
						<div class="p-2 flex-fill">
						<u>Info Quality</u><br><input type="text" id="info_quality" name="info_quality" class="kv-rtl-theme-svg-star2 rating-loading" value="'.$info_quality.'" data-size="xs">
						</div>
						<div class="p-2 flex-fill">
						<u>Track record</u><br><input type="text" id="track_record" name="track_record" class="kv-rtl-theme-svg-star2 rating-loading" value="'.$track_record.'" data-size="xs">
						</div>
						</div>
						<hr>
						<p><small><strong><span class="text-muted"><a href="/user/'.urlencode($username).'" class="text-dark">'.$username.'</a></strong> posted</span> on '.$reviewDate.'</small></p>
						<p class="pt-2">'.$review.'</p><hr>';
						include('review/voting.php');
						echo '</div>
						</div>';
					}
					?>
				<?php include('inc/twitterFeed.php'); ?>
				</div>
			</div>
			<div class="col-lg-3">
				<?php include('inc/rightSideBar.php'); ?>
			</div>
		</div>
	</div>
	<?php include('inc/footer.php'); ?>
	<?php include('inc/endScripts.php'); ?>
	<script  type="text/javascript">
		var myTextEl = document.getElementById('reviewText');
		myTextEl.innerHTML = Autolinker.link( myTextEl.innerHTML, {
			stripPrefix: false,
			newWindow: true
		});
	</script>
	<script type="text/javascript">
		$('.kv-rtl-theme-svg-star').rating({
			hoverOnClear: false,
			theme: 'krajee-svg',
			step: 1,
			disabled: false,
			readonly: false,
			showClear: false,
			starCaptions: {1: 'Very Poor', 2: 'Poor', 3: 'Neutral', 4: 'Good', 5: 'Very Good'}
		});
		$('.kv-rtl-theme-svg-star2').rating({
			hoverOnClear: false,
			theme: 'krajee-svg',
			showClear: false,
			disabled: true,
			readonly: true,
			showCaption: false
		});
	</script>
	<script type="application/ld+json">
		{
			"@context": "http://schema.org/",
			"@type": "Product",
			"aggregateRating": {
			"@type": "AggregateRating",
			"ratingValue": <?php echo round($confidence); ?>,
			"reviewCount": <?php if($iNumber>0) {echo $iNumber -1;} else {echo 0;} ?>,
			"bestRating": 100,
			"worstRating": 0
		},
		"brand": "<?php echo $entity_name; ?>",
		"name": "<?php echo $entity_name; ?>",
		"alternateName": "<?php echo $symbol; ?>",
		"description": "<?php echo $description; ?>",
		"image": "https://cryptocanary.app/images/entities/<?php echo $logo; ?>",
		"url": "<?php echo $websiteURL; ?>",
		"review": {
		"reviewRating": {
		"@type": "Rating",
		"ratingValue": <?php echo round($confidence); ?>,
		"bestRating": 100,
		"worstRating": 0
	},
	"author": {
	"@type": "Organization",
	"name": "CryptoCanary",
	"logo": "https://cryptocanary.app/images/Cryptocanary-logo.png",
	"url": "https://cryptocanary.app/",
	"description": "CryptoCanary consolidates community reviews and research to help beginners avoid crypto scams. Submit, rate, and review crypto projects to get started!."
}
}
}
</script>
<script type="text/javascript">$(function () {$('[data-toggle="tooltip"]').tooltip()})</script>
<script type="text/javascript">console.log('Confidence: <?php echo $confidence; ?>. Overall Stars: <?php echo $overall_stars ; ?>.');</script>
</body>
</html>
