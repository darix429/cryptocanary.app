<?php
if ($confidence <25) {
	$confidenceBgColor = 'alert-danger';
}
if ($confidence >=25 AND $confidence < 50) {
	$confidenceBgColor = 'alert-warning';
}
if ($confidence >=50 AND $confidence < 75) {
	$confidenceBgColor = 'alert-primary';
}
if ($confidence >=75) {
	$confidenceBgColor = 'alert-success';
}
if ($average_team_quality == 0 AND $average_info_quality == 0 AND $average_track_record == 0) {
	$average_team_quality = $average_info_quality = $average_track_record = $confidence;
}
if (!isset($overall_rating)) {
	$average_team_quality = $average_info_quality = $average_track_record = 0;
}
$overall_stars = number_format(($overall_rating/25)+1,1);
if ($overall_rating == 0) {
	$average_rating = 'N/A';
	$overall_stars = '';
} else {
	$average_rating = number_format($overall_rating,0);
}
?>
<div class="row" id="overallRating">
	<div class="col-sm-6 col-lg-3 pb-2 border-right border-dark">
		<span class="text-muted"></span><strong>(<?php echo $average_rating; ?>%)</strong> <small>Average Rating</small><br>
		<div class="pt-3"><input id="average_team_quality" class="kv-rtl-theme-svg-star2 rating-loading" value="<?php echo $overall_stars; ?>" data-size="md"></div>
	</div>
	<div class="col-sm-6 col-lg-3 pt-2 pb-2 pt-2">
		<span class="pl-2">Team Quality:</span><button type="button" class="btn btn-link" data-toggle="tooltip" data-placement="top" title="How strong is the team and do they have the relevant experience and integrity to execute on their vision?"><i class="fas fa-question-circle"></i></button><br><input id="average_team_quality" class="kv-rtl-theme-svg-star2 rating-loading" value="<?php echo $average_team_quality; ?>" data-size="sm">
	</div>
	<div class="col-sm-6 col-lg-3 pt-2 pb-2 pt-2">
		<span class="pl-2">Info Quality:</span><button type="button" class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Is information about this project accessible, high-quality, and truthful? (e.g. whitepaper, website, etc.)"><i class="fas fa-question-circle"></i></button><br><input id="average_info_quality" class="kv-rtl-theme-svg-star2 rating-loading" value="<?php echo $average_info_quality; ?>" data-size="sm">
	</div>
	<div class="col-sm-6 col-lg-3 pt-2 pb-2 pt-2">
		<span class="pl-2">Track record:</span><button type="button" class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Have they delivered on prior promises and do you have confidence that they will deliver on future ones?"><i class="fas fa-question-circle"></i></button><br><input id="average_track_record" class="kv-rtl-theme-svg-star2 rating-loading" value="<?php echo $average_track_record; ?>" data-size="sm">
	</div>
</div>
