<?php
session_start();
$success = filter_input(INPUT_GET,'success',FILTER_SANITIZE_STRING);
$rand = 'crypto'.rand(100,9999);
// include('../inc/session.php');
$user_id = $_SESSION['user_id'];
if ($user_id < 1) {
	header('Location: ../login/');
	exit();
}
require('../db.php');
try {
	$stmt = $conn->prepare('SELECT `username`, `first_name`, `last_name`, `email`, `avatar`, `bio`,`facebook`,`youtube`,`twitter`,`score`,`password` FROM `user` WHERE `user_id` = :user_id');
	$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$data = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$conn = null;
// update the session info about the user
$_SESSION['username'] = $data[0]['username'];
$_SESSION['userEmail'] = $data[0]['email'];
$_SESSION['avatar'] = $data[0]['avatar'];
$_SESSION['chirps'] = $data[0]['score'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php'); ?>
</head>
<body>
	<?php include('../inc/nav.php'); ?>
	<main class="container pt-5 pb-5">
		<?php
			include('../inc/notification.php');
		?>
		<h1>My Profile:</h1>
		<?php
		if (strlen($success)>3) {
			echo '<div class="alert alert-success alert-dismissible fade show" role="alert" id="profileUpdateSuccess"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h3>Congratulation!</h3><p>Your profile picture has been uploaded successfully.</p></div>';
		}
		?>
		<div id="result"></div>
		<form ic-post-to="update.php" ic-target="#result" ic-indicator="#spinner" method="POST" role="form">
			<div class="row">
				<div class="col-md-4">
					<h5>Public Profile Link</h5>
				</div>
				<div class="col-md-6">
					<p><a href="https://cryptocanary.app/user/<?php echo $data[0]['username']; ?>">https://cryptocanary.app/user/<?php echo $data[0]['username']; ?></a></p>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-4">
					<h5>Email and Password reset</h5>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="form-control-label" for="email">Email</label>
						<input type="email" name="email" id="email" class="form-control" value="<?php echo $data[0]['email']; ?>" required="required" minlength="5" maxlength="50" <?php if(strlen($data[0]['password']) <1){ echo 'readonly';} ?>>
						<?php if(strlen($data[0]['password']) <1){ echo '<small id="emailHelp" class="form-text text-muted">You are loggedIn via your social media account (Facebook or Google account), therefore you cannot change this email address.</small>';} ?>
					</div>
					<p><a href="/reset/">Password Reset</a></p>
					<p><button type="submit" class="btn btn-primary">Update</button> <i id="spinner" class="fas fa-spinner fa-spin" style="display:none"></i></p>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-4">
					<h5>Basic Information</h5>
					<p>Show the community your personal style with a unique biography.</p>
					<p class="text-mute"><small>Visible on your public profile and reviews.</small></p>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label class="form-control-label" for="username">Username</label>
						<input type="text" name="username" id="username" class="form-control text-lowercase" value="<?php echo $data[0]['username']; ?>" required="required" minlength="3" maxlength="50">
						<small id="usernameHelp" class="form-text text-muted">The username is what will appear on the website. (minimum 3 characters, no Uppercase allowed)</small>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="first_name">First Name</label>
						<input type="text" name="first_name" id="first_name" class="form-control" value="<?php echo $data[0]['first_name']; ?>" minlength="2" maxlength="50">
					</div>
					<div class="form-group">
						<label class="form-control-label" for="last_name">Last Name</label>
						<input type="text" name="last_name" id="last_name" class="form-control" value="<?php echo $data[0]['last_name']; ?>" minlength="2" maxlength="50">
					</div>

					<div class="form-group">
						<label class="form-control-label" for="bio">Biography</label>
						<textarea name="bio" id="bio" class="form-control" cols="30" rows="3" minlength="20" maxlength="255"><?php echo $data[0]['bio']; ?></textarea>
					</div>
					<p><button type="submit" class="btn btn-primary">Update</button> <i id="spinner" class="fas fa-spinner fa-spin" style="display:none"></i></p>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-4">
					<h5>Avatar</h5>
					<p>Show the community your personal style with a unique avatar image.</p>
					<p class="text-mute"><small>Visible on your public profile and reviews.</small></p>
				</div>
				<div class="col-md-8">
					<p><img src="/images/avatars/<?php echo $user_id.'/'.$data[0]['avatar']; ?>" class="img-fluid img-thumbnail" alt="Avatar" style="max-width: 200px;"></p>
					<p><a href="avatar.php" class="btn btn-primary">Change Your avatar</a></p>
				</div>
			</div>
			<hr>
			<div class="row" id="socialMedia">
				<div class="col-md-4">
					<h5>Social Media Links</h5>
					<p>Connect with other community members on social media.</p>
					<p class="text-mute"><small>Visible on your public profile.</small></p>
				</div>
				<div class="col-md-8">
					<div class="form-group">
						<label class="form-control-label" for="facebook">Your Facebook URL:</label>
						<input type="url" name="facebook" id="facebook" class="form-control" value="<?php echo $data[0]['facebook']; ?>">
						<small id="facebookHelp" class="form-text text-muted">Please include the http:// or https:// in your Facebook URL.</small>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="youtube">Your Youtube URL:</label>
						<input type="url" name="youtube" id="youtube" class="form-control" value="<?php echo $data[0]['youtube']; ?>">
						<small id="youtubeHelp" class="form-text text-muted">Please include the http:// or https:// in your Youtube URL.</small>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="twitter">Your Twitter URL:</label>
						<input type="url" name="twitter" id="twitter" class="form-control" value="<?php echo $data[0]['twitter']; ?>">
						<small id="twitterHelp" class="form-text text-muted">Please include the http:// or https:// in your Twitter URL.</small>
					</div>
					<p><button type="submit" class="btn btn-primary">Update</button> <i id="spinner" class="fas fa-spinner fa-spin" style="display:none"></i></p>
				</div>
			</div>
			<hr>
			<div class="row" id="chirps">
				<div class="col-md-4">
					<h5>Chirps and Badges</h5>
					<p>Earn Chirps by interacting with the community and unlock profile badges to show your expertise.</p>
				</div>
				<div class="col-md-8">
					<div class="form-group">
						<label class="form-control-label" for="chirp">Chirps</label>
						<input type="number" name="chirp" id="chirp" class="form-control" value="<?php echo $data[0]['score']; ?>" disabled="disabled">
						<small id="chirpHelp" class="form-text text-muted">To get more chirp, add a photo profile, update all the profile field, write review, add new cryptocurrency project, and upvote/downvote reviews.</small>
					</div>
				</div>
			</div>
		</form>
		<hr>
		<div class="row">
			<div class="col-md-4">
				<p>&nbsp;</p>
			</div>
			<div class="col-md-8">
				<p><a href="emailNotification.php" class="btn btn-secondary">Email Notification Setup</a></p>
				<p><a href="/onBoarding/" class="btn btn-warning">Onboarding</a></p>
			</div>
		</div>
	</main>
	<?php include('../inc/footer.php'); ?>
	<?php include('../inc/endScripts.php'); ?>
</body>
</html>
