<?php
ob_start();
session_start();
// var_dump($_POST);
// sanitize the session
if (!isset($_SESSION['user_id'])) {
	$jsonPost=json_encode($_POST);
	echo $jsonPost;
	exit();
}
$user_id = filter_var($_SESSION['user_id'], FILTER_SANITIZE_NUMBER_INT);
if (!isset($user_id)) {
	echo '<h1>Redirect to the login page</h1>';
}
if (strpos(__DIR__,'/Applications/MAMP/') !== false) {
} else {
	$outputFile = '/var/www/html/logs/reviews.txt';
	$log = time().','.$user_id.','.json_encode($_POST).PHP_EOL;
	file_put_contents($outputFile,$log,FILE_APPEND);
}
$review_id = 0;
// sanitize user post
$rating = filter_input(INPUT_POST,'rating',FILTER_SANITIZE_NUMBER_INT);
$review = trim(filter_input(INPUT_POST,'review',FILTER_SANITIZE_STRING));
$entity_id = filter_input(INPUT_POST,'entity_id',FILTER_SANITIZE_NUMBER_INT);
$entity_name = filter_input(INPUT_POST,'entity_name',FILTER_SANITIZE_STRING);
$token = filter_input(INPUT_POST,'token',FILTER_SANITIZE_STRING);
$review_id = filter_input(INPUT_POST,'updateCode',FILTER_SANITIZE_NUMBER_INT);
// some check
date_default_timezone_set('America/Lima');
//$tokenVerif = md5('cryptocanary'.date('Ymd'));
// if ($token != md5('cryptocanary'.date('Ymd')) AND $token != date('Ymd',strtotime("-1 days"))) {
//	exit('<div class="alert alert-danger alert-dismissible fade show" role="alert">
//	     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
//	     <h3>Holy guacamole!</h3><p>There was an unexpected problem.<p>Please contact support at <a href="mailto:team@cryptocanary.app?subject=Wrong Token">team@cryptocanary.app</a></p>
//	     </div>');
//}
if (strlen($review )<160) {
	exit('<div class="alert alert-danger alert-dismissible fade show" role="alert" id="reviewAddError">
	     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     <h3>Holy guacamole!</h3><p>You need at least 160 characters in your review</p>
	     </div><script>$(document).ready(function(){$(window).scrollTop(0);});</script>');
}
if ($entity_id<1) {
	exit('<div class="alert alert-danger alert-dismissible fade show" role="alert" id="reviewAddError">
	     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     <h3>Holy guacamole!</h3><p>There was an unexpected problem.</p><p>Please contact support at <a href="mailto:team@cryptocanary.app?subject=Entity ID is missing">team@cryptocanary.app</a></p>
	     </div><script>$(document).ready(function(){$(window).scrollTop(0);});</script>');
}
if ($user_id<1) {
	exit('<div class="alert alert-danger alert-dismissible fade show" role="alert" id="reviewAddError">
	     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     <h3>Holy guacamole!</h3><p>There was an unexpected problem.</p><p>Please contact support at <a href="mailto:team@cryptocanary.app?subject=User ID is missing">team@cryptocanary.app</a></p>
	     </div><script>$(document).ready(function(){$(window).scrollTop(0);});</script>');
}
// store it in the database
$review_title = 'My Review';
require('../db.php');
// Check if the user has already entered something for this review
try {
	$stmt4 = $conn->prepare('SELECT review_id FROM `review` WHERE `entity_id` = :entity_id AND `user_id` = :user_id');
	$stmt4->bindParam(':entity_id', $entity_id, PDO::PARAM_INT);
 	$stmt4->bindParam(':user_id', $user_id, PDO::PARAM_INT);
 	$stmt4->execute();
 	$count = $stmt4->rowCount();
}
catch(PDOException $e) {
 	echo 'Error: ' . $e->getMessage();
}
// if the user didn't enter any review yet, create a new record
// echo $count.' - '.$entity_id.' - '.$user_id;
if ($count == 0) {
	try {
		$stmt = $conn->prepare('INSERT INTO `review` (`review`, `user_id`, `entity_id`, `rating`, `review_title`) VALUES (:review, :user_id, :entity_id, :rating, :review_title)');
		$stmt->bindParam(':review', $review, PDO::PARAM_STR);
		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
		$stmt->bindParam(':entity_id', $entity_id, PDO::PARAM_INT);
		$stmt->bindParam(':rating', $rating, PDO::PARAM_INT);
		$stmt->bindParam(':review_title', $review_title, PDO::PARAM_STR);
		$stmt->execute();
	}
	catch(PDOException $e) {
		echo 'Error: ' . $e->getMessage();
	}
	echo '<div class="alert alert-success alert-dismissible fade show" role="alert" id="reviewAddSuccess">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h3>Great!</h3><p>You review has been added.</p><p><button class="btn btn-primary" onClick="document.location.reload(true)"><i class="fas fa-sync"></i> Refresh this page</button></p>
	</div><script>$(document).ready(function(){$(window).scrollTop(0);});</script>';
} else {
	try {
		$stmt = $conn->prepare('UPDATE `review` SET `review` = :review, `user_id` = :user_id, `entity_id` = :entity_id, `rating` = :rating, `review_title` = :review_title WHERE `review_id` = :review_id');
		$stmt->bindParam(':review', $review, PDO::PARAM_STR);
		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
		$stmt->bindParam(':entity_id', $entity_id, PDO::PARAM_INT);
		$stmt->bindParam(':rating', $rating, PDO::PARAM_INT);
		$stmt->bindParam(':review_title', $review_title, PDO::PARAM_STR);
		$stmt->bindParam(':review_id', $review_id, PDO::PARAM_INT);
		$stmt->execute();
	}
	catch(PDOException $e) {
		echo 'Error: ' . $e->getMessage();
	}
	echo '<div class="alert alert-success alert-dismissible fade show" role="alert" id="reviewChangeSuccess">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h3>Great!</h3><p>You review has been modified.</p><p><button class="btn btn-primary" onClick="document.location.reload(true)"><i class="fas fa-sync"></i> Refresh this page</button></p>
	</div><script>$(document).ready(function(){$(window).scrollTop(0);});</script>';
}
$conn = null;
ob_end_flush();
