<?php
session_start();
$title = 'Website Disclaimer | CryptoCanary';
$description = 'The information is provided by www.cryptocanary.app and whilst we endeavor to keep the information up-to-date and correct, we make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the website or the information, products, services, or related graphics contained on the website for any purpose. Any reliance you place on such information is therefore strictly at your own risk.';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php'); ?>
</head>
<body>
	<?php include('../inc/nav.php'); ?>
	<main class="container pt-3 pb-3">
		<h1>Website Disclaimer</h1>
		<p>EFFECTIVE DATE: JANUARY 10, 2019.</p>
		<h2>Thank you for visiting the Cryptocanary.app.</h2>
		<p>Please note that the information provided on www.cryptocanary.app and other related web properties are for informational purposes only.</p>
		<p>The information is provided by www.cryptocanary.app and whilst we endeavor to keep the information up-to-date and correct, we make no representations or warranties of any kind, express or implied, about the completeness, accuracy, reliability, suitability or availability with respect to the website or the information, products, services, or related graphics contained on the website for any purpose. Any reliance you place on such information is therefore strictly at your own risk.</p>
		<p>In no event will we be liable for any loss or damage including without limitation, indirect or consequential loss or damage, or any loss or damage whatsoever arising from loss of data or profits arising out of or in connection with the use of this website.</p>
		<p>Through this website you are able to link to other websites which are not under the control of www.cryptocanary.app. We have no control over the nature, content and availability of those websites. The inclusion of any links does not necessarily imply a recommendation or endorse the views expressed within them.</p>
		<p>Every effort is made to keep the website up and running smoothly. However, Cryptocanary.app takes no responsibility for and will not be liable for the website being temporarily unavailable due to technical issues within or beyond our control.</p>
		<p>Our Website is provided by Cryptocanary.app on an "AS IS" basis.</p>
		<p>This Website Disclaimer was created by disclaimer-template.com for www.cryptocanary.app</p>
		<p>Cryptocanary.app makes no representations or warranties of any kind, express or implied as to the operation of the site, the information, content, materials or products included on the site.</p>
		<p>To the full extent permissible by applicable law, Cryptocanary.app disclaims all warranties, express or implied, including, but not limited to, the implied warranties and/or conditions of merchantability or satisfactory quality and fitness for a particular purpose and non-infringement.</p>
		<p>Cryptocanary.app will not be liable for any damages of any kind arising from the use of the site, including, but not limited to direct, indirect, incidental, punitive and consequential damages.</p>
		<p>Investing in Cryptocurrencies is inherently risky and could potentially lead to (permanent) huge or even total, monetary losses. You are strongly recommended to consult a professional in legal or financial advice to establish if this type of investing suits your personal and financial situation and risk profile or appetite. We do not recommend any form of investment decision based on the information provided by the content of this Website.</p>
		<p>Cryptocanary.app does not warrant that the site, or the server that makes it available, is free of viruses or other harmful components.</p>
	</main>
	<?php include('../inc/footer.php'); ?>
	<?php include('../inc/endScripts.php'); ?>
</body>
</html>
