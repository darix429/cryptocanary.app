<?php
session_start();
date_default_timezone_set('America/Lima');
$tokenVerif =  md5('cryptocanary'.date('Ymd'));
// sanitization
$email = strtolower(filter_input(INPUT_POST,'email',FILTER_SANITIZE_EMAIL, FILTER_FLAG_EMAIL_UNICODE));
$name = filter_input(INPUT_POST,'name',FILTER_SANITIZE_STRING);
$username = filter_input(INPUT_POST,'username',FILTER_SANITIZE_STRING);
$token = filter_input(INPUT_POST,'token',FILTER_SANITIZE_STRING);

// initial check
if ($token != $tokenVerif) {
	exit ('<div class="alert alert-danger" role="alert"><h3>Error!</h3><p>Something went wrong.</p><p>Try Again</p></div>');
}
if ($token != $tokenVerif) {
	exit ('<div class="alert alert-danger" role="alert"><h3>Error!</h3><p>Something went wrong.</p><p>Try Again</p></div>');
}
if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	exit ('<div class="alert alert-danger" role="alert"><h3>Error!</h3><p>Your email address: '.$email.' is not valid.</p><p>Try Again</p></div>');
}
if (strlen($name) <3) {
	exit ('<div class="alert alert-danger" role="alert"><h3>Error!</h3><p>Your name: '.$name.' need to have more than 3 characters.</p><p>Try Again</p></div>');
}
if (strlen($username) <3) {
	exit ('<div class="alert alert-danger" role="alert"><h3>Error!</h3><p>Your username: '.$username.' need to have more than 3 characters.</p><p>Try Again</p></div>');
}

// check if the username is already taken.
require('../db.php');
try {
	$stmt = $conn->prepare('SELECT `username` FROM `user` WHERE `username` = :username');
	$stmt->bindParam(':username', $username, PDO::PARAM_STR);
	$stmt->execute();
	$count = $stmt->rowCount();
	$stmt->setFetchMode(PDO::FETCH_NUM);
	$data = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
if ($count >0) {
	$conn = null;
	exit ('<div class="alert alert-danger" role="alert"><h3>Error!</h3><p>Username: '.$username.' is already taken.</p><p>Try Again</p></div>');
}
// check if the email is already activated.
try {
	$stmt2 = $conn->prepare('SELECT `email` FROM `user` WHERE `email` = :email');
	$stmt2->bindParam(':email', $email, PDO::PARAM_STR);
	$stmt2->execute();
	$count2 = $stmt2->rowCount();
	$stmt2->setFetchMode(PDO::FETCH_NUM);
	$data = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
if ($count2 >0) {
	$conn = null;
	exit ('<div class="alert alert-danger" role="alert"><h3>Error!</h3><p>An account with the following email address: '.$email.' was already activated.</p><p>Please Login</p></div>');
} else {
	$avatar = 'random-avatar-'.rand(1,19);
	$create_time = date('Y-m-d H:i:s');
	// Create the user account
	try {
		$stmt3 = $conn->prepare("INSERT INTO `user` (`name`, `username`, `email`, `avatar`, `create_time`) VALUES (:name, :username, :email, :avatar, :create_time)");
		$stmt3->bindParam(':name', $name, PDO::PARAM_STR);
		$stmt3->bindParam(':username', $username, PDO::PARAM_STR);
		$stmt3->bindParam(':email', $email, PDO::PARAM_STR);
		$stmt3->bindParam(':avatar', $avatar, PDO::PARAM_STR);
		$stmt3->bindParam(':create_time', $create_time, PDO::PARAM_STR);
		$stmt3->execute();
		$last_id = $conn->lastInsertId();
	}
	catch(PDOException $e) {
		echo 'Error: ' . $e->getMessage();
	}
}

require_once '../class/util.php';
$util = Util::getInstance();
$util->setConnection($conn);

// send welcome email
$referer = "https://cryptocanary.app/profile/";
if (isset($_SESSION["http_referer"]))
$referer = $_SESSION["http_referer"];

$welcome_template = "welcome2.html";
if (strpos($referer, "discover") !== false) {
	$welcome_template = "welcome-via-discover.html";
}
$emailData = array(
	"email"=>$email,
	"template"=>$welcome_template,
	"subject"=>"Welcome to the CryptoCanary Flock! Here's how to get started."
);
$util->queueEmail($emailData);
unset($_SESSION["http_referer"]);

// get profile
$profile = $util->getUserDetailsByEmail($email);
$_SESSION['profile'] = $profile;
$_SESSION['profile']['new'] = true;
$_SESSION['auth_method'] = 'google';
$_SESSION['LoggedIn'] = 'LoggedIn';
$_SESSION['user_id'] = $profile['user_id'];
$_SESSION['name'] = $profile['name'];
$_SESSION['username'] = $profile['username'];
$_SESSION['avatar'] = $profile['avatar'];
$_SESSION['chirps'] = $profile['score'];

$outputFile = 'authentication.log';
$log = time().', '.$profile['user_id'].', google'.PHP_EOL;
file_put_contents($outputFile,$log,FILE_APPEND);

$conn = null;
$path = parse_url($referer)["path"];
$split = explode("/", $path);
$validRedirects = ["review", "profile", "user", "browse"];
if (!in_array($split[1], $validRedirects)) {
	$referer = "https://cryptocanary.app/profile/";
}

header("Location: $referer");