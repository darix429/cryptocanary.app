<?php
session_start();
date_default_timezone_set('America/Lima');
$now = date('Y-m-d H:i:s');
$outputFile = '../logs/votes.txt';
$log = json_encode($_GET).PHP_EOL;
file_put_contents($outputFile,$log,FILE_APPEND);
$vote = filter_input(INPUT_GET,'vote',FILTER_SANITIZE_STRING);
$review_id = filter_input(INPUT_GET,'review_id',FILTER_SANITIZE_NUMBER_INT);
$entity_name = filter_input(INPUT_GET,'entity_name',FILTER_SANITIZE_STRING);
$entity_id = filter_input(INPUT_GET,'entity_id',FILTER_SANITIZE_NUMBER_INT);
$iNumber = filter_input(INPUT_GET,'iNumber',FILTER_SANITIZE_NUMBER_INT);
if (!isset($_SESSION['user_id'])) {
	$error = 'You must login first!';
	echo '<div class="alert alert-danger alert-dismissible fade show" role="alert" id="voteError">
	     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     <p>'.$error.'</p><p><a href="/login/" class="btn btn-primary">Login here</a></p>
	     </div>';
	include('reviewVoting.php');
	exit();
}
$user_id = $_SESSION['user_id'];
if ($_SESSION['user_id'] == 'LoggedIn') {
	echo '<div id="result'.$iNumber.'"><div class="alert alert-danger alert-dismissible fade show" role="alert" id="voteError">
	     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     <p>There was an error somewhere.</p>
	     </div>';
	include('reviewVoting.php');
	exit();
}
if ($vote == 'up') {
	$vote_type = 1;
}
if ($vote == 'down') {
	$vote_type = 0;
}
require('../db.php');
// Check if the user has 15 chirps
try {
	$stmt = $conn->prepare('SELECT `score` FROM `user` WHERE user_id = :user_id');
	$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_NUM);
	$data = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$score = $data[0][0];
if ($score < 15) {
	$conn = null;
	$error = 'You need at least 15 chirps to vote. Currently you only have: '.$score.' chirps!';
	file_put_contents($outputFile,$now.','.$error.PHP_EOL,FILE_APPEND);
	echo '<div id="result'.$iNumber.'"><div class="alert alert-danger alert-dismissible fade show" role="alert" id="voteError">
	     <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     <p>'.$error.'</p>
	     </div>';
	include('reviewVoting.php');
	exit();
}
// check if the user has already voted for this review
try {
	$stmt = $conn->prepare('SELECT `vote_id` FROM `vote` WHERE `review_id` = :review_id AND user_id = :user_id');
	// Bind Parameter
	$stmt->bindParam(':review_id', $review_id, PDO::PARAM_INT);
	$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
	$stmt->execute();
	$count = $stmt->rowCount();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$data = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
// if the user already has voted for this review then make an update of his previous vote
if ($count > 0) {
	$vote_id = $data[0]['vote_id'];
	try {
		$stmt = $conn->prepare('UPDATE `vote` SET `vote_type` = :vote_type WHERE `vote_id` = :vote_id');
		$stmt->bindParam(':vote_type', $vote_type, PDO::PARAM_INT);
		$stmt->bindParam(':vote_id', $vote_id, PDO::PARAM_INT);
		$stmt->execute();

		// echo a message to say the UPDATE succeeded
		$success= $count.' records UPDATED successfully';
		file_put_contents($outputFile,$now.', '.$success.PHP_EOL,FILE_APPEND);
		echo '<div id="result'.$iNumber.'"><div class="alert alert-success alert-dismissible fade show" role="alert" id="voteSuccess">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		<p>Your vote has been updated.</p>
		</div>';
		include('reviewVoting.php');
		exit();
	}
	catch(PDOException $e) {
		echo 'Error: '.$e->getMessage();
	}
}
// first time the user vote for this entity, make an new entry in the system
else {
	try {
		$stmt = $conn->prepare('INSERT INTO `vote` (`review_id`, `user_id`, `vote_type`) VALUES (:review_id, :user_id, :vote_type)');
		$stmt->bindParam(':review_id', $review_id, PDO::PARAM_INT);
		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
		$stmt->bindParam(':vote_type', $vote_type, PDO::PARAM_INT);
		$stmt->execute();
		$last_id = $conn->lastInsertId();
		$success = 'New record created successfully. Last inserted ID is: '.$last_id;
		file_put_contents($outputFile,$now.', '.$success.PHP_EOL,FILE_APPEND);
	}
	catch(PDOException $e) {
		echo 'Error: ' . $e->getMessage();
	}
	echo '<div id="result'.$iNumber.'"><div class="alert alert-success alert-dismissible fade show" role="alert" id="voteSuccess">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<p>Your vote has been saved.</p>
	</div>';
	include('reviewVoting.php');
}
$conn = null;
