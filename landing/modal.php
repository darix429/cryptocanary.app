<!-- The Modal -->
<div class="modal" id="myModal">
	<div class="modal-dialog" style="max-width: 600px;">
		<div class="modal-content">
			<!-- Modal Header -->
			<div class="modal-header" style="background-color: #f3b333;">
				<h4 class="modal-title">Welcome Product Hunters!</h4>
				<button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
			<!-- Modal body -->
			<div class="modal-body">
				<p>We appreciate you!</p>
				<p>If you sign up within 48 hours of the launch, we'll enter you into a drawing to win $100 in ETH.</p>
				<p>Also, we'll mail the first 50 U.S.-based sign ups a free CryptoCanary laptop sticker too.</p>
				<p><a href="/signup/?ref=producthunt" class="btn btn-primary pl-5 pr-5">Join</a></p>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">No thanks</button>
			</div>
		</div>
	</div>
</div>
