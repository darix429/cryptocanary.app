<?php
session_start();
$user_id = $_SESSION['user_id'];
if ($user_id < 1) {
	header('Location: ../login/');
	exit();
}

// initial Setup
define ('SITE_ROOT', '/var/www/html/');
$target_dir = SITE_ROOT.'images/avatars/'.$user_id;

$uploadOk = 1;

// get the fileType
$fileType = $_FILES['fileToUpload']['type'];
switch ($fileType) {
	case 'image/png':
	$extension = '.png';
	break;

	case 'image/jpg':
	$extension = '.jpg';
	break;

	case 'image/jpeg':
	$extension = '.jpg';
	break;

	case 'image/gif':
	$extension = '.gif';
	break;

	default:
	$extension = '';
	$uploadOk = 0;
	break;
}
$avatar = time().$extension;
$target_file = $target_dir.'/'.$avatar;
$filesize = $_FILES['fileToUpload']['size'];
$maxFileSize = 5*1024*1024;
if ($filesize > $maxFileSize) {
	echo 'Your file is too big.';
}
// check if directory exist, if not create the directory
if (!is_dir($target_dir)) {
	mkdir($target_dir);
}
if (strlen($extension)<1) {
	echo 'Your File type is not recognized. It should be JPEG, PNG or GIF file.';
}
// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0) {
	echo 'Sorry, your file was not uploaded.';
// if everything is ok, try to upload file
} else {
	$moved = move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);
	if($moved) {
		echo 'Successfully uploaded';
		require('../db.php');
		$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		try {
			$stmt = $conn->prepare('UPDATE `user` SET `avatar` = :avatar WHERE `user_id` = :user_id');
			$stmt->bindParam(':avatar', $avatar, PDO::PARAM_STR);
			$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
			$stmt->execute();
			$count = $stmt->rowCount();
			// echo a message to say the UPDATE succeeded
			echo $user_id.'-'.$avatar.' - '.$count.' records UPDATED successfully';
		}
		catch(PDOException $e) {
			echo 'Error: '.$e->getMessage();
		}
		$conn = null;
	} else {
		echo "Not uploaded because of error #".$_FILES["fileToUpload"]["error"];
	}
}
header('Location: index.php?success=success');
