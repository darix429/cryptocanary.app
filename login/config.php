<?php

require_once '../vendor/autoload.php';
require_once '../db.php';
require_once '../class/util.php';
require_once '../class/user.php';

$util = Util::getInstance();
$util->setConnection($conn);

$googleKey = '582327992876-4urvj5nhvsrqroi0vfan2qaolvg9kcvf.apps.googleusercontent.com';
$googleSecret = '1BCSAYjKEYcAX0QgpRruagHN';
$googleCallback = 'https://cryptocanary.app/login/cb.google.php';

$fbKey = '1396440270491537';
$fbSecret = '947137e47bb8d81a341234f5becc54af';
$fbCallback = 'https://cryptocanary.app/login/cb.fb.php';

$google = new \League\OAuth2\Client\Provider\Google([
    'clientId'     => $googleKey,
    'clientSecret' => $googleSecret,
    'redirectUri'  => $googleCallback
]);

$facebook = new \League\OAuth2\Client\Provider\Facebook([
    'clientId'          => $fbKey,
    'clientSecret'      => $fbSecret,
    'redirectUri'       => $fbCallback,
    'graphApiVersion'   => 'v3.2',
]);


$googleAuthUrl = $google->getAuthorizationUrl();
$fbAuthUrl = $facebook->getAuthorizationUrl([
    "scope"=>["email"]
]);