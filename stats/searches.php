<?php
session_start();
if ($_SESSION['ccstatistics'] != 'LoggedIn') {
	header('Location: login.php');
}
$datasets = array();
$dates = array();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('head.php'); ?>
</head>
<body>
	<?php include('nav.php'); ?>
	<main class="container">
		<h1 class="pt-3">Searches per day in the last 30 days</h1>
		<canvas id="myChart"></canvas>
		<hr>
		<table class="table table-striped table-bordered" id="tableData">
			<thead>
				<tr>
					<th>Date</th>
					<th>Searches / day</th>
				</tr>
			</thead>
			<tbody>
				<?php
				for ($x = 30; $x > 0; $x--) {
					$date = date('Y-m-d', strtotime('today - '.$x.' days'));
					array_push($dates, $date);
					$output = (int)trim(shell_exec('grep "'.$date.'" /var/www/html/logs/route.log | wc -l'));
					array_push($datasets, $output);
					echo '<tr><td>'.$date.'</td><td>'.number_format($output).'</td></tr>'.PHP_EOL;
				}
				?>
			</tbody>
		</table>
		<hr>
		<p><a href="../logs/route.log" class="btn btn-danger">Click-here to download the raw data</a></p>
	</main>
	<?php include('endScripts.php'); ?>
	<script type="text/javascript">
		var ctx = document.getElementById('myChart').getContext('2d');
		var myChart = new Chart(ctx, {
			type: 'line',
			data: {
				labels: [<?php foreach ($dates as $key => $datestamp) {
					echo '\''.$datestamp.'\', ';
				} ?>],
				datasets: [{
					label: 'searches per day',
					borderColor: "#2999ff",
					backgroundColor: "rgba(41,153,255,0.5)",
					data: [<?php foreach ($datasets as $key => $dataset) {
						echo $dataset.', ';
					} ?>]
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							min:0,
							max:<?php echo max($datasets)+1000; ?>
						}
					}]
				}
			}
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tableData').DataTable( {
				"pageLength": 50,
				"order": [[ 0, "desc" ]]
			});
		});
	</script>
</body>
</html>
