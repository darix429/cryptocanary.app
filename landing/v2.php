<?php
session_start();
require('db.php');
// top 10 risky
try {
	$stmt = $conn->prepare('SELECT entity_id, entity_name, symbol, entity_desc, overall_rating, links, image FROM entity WHERE approved_flag = 1 AND LENGTH(entity_desc)> 1 ORDER BY overall_rating DESC LIMIT 4');
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$data = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
// top 10 reviews
try {
	$stmt2 = $conn->prepare('SELECT review_id, review.user_id, review.entity_id, review, review_title, rating, avatar, username, overall_rating, entity_name, symbol, image FROM review INNER JOIN `user` ON `review`.user_id = `user`.user_id INNER JOIN `entity` ON `review`.entity_id = `entity`.entity_id WHERE entity.approved_flag = 1 AND review IS NOT NULL ORDER BY review_id DESC LIMIT 3');
	$stmt2->execute();
	$stmt2->setFetchMode(PDO::FETCH_ASSOC);
	$review = $stmt2->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$conn = null;

$title = 'Crypto Reviews | Shady or Legit? | CryptoCanary';
$description = 'CryptoCanary consolidates community reviews and research to help beginners avoid crypto scams. Submit, rate, and review crypto projects to get started!';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('inc/head.php'); ?>
</head>
<body>
	<?php
	if (strpos(__DIR__,'/var/www/html/') !== false) {include('/var/www/html/inc/googleTag.php');}
	?>
	<div id="Map">
		<?php include('inc/landingNav.php'); ?>
		<div id="intro">
			<div class="container text-center">
				<div class="row justify-content-center">
					<div class="col-12 col-md-8 col-lg-6 pt-5">
						<h1 style="font-weight:800">SICK of all the scams hurting the crypto world?</h1>
						<p class="pt-3"><strong>Help clean them up with your reviews!</strong></p>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-12 col-md-8 col-lg-6 pt-5 pb-5">
						<form action="search.php" accept-charset="UTF-8" method="POST" role="FORM">
							<div class="input-group">
								<input list="cryptocurrency" name="search" id="search" placeholder="e.g. Bitcoin (BTC)" class="form-control searchText">
								<?php include('data/datalist.html'); ?>
								<span class="input-group-btn">
									<button type="submit" class="btn btn-primary searchBT"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
								</span>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<main class="container pt-3 pb-3">
		<div id="top10review">
			<h2>Latest Reviews:</h2>
			<div class="row">
				<div class="col-md-6 col-xl-3">
					<p>These reviews are making a difference</p>
					<p class="small">Contribute in making cryptocurrencies a safe space for everyone.</p>
					<p><a href="/browse/" class="btn btn-primary">View more</a></p>
				</div>
				<?php
				foreach ($review as $key => $row) {
					$starRating = '';
					$rating = (int)$row['rating'];
					for ($i=0; $i <= $rating; $i++) {
						$starRating .= '<i class="fas fa-star"></i>';
					}
					// var_dump($data);
					$overall_rating = $row['overall_rating'];
					$confidence = "N/A";
					if ($overall_rating != null) {
						$confidence = number_format((($overall_rating)), 0);
						if ($confidence <= 25) {$pbcolor = 'alert-danger';}
						if ($confidence > 25 AND $confidence <=50) {$pbcolor = 'alert-warning';}
						if ($confidence > 50 AND $confidence <=75) {$pbcolor = 'alert-info';}
						if ($confidence > 75) {$pbcolor = 'alert-success';}
						$confidence .= "%";
					} else {
						$pbcolor = "";
					}
					$user_id = $row['user_id'];
					$avatar = $row['avatar'];
					// clean the avatar picture
					$randomAvatar = '/images/avatars/random/avatar'.rand(1,30).'.svg';
					if(strpos($avatar, 'random') !== false) { $avatar = $randomAvatar;}
					elseif(strlen($avatar) <1) { $avatar = $randomAvatar;}
					else { $avatar = '/images/avatars/'.$user_id.'/'.$avatar;}
					echo '<div class="col-md-6 col-xl-3">
					<table style="width:100%"><tr><td width="50px;"><img src="/images/entities/'.$row['entity_id'].'/'.$row['image'].'" alt="'.$row['entity_name'].'" class="img-fluid" style="height: 30px;"></td><td><strong>'.$row['entity_name'].' ('.$row['symbol'].')</strong></td><td class="'.$pbcolor.' text-center" width="50px;">'.$confidence.'</td></tr></table>
					<p class="alert alert-secondary text-justify"><small><i>'.substr($row['review'],0,120).' ...</i><br><a href="/review/'.urlencode(trim($row['entity_name'])).'">Read full review...</a></small></p>
					<table style="width:100%"><tr>
					<td><img src="'.$avatar.'" class="img-fluid" alt="avatar" style="height: 30px;"></td>
					<td><strong>'.$row['username'].'</strong>: '.$starRating.'</td>
					</tr></table>
					</div>';
				}
				?>
			</div>
		</div>
		<div id="top10Risky">
			<h2>Riskiest cryptocurrencies</h2>
			<?php
			$count=0;
			foreach ($data as $key => $row) {
				$rating = $row['overall_rating'];
				$confidence = "N/A";
				if($rating != null) {
					$confidence = number_format($rating, 0);
					if ($confidence <= 25)  {$pbcolor = 'alert-danger'; }
					if ($confidence > 25 AND $confidence <=50) {$pbcolor = 'alert-warning';}
					if ($confidence > 50 AND $confidence <=75) {$pbcolor = 'alert-info';}
					if ($confidence > 75) {$pbcolor = 'alert-success';}
					$confidence .="%";
				} else {
					$pbcolor="";
				}
				$json = json_decode($row['links'], true);
				// $link = strtolower($json[0]['link']);
				$count++;
				if ($count % 4 == 1){echo '<div class="row pb-3 pt-3">';}
				echo '<div class="col-md-6 col-xl-3">
				<div class="media">
				<div class="media-body">
				<table style="width:100%"><tr><td><img src="/images/entities/'.$row['entity_id'].'/'.$row['image'].'" alt="'.$row['entity_name'].'" class="img-fluid" style="height: 30px;"></td><td><strong>'.$row['entity_name'].' ('.$row['symbol'].')</strong></td><td class="'.$pbcolor.' text-center">'.$confidence.' </td></tr></table>
				<p class="alert alert-secondary text-justify"><small><i>'.substr($row['entity_desc'],0,200).'...</i></small><br><small><a href="/review/'.urlencode(trim($row['entity_name'])).'">Read full review...</a></small></p>
				<table style="width:100%; font-size: 80%;"><tr><td><i class="fas fa-calendar-alt"></i> 2018</td><td>ICO: <i class="fas fa-check text-success"></i></td><td>FORK: <i class="fas fa-times text-danger"></i></td></tr></table>
				</div>
				</div>
				</div>';
				if ($count % 4 == 0){echo '</div>';}
			}
			?>
		</div>
		<div id="browse">
			<h2 class="text-center pb-3" style="color: #222;">Browse by category</h2>
			<p class="text-center"><a href="/browse/" style="color:#00a2ee;">View All</a></p>
			<div class="row pt-3">
				<div class="col-4 text-center">
					<p><a href="/browse/" style="color:#00a2ee;"><i class="fab fa-ethereum fa-8x" aria-hidden="true"></i></a></p>
					<p><a href="/browse/" style="color:#00a2ee;">Browse Token</a></p>
				</div>
				<div class="col-4 text-center">
					<p><a href="/browse/" style="color:#00a2ee;"><i class="fab fa-btc fa-8x"></i></a></p>
					<p><a href="/browse/" style="color:#00a2ee;">Browse coins</a></p>
				</div>
				<div class="col-4 text-center">
					<p><a href="/browse/" style="color:#00a2ee;"><i class="fas fa-coins fa-8x"></i></a></p>
					<p><a href="/browse/" style="color:#00a2ee;">Browse ICO's</a></p>
				</div>
			</div>
		</div>
	</main>
	<?php include('inc/telegram.php'); ?>
	<?php include('inc/footer.php'); ?>
	<?php include('inc/endScripts.php'); ?>
</body>
</html>
