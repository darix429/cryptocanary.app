<?php
session_start();
$all = filter_input(INPUT_GET,'id',FILTER_SANITIZE_STRING);
$typeID = filter_input(INPUT_GET,'type',FILTER_SANITIZE_NUMBER_INT);
switch ($typeID) {
	case '7':
		$typeClause = 'WHERE `entity_category_id` = 7';
		$typeText = 'Exchanges';
		break;
	case '8':
		$typeClause = 'WHERE `entity_category_id` = 8';
		$typeText = 'Wallets';
		break;
	default:
		$typeClause = 'WHERE `review_count` > 0 AND `approved_flag` = 1 AND `entity_category_id` = 1';
		$typeText = 'Coins';
		break;
}
if ($all == 'all') {
	$where = 'WHERE approved_flag = 1 AND entity_id = 1';
}
require('../db.php');
try {
	$stmt = $conn->prepare("SELECT entity_name, entity_id, image, symbol, latest_review_time, industry, overall_rating, review_count FROM entity $typeClause ORDER BY `latest_review_time` DESC");
	$stmt->execute();
	$count = $stmt->rowCount();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$data = $stmt->fetchAll();
	// $stmt->debugDumpParams();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$conn = null;
$title = 'Browse '.$typeText.' reviews | Cryptocanary';
$description = 'Browse list of all cryptocurrency ratings and reviews. Categories include coins, tokens, ICOs, exchanges, wallets, influencers, tools, projects, and more!';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php'); ?>
</head>
<body>
	<?php include('../inc/nav.php'); ?>
	<div class="greyBG">
		<div class="container">
			<?php
			if (!empty($_SESSION['name'])) {
				echo '<h2>Hello '.$_SESSION['name'].'</h2><hr>';
			}
			include_once "../inc/notification.php";
			?>
			<h1 class="text-center pt-5 pb-5">Browse <?php echo $typeText ; ?> With Reviews</h1>
			<?php if ($count == 0){ echo '<h1 class="text-center">Coming soon...</h1>'; }?>
		</div>
	</div>
	<main class="container pt-3 pb-3">
		<p class="text-right">Show: <a href="/browse/all.php">All Coins</a> / <a href="/browse/">Coins with Reviews</a></p>
		<hr>

		<table id="table" class="display nowrap" style="width:100%">
			<thead><tr><th data-priority="1">#</th><th data-priority="2">Name</th><th data-priority="4">Symbol</th><th data-priority="6">Latest Review</th><th data-priority="3">Confidence</th><th data-priority="5">Reviews</th></tr></thead>
			<tbody>
				<?php
				foreach ($data as $key => $row) {
					$rating = $row['overall_rating'];
					$confidence = "N/A";
					if($rating != null){
						$confidence = number_format((($rating)), 0);
						if ($confidence <= 25) {$pbcolor = 'alert-danger';}
						if ($confidence > 25 AND $confidence <=50) {$pbcolor = 'alert-warning';}
						if ($confidence > 50 AND $confidence <=75) {$pbcolor = 'alert-info';}
						if ($confidence > 75) {$pbcolor = 'alert-success';}
						$confidence.="%";
					} else {
						$pbcolor="";
					}
					$score = number_format((4-$rating)/4 * 100,0);
					echo '<tr><td style="width: 100px;"><a href="/review/'.urlencode($row['entity_name']).'"><img style="width: 50px;" class="img-fluid d-none d-sm-block" src="/images/entities/'.$row['entity_id'].'/'.$row['image'].'" alt="'.$row['entity_name'].'"></a></td><td><a href="/review/'.urlencode($row['entity_name']).'">'.$row['entity_name'].'</a></td><td>'.$row['symbol'].'</td><td>'.date('Y-m-d',strtotime($row['latest_review_time'])).'</td><td class="'.$pbcolor.' text-right">'.$confidence.'</td><td class="text-right">'.$row['review_count'].'</td></tr>';
				}
				?>
			</tbody>
		</table>
	</main>
	<?php include('../inc/telegram.php') ?>
	<?php include('../inc/footer.php') ?>
	<?php include('../inc/endScripts.php') ?>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#table').DataTable( {
				"responsive": true,
				"pageLength": 50,
				"order": [[ 3, "desc" ]]
			});
		});
	</script>
</body>
</html>
