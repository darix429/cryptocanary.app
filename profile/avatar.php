<?php
session_start();
$user_id = $_SESSION['user_id'];
if ($user_id < 1) {
	header('Location: ../login/');
	exit();
}
$error = filter_input(INPUT_GET,'error',FILTER_SANITIZE_STRING);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php'); ?>
</head>
<body>
	<?php include('../inc/nav.php'); ?>
	<?php if (strlen($error)>1) {
		echo '<div class="alert alert-danger" role="alert"><h3>Error!</h3><p>'.$error.'</p></div>';
	} ?>
	<main class="container pt-5 pb-5">
		<div class="text-center"><img src="/img/canary-white.png" style="width: 400px;" class="img-fluid" alt="CryptoCanary logo"></div>
		<h1>Change your Avatar:</h1>
		<div id="alertFileSize" style="display:none">
			<div class="alert alert-danger" role="alert">
				<h3>Error!</h3><p>Sorry you file size has exceed the 2 MB size limit.<br>Try again with a smaller file.</p>
			</div>
		</div>
		<form action="uploadAvatar.php" method="post" enctype="multipart/form-data">
			<p>Select image to upload:</p>
			<p><input type="file" name="fileToUpload" id="fileToUpload" class="form-control-file" accept="image/*" required="required" onchange="ValidateSize(this)"></p>
			<small id="fileHelp" class="form-text text-muted">Only JPG, PNG or GIF of a maximum of 2MB are allowed.</small>
			<div class="pt-3 pb-3"><input type="submit" value="Upload Image" name="submit" class="btn btn-primary"></div>
		</form>
	</main>
	<?php include('../inc/footer.php'); ?>
	<?php include('../inc/endScripts.php'); ?>
	<script type="text/javascript">
		function ValidateSize(file) {
			var FileSize = file.files[0].size / 1024 / 1024;
			if (FileSize > 2) {
				$('#alertFileSize').show();
			}
		}
	</script>
</body>
</html>
