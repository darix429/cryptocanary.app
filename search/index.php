<?php
$search= filter_input(INPUT_GET,'search',FILTER_SANITIZE_STRING);
$result = explode('(', $search);
$entity_name = urlencode($result['0']);
header("Location: ../review/$entity_name");
