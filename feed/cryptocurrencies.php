<?php
date_default_timezone_set('America/Lima');
$output = '<?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
xmlns:content="http://purl.org/rss/1.0/modules/content/"
xmlns:wfw="http://wellformedweb.org/CommentAPI/"
xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:atom="http://www.w3.org/2005/Atom"
xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
>'.PHP_EOL;
$output .='<channel>
<title>CryptoCanary Latest Coins</title>
<atom:link href="https://cryptocanary.app/feed/cryptocurrency.xml" rel="self" type="application/rss+xml" />
<link>https://cryptocanary.app/</link>
<lastBuildDate>'.date(DATE_RFC2822).'</lastBuildDate>
<language>en-US</language>
<description>CryptoCanary consolidates community reviews and research to help beginners avoid crypto scams. Submit, rate, and review crypto projects to get started!</description>'.PHP_EOL;
require ('/var/www/html/db.php');
try {
	$stmt = $conn->prepare('SELECT entity_id, entity_name, symbol, industry, country, image, `year`, links, create_time, update_time FROM `entity` WHERE approved_flag = 1 ORDER BY update_time DESC LIMIT 50');
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$data = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$conn = null;
foreach ($data as $key => $row) {
	$json = json_decode($row['links'],true);
	$output .='<item>'.PHP_EOL;
	$output .='<title>'.$row['entity_name'].' - '.$row['symbol'].'</title>'.PHP_EOL;
	$output .='<link>https://cryptocanary.app/review/'.urlencode(trim($row['entity_name'])).'</link>'.PHP_EOL;
	$output .='<pubDate>'.date(DATE_RFC2822, strtotime($row['create_time'])).'</pubDate>'.PHP_EOL;
	if (!empty($row['description'])) {
		$output .='<description>'.$row['description'].'</description>'.PHP_EOL;
	}
	$output .='<guid>https://cryptocanary.app/review/'.urlencode(trim($row['entity_name'])).'</guid>'.PHP_EOL;
	$output .='</item>'.PHP_EOL;
}
$output .='</channel>'.PHP_EOL.'</rss>';
$filename = '/var/www/html/feed/cryptocurrency.xml';
file_put_contents($filename, $output);
