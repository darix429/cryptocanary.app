<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/intercooler-js/1.2.2/intercooler.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/autolinker/3.0.5/Autolinker.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.6/js/star-rating.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.6/themes/krajee-svg/theme.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/jquery.easy-autocomplete.min.js"></script>
<script type="text/javascript">
	var options = {
		url: "/data/coins.json",
		getValue: "name",
		cssClasses: "coinList",
		adjustWidth: false,
		template: {
			type: "iconRight",
			fields: {
				iconSrc: "icon"
			}
		},
		list: {
			match: {
		    	enabled: true
		    },
			maxNumberOfElements: 10,
			onChooseEvent: function() {
				var value = $("#coinList").getSelectedItemData();
				var name = value['name'];
				console.log(name);
				window.location.href='/search.php?search='+name;
			}
		},
		theme: "square"
	};
	$("#coinList").easyAutocomplete(options);
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js"></script>
<?php
if (strpos(__DIR__,'/var/www/html') === 0) {
	echo '
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.cookieBar/0.0.3/jquery.cookieBar.min.js"></script>
<script type="text/javascript">
$(".cookie-message").cookieBar({ closeButton: ".cbc" });
</script>
<script type="text/javascript">
// This is the "Offline page" service worker
// Check compatibility for the browser we are running this in
if ("serviceWorker" in navigator) {
	if (navigator.serviceWorker.controller) {
		console.log("[PWA Builder] active service worker found, no need to register");
	} else {
		// Register the service worker
		navigator.serviceWorker
		.register("/pwabuilder-sw.js", {
			scope: "./"
		})
		.then(function (reg) {
			console.log("[PWA Builder] Service worker has been registered for scope: " + reg.scope);
		});
	}
}
</script>';
}
