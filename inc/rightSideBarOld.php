<div class="col-md-4">
	<h4>Market information:</h4>
	<?php
	$csv = array_map('str_getcsv', file('data/coinList.csv'));
	foreach ($csv as $key => $value) {
		if ($value[2] == $symbol) {
			$coinloreID = $value[0];
		}
	}
	if ($coinloreID > 1) {
		$coinloreURL = 'https://api.coinlore.com/api/ticker/?id='.$coinloreID;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $coinloreURL);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);
		curl_close($ch);
		$coinloreData = json_decode($response,true);
		if (!empty($coinloreData)) {
			$coinloreSymbol = $coinloreData[0]["symbol"];
			$coinlorePrice_usd = '$ '.$coinloreData[0]["price_usd"];
			$coinlorePercent_change_24h = $coinloreData[0]["percent_change_24h"].' %';
			$coinlorePercent_change_1h = $coinloreData[0]["percent_change_1h"].' %';
			$coinloreMarket_cap_usd = (float)$coinloreData[0]["market_cap_usd"];
			$coinloreVolume_24 = (float)$coinloreData[0]["volume24"];
			// $coinloreVolume_24a = (float)$coinloreData[0]["volume24a"];
			if ($coinloreMarket_cap_usd < 1) {
				$coinloreMarket_cap_usd = 'N/A';
			} else {
				$coinloreMarket_cap_usd = '$ '.number_format((float)$coinloreData[0]["market_cap_usd"],2);
			}
		}
		else {
			$coinlorePrice_usd = $coinlorePercent_change_24h = $coinlorePercent_change_1h = $coinloreMarket_cap_usd = 'N/A';
		}
	} else {
		$coinlorePrice_usd = $coinlorePercent_change_24h = $coinlorePercent_change_1h = $coinloreMarket_cap_usd = 'N/A';
	}
	?>
	<table width="100%">
		<tr><td>Symbol:</td><td><?php echo $symbol; ?></td></tr>
		<tr><td>Price (USD):</td><td><?php echo $coinlorePrice_usd; ?></td></tr>
		<!-- <tr><td>Percent change (24H):</td><td><?php echo $coinlorePercent_change_24h; ?></td></tr>
		<tr><td>Percent change (1H):</td><td><?php echo $coinlorePercent_change_1h ; ?></td></tr> -->
		<tr><td>Market Cap (USD):</td><td><?php echo $coinloreMarket_cap_usd; ?></td></tr>
		<tr><td>24h Volume:</td><td><?php echo '$ ' . number_format($coinloreVolume_24, 2); ?></td></tr>
		<?php if($entity[0]["exchange_count"] > 0) { ?>
			<tr><td>Exchanges available on:</td><td><?php echo $entity[0]["exchange_count"]; ?></td></tr>
		<?php } ?>
	</table>
	<hr>
	<h4>General information:</h4>
	<ul>
		<?php
		foreach ($links as $key => $value) {
			if (is_array($value)) {
				$value = $value[0];
			}
			echo '<li><a href="'.trim(strtolower($value)).'" target="_blank"><i class="fab fa-'.trim(strtolower($key)).'"> '.trim(ucfirst($key)).'</i></a></li>';
		}
		?>
	</ul>
	<hr>
	<p><strong>See something missing?</strong><br><a href="https://docs.google.com/forms/d/e/1FAIpQLScUz2r0yuXNp7cKOre1fte_Ip0m4sOOdf7uxMe96f66VpXdLw/viewform">Help us fill out additional details!</a></p>
	<hr>
	<p><a href="/advertise/" id="advertiseWithUs"><img src="/advertise/400.png" class="img-fluid" alt="Place your advertising here"></a></p>
</div>
