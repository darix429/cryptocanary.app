<?php
require('discover/init.php');
$sort = filter_input(INPUT_GET,'sort',FILTER_SANITIZE_STRING);
switch ($sort) {
	case 'upvote':
	$sorting = '`product_review`.upvote_tally DESC';
	break;
	case 'downvote':
	$sorting = '`product_review`.downvote_tally DESC';
	break;
	case 'date';
	$sorting = '`product_review`.update_time DESC';
	break;
	case 'latest';
	$sorting = '`product_review`.update_time DESC';
	break;
	case 'earliest';
	$sorting = '`product_review`.update_time ASC';
	break;
	default:
	$sorting = '`product_review`.review_id DESC';
	break;
}
if (!isset($product_id )) {
	$product_id = filter_input(INPUT_GET,'productID',FILTER_SANITIZE_NUMBER_INT);
}
if ($product_id < 6549) {
	exit();
}
// Get product information
try {
	$stmt = $conn->prepare('SELECT * FROM product INNER JOIN entity_category ON product.category = entity_category.entity_category_id WHERE product_id = :product_id LIMIT 1');
	// Bind Parameter
	$stmt->bindParam(':product_id', $product_id, PDO::PARAM_INT);
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$productData = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
// Get review informations about this product
try {
	$stmt = $conn->prepare("SELECT product_review.review_id, product_review.user_id, product_review.product_id, product_review.review, product_review.rating, product_review.upvote_tally, product_review.downvote_tally,product_review.comments,product_review.update_time,`user`.username,`user`.score,`user`.avatar FROM product_review INNER JOIN `user` ON product_review.user_id = `user`.user_id  WHERE `product_review`.product_id = :product_id AND `product_review`.delete_time IS NULL ORDER BY $sorting");
	// Bind Parameter
	$stmt->bindParam(':product_id', $product_id, PDO::PARAM_INT);
	$stmt->execute();
	$totalReviews = $stmt->rowCount();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$reviews = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}

// get the username/user_id Lookup information
try {
	$stmt3 = $conn->prepare('SELECT `user_id`,`username` FROM `user` ORDER BY `user_id` ASC');
	$stmt3->execute();
	$stmt3->setFetchMode(PDO::FETCH_ASSOC);
	$lookupTable = $stmt3->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$conn = null;
if (!isset($productData['0']['overall_rating'])) {
	$confidence = $overall_rating = 0;
} else {
	$confidence = $overall_rating = $productData['0']['overall_rating'];
}
$overall_stars = round($overall_rating/25+1, 1);
$product_name = $productData['0']['product_name'];
$websiteURL =  $productData['0']['website'];
$links = $productData['0']['product_links'];
$logo = '/images/products/'.$product_id.'/'.$productData['0']['logo'];
$image = '/images/products/'.$product_id.'/'.$productData['0']['image'];
$short_desc = $productData['0']['short_desc'];
$product_desc = $productData['0']['product_desc'];
$category = $productData['0']['category'];
$review_count = $productData['0']['review_count'];
// SEO
$title = $product_name.' - '.$category.' Reviews - Read Opinions About '.$product_name.'!';
$description = 'View '.$product_name.' reviews and overall rating score. Use our info and your own research to determine if '.$product_name.' is worth trying out.';
$img = 'https://cryptocanary.app'.$image;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<script type="text/javascript">
		window.dataLayer = window.dataLayer || [];
		window.dataLayer.push({
			'event': 'formSubmission',
			'formType': 'Search',
			'searchText': '<?php echo $product_name;?>',
			'formPosition': 'NavBar'
		});
	</script>
	<?php include('inc/head.php'); ?>
</head>
<body>
	<?php include('nav.php'); ?>
	<div class="greyBG">
		<div class="container pt-5">
			<?php
			include('inc/notification.php');
			if (isset($message)) {
				echo '<div class="alert alert-success" role="alert" id="reviewAddSuccess">'.$message.'</div>';
			}
			if ($confidence>50) {
				// echo '<p class="text-center"><i class="fas fa-check success"></i> Trusted Project</p>';
			}
			?>
			<div class="row align-items-center">
				<div class="d-md-flex align-items-center">
					<div class="pl-1 pr-1">
						<table>
							<tr><td><img src="<?php echo $logo; ?>" alt="<?php echo $product_name; ?>" class="img-fluid" style="width: 50px; height:auto;"></td><td><h1><?php echo $product_name;?></h1></td></tr>
						</table>
					</div>
					<div class="pl-1 pr-1">
						<table>
							<tr><td><input id="average_team_quality" class="kv-rtl-theme-svg-star2 rating-loading" value="<?php echo $overall_stars; ?>" data-size="sm"></td><td><strong><?php echo number_format($overall_rating,0); ?></strong>% - <?php echo $totalReviews;?> review<?php if ($totalReviews !=1) { echo 's';} ?></td></tr>
						</table>
					</div>
				</div>
			</div>
			<div class="row align-items-center">
				<div class="d-sm-flex flex-fill">
					<div class="pl-2 pt-2">
						<p><?php echo $short_desc; ?></p>
						<p>Category: <strong><?php echo $category; ?></strong></p>
						<p><a href="<?php echo $websiteURL.'?ref=cryptocanary'; ?>"  target="_blank" class="text-dark"><?php echo $websiteURL; ?></a></p>
					</div>
					<div class="pl-2">
						<?php
						if (isset($links)) {
							$link = json_decode($links, true);
							foreach ($link as $key => $value) {
								if (is_array($value)) {
									$value = $value[0];
								}
								if ($key =='website') {
									echo '';
								}elseif ($key =='youtube') {
									echo '<a href="'.trim($value).'" target="_blank"><i class="fab fa-'.trim(strtolower($key)).'"></i></a>&nbsp;';
								} else{
									echo '<a href="'.trim(strtolower($value)).'" target="_blank"><i class="fab fa-'.trim(strtolower($key)).'"></i></a>&nbsp;';
								}
							}
						}
						?>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-12">
					<p><?php echo $product_desc; ?></p>
					<?php
					if (isset($img)) {
						echo '<p><img src="'.$image.'" alt="'.$product_name.'" class="img-fluid"></p>';
					}
					?>
				</div>
			</div>
			<hr>
		</div>
	</div>
	<div class="container pt-3 pb-3">
		<div class="row">
			<div class="col-lg-9" id="reviewText">
				<?php
				require('discover/review1.php');
				include('inc/horizontalBanner.php');
				?>
				<div id="reviews">
					<p class="text-right">Sort by: <a href="<?php echo $uri; ?>&sort=latest">Latest</a> / <a href="<?php echo $uri; ?>&sort=earliest">Earliest</a> / <a href="<?php echo $uri;?>&sort=upvote">Upvotes</a> / <a href="<?php echo $uri;?>&sort=downvote">Downvotes</a></p>
					<?php
					$iNumber = 1;
					if ($totalReviews <1) {
						echo '<div class="alert alert-success" role="alert"><p>So... there aren\'t any reviews here yet.</p><p>But here are 3 reasons why you should write one:</p><ol>
						<li>You can set the tone for this product/service and kickstart the conversation.</li><li>You can help newbies make better decisions by sharing your experiences.</li><li>You\'d earn Canary Chirps for writing this review!</li></ol><p>What are you waiting for? Write one now!</p></div><div><p class="text-center"><img src="/img/empty_state.svg" class="img-fluid" alt="No review"></p></div>';
					}
					foreach ($reviews as $key => $row) {
						$reviewDate = date('F d, Y H:i:s', strtotime($row['update_time']));
						// $reviewDate = $row['update_time'];
						$review = nl2br($row['review']);
						$textLimit = 400;
						if (strlen($review) > $textLimit) {
							$review = substr($review, 0, $textLimit).'<button class="btn btn-link" id="seeMore'.$iNumber.'" onClick="$(\'#more'.$iNumber.'\').show();$(\'#seeMore'.$iNumber.'\').hide()"> Click here to see more...</button><span id="more'.$iNumber.'" style="display:none;">'.substr($review, $textLimit).'</span>';
						}
						$review_id = $row['review_id'];
						$product_id = $entity_id = $row['product_id'];
						$upvote_tally = $row['upvote_tally'];
						$downvote_tally = $row['downvote_tally'];
						$username = $row['username'];
						$rating = (int)$row['rating'];
						$user_id = $row['user_id'];
						$avatar = $row['avatar'];
						$chirps = $row['score'];
						$randomAvatar = '/images/avatars/random/avatar'.rand(1,19).'.png';
						if(strpos($avatar, 'random') !== false) { $avatar = $randomAvatar;}
						elseif(strlen($avatar) <1) { $avatar = $randomAvatar;}
						else { $avatar = '/images/avatars/'.$user_id.'/'.$avatar;}
						echo '
						<div class="row align-items-center pb-3" id="#review-'.$review_id.'">
						<div class="col-12 whiteBG mt-3 mp-3 pt-2 rounded-lg">';
						echo '
						<div class="d-sm-flex align-items-center">
						<div class="p-2">
						<a href="/user/'.urlencode($username).'"><img src="'.$avatar.'" class="img-fluid" alt="avatar" style="height:50px;"></a> <a href="/user/'.urlencode($username).'">'.$username.'</a> <span data-toggle="tooltip" title="These are Chirps (reputation points). The more you gain, the more features and prizes you’ll unlock!" class="text-dark"><span class="fa-layers fa-fw" style="font-size: 1.8em;"><i class="fas fa-comment-alt" style="color: #ffa81e;"></i><span class="fa-layers-text" data-fa-transform="shrink-8 up-1" style="font-weight:900; font-size: 1.5rem;">'.$chirps.'</span></span></span>
						</div>
						<div class="p-2">
						<table>
						<tr><td>rated: </td><td><input id="average_team_quality" class="kv-rtl-theme-svg-star2 rating-loading" value="'.$rating.'" data-size="sm"></td></tr>
						</table>
						</div>
						</div>
						<hr>
						<p><small><strong><span class="text-muted"><a href="/user/'.urlencode($username).'" class="text-dark">'.$username.'</a></strong> posted</span> on '.$reviewDate.'</small></p>
						<p class="pt-2">'.$review.'</p><hr>';
						include('voting.php');
						echo '</div>
						</div>';
					}
					?>
				</div>
				<?php include('inc/twitterFeed.php'); ?>
			</div>
			<div class="col-lg-3">
				<?php include('inc/rightSideBar.php'); ?>
			</div>
		</div>
	</div>
	<?php include('discover/footer.php'); ?>
	<?php include('inc/endScripts.php'); ?>
	<script  type="text/javascript">
		var myTextEl = document.getElementById('reviewText');
		myTextEl.innerHTML = Autolinker.link( myTextEl.innerHTML, {
			stripPrefix: false,
			newWindow: true
		});
	</script>
	<script type="text/javascript">
		$('.kv-rtl-theme-svg-star').rating({
			hoverOnClear: false,
			theme: 'krajee-svg',
			step: 1,
			disabled: false,
			readonly: false,
			showClear: false,
			starCaptions: {1: 'Very Poor', 2: 'Poor', 3: 'Neutral', 4: 'Good', 5: 'Very Good'}
		});
		$('.kv-rtl-theme-svg-star2').rating({
			hoverOnClear: false,
			theme: 'krajee-svg',
			showClear: false,
			disabled: true,
			readonly: true,
			showCaption: false
		});
	</script>
	<script type="application/ld+json">
		{
			"@context": "http://schema.org/",
			"@type": "Product",
			"aggregateRating": {
			"@type": "AggregateRating",
			"ratingValue": <?php echo round($confidence); ?>,
			"reviewCount": <?php if($iNumber>0) {echo $iNumber -1;} else {echo 0;} ?>,
			"bestRating": 100,
			"worstRating": 0
		},
		"brand": "<?php echo $product_name; ?>",
		"name": "<?php echo $product_name; ?>",
		"alternateName": "<?php echo $symbol; ?>",
		"description": "<?php echo $description; ?>",
		"image": "https://cryptocanary.app/images/entities/<?php echo $logo; ?>",
		"url": "<?php echo $websiteURL; ?>",
		"review": {
		"reviewRating": {
		"@type": "Rating",
		"ratingValue": <?php echo round($confidence); ?>,
		"bestRating": 100,
		"worstRating": 0
	},
	"author": {
	"@type": "Organization",
	"name": "CryptoCanary",
	"logo": "https://cryptocanary.app/images/Cryptocanary-logo.png",
	"url": "https://cryptocanary.app/",
	"description": "CryptoCanary consolidates community reviews and research to help beginners avoid crypto scams. Submit, rate, and review crypto product and services to get started!."
}
}
}
</script>
<script type="text/javascript">$(function () {$('[data-toggle="tooltip"]').tooltip()})</script>
<script type="text/javascript">
	console.log(<?php echo '"confidence: '.$confidence.' overall_rating: '.$overall_rating.' overall_stars: '.$overall_stars.'"'; ?>);
</script>
<script type="text/javascript">
	var uri = window.location.toString();
	if (uri.indexOf("?") > 0) {
		var clean_uri = uri.substring(0, uri.indexOf("?"));
		window.history.replaceState({}, document.title, clean_uri);
	}
</script>
</body>
</html>
