<?php
session_start();
$user_id = 0;
if (isset($_SESSION['user_id'])) { $user_id = $_SESSION['user_id'];}
if ($user_id < 1) {
    header('Location: index.php?error=something went wrong. Contact our support team or try again.');
}
date_default_timezone_set('America/Lima');
$date = date('Y-m-d H:i:s');
$user_id = $_SESSION['user_id'];
$username = $_SESSION['username'];
$cryptoname = filter_input(INPUT_POST,'cryptoname',FILTER_SANITIZE_STRING);
$cryptosymbol = filter_input(INPUT_POST,'cryptosymbol',FILTER_SANITIZE_STRING);
$cryptoWebsite = filter_input(INPUT_POST,'cryptoWebsite',FILTER_SANITIZE_STRING);
$description = filter_input(INPUT_POST,'description',FILTER_SANITIZE_STRING);
$data = $date.', '.$user_id.', '.$username.', '.$cryptoname.', '.$cryptosymbol.', '.$cryptoWebsite.', '.$description.PHP_EOL;
$emailData = $date.'<br>'.$user_id.'<br>'.$username.'<br>'.$cryptoname.'<br>'.$cryptosymbol.'<br>'.$cryptoWebsite.'<br>'.$description.PHP_EOL;
$outputFile = '../logs/addProject.txt';
$log = $data.PHP_EOL;
file_put_contents($outputFile,$log,FILE_APPEND);

// send an email
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
// Load Composer's autoloader
require '../vendor/autoload.php';

// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);

try {
    //Server settings
    $mail->SMTPDebug = 0;                                       // Enable verbose debug output
    $mail->isSMTP();                                            // Set mailer to use SMTP
    $mail->Host       = 'email-smtp.us-east-1.amazonaws.com';   // Specify main and backup SMTP servers
    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
    $mail->Username   = 'AKIAJX4KAZZ54CMFZGXA';                 // SMTP username
    $mail->Password   = 'BFSCM0P5xx0KXrGktqMQZ4fSUOPWWMyNB3+Jf128cnHM';                               // SMTP password
    $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
    $mail->Port       = 587;                                    // TCP port to connect to

    //Recipients
    $mail->setFrom('team@cryptocanary.app', 'CryptoCanary');
    $mail->addAddress('team@cryptocanary.app', 'CryptoCanary');    // Add a recipient
    $mail->addReplyTo('team@cryptocanary.app', 'CryptoCanary');

    // Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = $cryptoname.' ('.$cryptosymbol.') was submitted via https://cryptocanary.app/add/';
    $mail->Body    = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html lang="en">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <style type="text/css">body{margin:0;color:#333;background-color:#10173b;font-size:16px}.container{max-width:600px;margin:auto;background-color:#fff;padding:15px}</style>
    </head>
    <body>
    <div class="container">
	<p>Someone request to add a cryptocurrency on the cryptocanary website.<br>Here are the details:</p>
	<p>'.$emailData.'</p>
    </div>
    </body>
    </html>';
    $mail->AltBody = 'Someone request to add a cryptocurrency on the cryptocanary website. Here are the details: '.$data;
    $mail->send();
    header('Location: index.php?success=success');
} catch (Exception $e) {
    header('Location: index.php?error=Message could not be sent.');
}

