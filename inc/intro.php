<div id="Map">
	<nav class="navbar navbar-expand-md navbar-dark bg-transparent">
		<div class="container">
			<a href="/" class="navbar-brand"><img src="/img/canary-white.png" class="img-fluid" alt="CryptoCanary logo" style="height:35px;"></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar5">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="navbar-collapse collapse">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a href="/login/" class="nav-link text-white" href="#">Log In</a>
					</li>
					<li class="nav-item">
						<a href="/signup/" class="nav-link btn btn-primary text-white">Sign Up</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>
	<div id="intro">
		<div class="container text-center">
			<div class="row justify-content-center">
				<div class="col-12 col-md-8 col-lg-6 text-white">
					<h1>SICK of all the <span class="yellow">scams</span> hurting the crypto world?</h1>
					<p class="pt-3 banner-sub"><strong>Help clean them up with your reviews!</strong></p>
				</div>
			</div>
			<div class="row justify-content-center">
				<div class="col-12 col-md-8 col-lg-6 pt-5">
					<form action="search.php" accept-charset="UTF-8" method="POST" role="FORM">
						<div class="input-group">
							<input list="cryptocurrency" name="search" id="search" placeholder="e.g. Bitcoin (BTC)" class="form-control searchText">
							<?php include('data/datalist.html'); ?>
							<span class="input-group-btn">
								<button type="submit" class="btn btn-primary searchBT"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
							</span>
						</div>
					</form>
				</div>
			</div>
			<div class="row justify-content-center pt-3">
				<div class="col-12 col-md-6">
					<p>Cannot find what you are looking for?<br><a href="add" class="yellow">Add a new cryptocurrency</a></p>
				</div>
			</div>
		</div>
	</div>
</div>
