<?php
session_start();
if (isset($_SERVER['HTTP_REFERER'])) {
	$http_referer = strtolower(trim($_SERVER['HTTP_REFERER']));
} else {
	$http_referer = '';
}
if (strpos($http_referer,'producthunt.com') !== false) {
	$ref = 'producthunt';
} else {
	$ref = strtolower(filter_input(INPUT_GET,'ref',FILTER_SANITIZE_STRING));
}
if (strlen($ref)> 3) {
	$_SESSION['referer'] = $ref;
}
require_once('db.php');
try {
	// $stmt = $conn->prepare('SELECT * FROM review INNER JOIN `user` ON `review`.user_id = `user`.user_id ORDER BY review.`review_id` DESC LIMIT 10');
	$stmt = $conn->prepare('SELECT review.review_id, review.user_id,review.entity_id, review.review,review.team_quality,review.info_quality,review.track_record,`user`.username,`user`.avatar,`entity`.entity_name,`entity`.symbol, `entity`.image FROM review INNER JOIN `user` ON `review`.user_id = `user`.user_id INNER JOIN `entity` ON `review`.entity_id = entity.`entity_id` ORDER BY review.`review_id` DESC LIMIT 4');
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$data = $stmt->fetchAll(); // store the result in a array that can be used after
	// $stmt->debugDumpParams();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
// Get some general Real Time Statistics
try {
	$stmt = $conn->prepare('SELECT COUNT(`username`) from `user` WHERE `username` IS NOT NULL');
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_NUM);
	$totalUser = $stmt->fetchAll();

}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
try {
	$stmt = $conn->prepare('SELECT COUNT(`entity_id`) from `entity` WHERE `approved_flag` = 1');
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_NUM);
	$totalEntity = $stmt->fetchAll();

}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
try {
	$stmt = $conn->prepare('SELECT COUNT(`review_id`) from `review`');
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_NUM);
	$totalReview= $stmt->fetchAll();

}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$conn = null;
$title = 'Crypto Insights Made Easy | CryptoCanary';
$description = 'CryptoCanary consolidates community reviews and research to help beginners avoid crypto scams. Submit, rate, and review crypto projects to get started!';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('inc/head.php');?>
	<style type="text/css">
		input[type='search'] {text-align: center;font-size: 1.25rem;line-height: 1.5;width: 100%;height: 48px;padding-left: 30px !important;border-radius: 20px !important;font-family: 'Inter', Helvetica, sans-serif !important;font-style: normal !important;}
		@media screen and (max-width: 576px) {
			#saying .pl-4 {
				padding-left: 0px !important;
				padding-bottom: 30px !important;
			}
		}
		.easy-autocomplete.eac-square input {
			min-width: 250px;
		}
	</style>
</head>
<body style="background-color: #fff;">
	<?php
	if (strpos(__DIR__,'/var/www/html/') !== false) {include('/var/www/html/inc/googleTag.php');}
	?>
	<div id="Map">
		<?php
		// Banner to inform user about the chirp system. Deactivated for now..
		// if (isset($_SESSION['user_id']) AND $_SESSION['user_id'] >0) {echo '<div id="chirp" class="text-center" style="background-color:#df8822; padding: 5px">Introducing Chirps - earn rewards for your contribution! <a href="/chirp/">Learn More...</a></div>';}
		include('inc/landingNav.php');
		?>
		<div id="intro">
			<div class="container text-center">
				<div class="row justify-content-center">
					<div class="col-12 col-md-10 pt-5">
						<h1 style="font-weight:800; font-size: 3rem;">Crypto Insights Made Easy</h1>
						<p class="pt-3" style="font-size: 2rem;">Community-powered ratings and reviews you can trust</p>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-12 col-md-8 col-lg-6 pt-5 pb-5">
						<form action="search.php" accept-charset="UTF-8" method="GET" role="FORM">
							<div class="form-group">
								<input type="search" id="coinList" name="search" placeholder="Find A Great Project" spellcheck="false">
							</div>
						</form>
						<div class="pt-3">
							<p><a href="/browse/" class="btn btn-lg btn-outline-dark mt-3">Browse Coins</a> <a href="/discover/" class="btn btn-lg btn-outline-dark mt-3">Discover Products</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php
	// 2nd banner for Product Hunt user
	if ($ref == 'producthunt') {
		echo '<div class="container pt-5 pb-5">
		<div class="row">
		<div class="col-12">
		<h2 class="pb-3">Special Message For Product Hunters</h2>
		<p>Don\'t forget our giveaway contest!</p><p>All you have to do is to sign up for an account within 48 hours of our launch and you get entered into a drawing for $100 in ETH.</p><p>Also first 50 U.S.-based sign ups can get a cool laptop sticker mailed to you!</p><p class="pt-3 pb-3"><a href="/signup/?ref=producthunt" class="btn btn-outline-primary">Sign Up Now</a></p>
		</div>
		</div>
		</div>';
	}
	?>
	<div id="lastReview">
		<div class="container">
			<h2>Recent Reviews</h2>
			<div class="row">
				<?php
				foreach ($data as $key => $row) {
					$image = '/images/entities/'.$row['entity_id'].'/'.$row['image'];
					$average_rating = number_format(($row['team_quality']+$row['info_quality']+$row['track_record'])/3,1);
					$percentage = number_format($average_rating*20,0);
					echo '<div class="col-sm-6 col-lg-3 pt-3 pb-3">
					<div class="card">
					<div class="card-body">
					<h5 class="card-title"><a href="/review/'.urlencode($row['entity_name']).'" class="text-dark"><img src="'.$image.'" class="img-fluid" alt="Image" style="height: 35px;"> '.$row['entity_name'].'</a></h5>
					<hr>
					<p class="card-text">'.$row['username'].' <small><span class="text-muted">reviewed</span></small><br>
					<input type="text" id="average_rating" name="average_rating" class="kv-rtl-theme-svg-star2 rating-loading" value="'.$average_rating.'" data-size="xs"><br>
					'.substr($row['review'], 0, 100).'...
					</p>
					<a href="/review/'.urlencode($row['entity_name']).'" class="btn btn-block btn-primary">Read Full Review</a>
					</div>
					</div>
					</div>';
				}
				?>
			</div>
		</div>
	</div>
	<div class="container pt-3 pb-3">
		<div id="insight">
			<div class="row pt-4 pb-4">
				<div class="col-12 text-center">
					<p class="text-muted">Fight Crypto Scams Together</p>
					<h2>Insights from a community of like-minded crypto enthusiasts</h2>
				</div>
			</div>
			<div class="row pt-4 pb-4 text-center">
				<div class="col-md-4 p-3">
					<div class="border border-secondary rounded p-1">
						<p class="text-center"><img src="/img/users-crown.svg" class="img-fluid landingIcon" alt="Image" style="background-color: #d8e3f2;"></p>
						<h3>Community-Driven Insights</h3>
						<p class="text-muted">In-depth reviews about hundreds of coins and projects</p>
					</div>
				</div>
				<div class="col-md-4 p-3">
					<div class="border border-secondary rounded p-1">
						<p class="text-center"><img src="/img/university.svg" class="img-fluid landingIcon" alt="Image" style="max-height:100px; fill: red; margin:20px; padding: 10px; border-radius: 10px; background-color: #d8f4e6"></p>
						<h3>Investor Quality Reviews</h3>
						<p class="text-muted">Leverage insights about coins and projects from the community</p>
					</div>
				</div>
				<div class="col-md-4 p-3">
					<div class="border border-secondary rounded p-1">
						<p class="text-center"><img src="/img/balance-scale-right.svg" class="img-fluid landingIcon" alt="Image" style="max-height:100px; fill: red; margin:20px; padding: 10px; border-radius: 10px; background-color: #e6d2eb"></p>
						<h3>Reviews You Can Trust</h3>
						<p class="text-muted">Our Chirps system rewards reviewers who share honest feedback</p>
					</div>
				</div>
			</div>
			<div class="row pt-4 pb-4">
				<div class="col-12 text-center">
					<p class="pt-3"><a href="/signup/" class="btn btn-lg btn-outline-primary">Join The Community</a></p>
				</div>
			</div>
		</div>
	</div>
	<div id="saying">
		<div class="container">
			<h2 class="pl-4">Here's what real users are saying</h2>
			<!-- For desktop User -->
			<div class="d-none d-md-block">
				<div id="slide1" class="carousel slide" data-ride="carousel">
					<div class="carousel-inner">
						<div class="carousel-item active">
							<img src="/img/testimony1.png" class="img-fluid"  alt="CryptoCanary Testimony 1">
						</div>
						<div class="carousel-item">
							<img src="/img/testimony2.png" class="img-fluid"  alt="CryptoCanary Testimony 2">
						</div>
						<div class="carousel-item">
							<img src="/img/testimony3.png" class="img-fluid"  alt="CryptoCanary Testimony 3">
						</div>
						<div class="carousel-item">
							<img src="/img/testimony4.png" class="img-fluid"  alt="CryptoCanary Testimony 4">
						</div>
						<div class="carousel-item">
							<img src="/img/testimony5.png" class="img-fluid"  alt="CryptoCanary Testimony 5">
						</div>
					</div>
					<a class="carousel-control-prev" href="#slide1" data-slide="prev">
						<span class="carousel-control-prev-icon"></span>
					</a>
					<a class="carousel-control-next" href="#slide1" data-slide="next">
						<span class="carousel-control-next-icon"></span>
					</a>
				</div>
			</div>
		</div>
		<!-- For mobile/iPad User -->
		<div class="d-md-none" id="testimonyBG">
			<div id="slide2" class="carousel slide" data-ride="carousel">
				<div class="container">
					<div class="carousel-inner">
						<div class="carousel-item active">
							<img src="/img/testimony1m.png" class="img-fluid img-thumbnail"  alt="CryptoCanary Testimony 1">
						</div>
						<div class="carousel-item">
							<img src="/img/testimony2m.png" class="img-fluid img-thumbnail"  alt="CryptoCanary Testimony 2">
						</div>
						<div class="carousel-item">
							<img src="/img/testimony3m.png" class="img-fluid img-thumbnail"  alt="CryptoCanary Testimony 3">
						</div>
						<div class="carousel-item">
							<img src="/img/testimony4m.png" class="img-fluid img-thumbnail"  alt="CryptoCanary Testimony 4">
						</div>
						<div class="carousel-item">
							<img src="/img/testimony5m.png" class="img-fluid img-thumbnail"  alt="CryptoCanary Testimony 5">
						</div>
					</div>
				</div>
				<a class="carousel-control-prev" href="#slide2" data-slide="prev">
					<span class="carousel-control-prev-icon"></span>
				</a>
				<a class="carousel-control-next" href="#slide2" data-slide="next">
					<span class="carousel-control-next-icon"></span>
				</a>
			</div>
		</div>
	</div>
</div>
<div class="container">
	<div id="fingertips">
		<p class="text-center text-muted">Everything Crypto At Your Fingertips</p>
		<h2 class="text-center">Why join CryptoCanary?</h2>
		<div class="row pt-4 pb-4">
			<div class="col-md-6">
				<p><img src="/img/process.svg" class="img-fluid" alt="Information At Your Fingertips"></p>
			</div>
			<div class="col-md-6">
				<p class="text-muted">Information At Your Fingertips</p>
				<h3>Stop digging through hundreds of Reddit, Twitter and Telegram posts just to get the information you need.</h3>
				<p class="pt-3"><a href="/signup/" class="btn btn-lg btn-outline-primary">Join The Community</a></p>
			</div>
		</div>
		<div class="row pt-5 pb-4">
			<div class="col-md-6">
				<p class="text-muted">Help Educate The Community</p>
				<h3>Help the community stay safe by sharing your insights and perspective with the crypto community.</h3>
				<p class="pt-3"><a href="/signup/" class="btn btn-lg btn-outline-primary">Join The Community</a></p>
			</div>
			<div class="col-md-6">
				<p><img src="/img/collaboration.svg" class="img-fluid" alt="Help Inform The Community"></p>
			</div>
		</div>
	</div>
</div>
<?php include('inc/telegram.php'); ?>
<?php include('inc/footer.php'); ?>
<?php include('inc/endScripts.php'); ?>
<script type="text/javascript">
	$('.kv-rtl-theme-svg-star2').rating({
		hoverOnClear: false,
		theme: 'krajee-svg',
		showClear: false,
		disabled: true,
		readonly: true,
		showCaption: false
	});
</script>
<?php
if ($ref == 'producthunt') {
	include('landing/modal.php');
	echo '<script type="text/javascript">$(window).on("load",function(){$("#myModal").modal("show");});</script>';
}
?>
</body>
</html>
