<div id="telegram">
	<div class="container">
		<div class="row text-center">
			<div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1">
				<h4>Don't miss out</h4>
				<p>Get cool product recs and special discounts delivered straight to your inbox!</p>
			</div>
		</div>
		<div class="row text-center">
			<div class="col-lg-6 offset-lg-3 col-md-10 offset-md-1">
				<form action="/newsletter/" method="POST" role="form">
					<div class="row">
						<div class="col-md-8">
							<div class="form-group">
								<input type="email" class="form-control" id="email" name="email" placeholder="Your email address" required="required" minlength="5">
							</div>
						</div>
						<div class="col-md-4">
							<button type="submit" class="btn btn-block btn-primary">I'm in!</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<footer>
	<div class="container">
		<div class="row pt-3">
			<div class="col-lg-3 col-sm-6 pt-3">
				<p><img src="/img/logo2.svg" class="img-fluid" alt="CryptoCanary Logo" style="max-height: 50px;"></p>
				<p>&copy; <?php echo date('Y'); ?> CanaryReviews LLC</p>
				<p><i class="far fa-envelope"></i> <a href="mailto:team@cryptocanary.app">team@cryptocanary.app</a></p>
				<p><i class="fas fa-phone-alt"></i> <a href="tel:+16282033813">(628) 203-3813</a></p>
			</div>
			<div class="col-lg-3 col-sm-6 pt-3">
				<h3>Get Involved</h3>
				<p><a href="/advertise/">Advertising</a></p>
				<p><a href="/signup/">Join the community</a></p>
				<p><a href="/leaderboard/">Leaderboard</a></p>
				<p><a href="/add/">Submit a Project</a></p>
				<p><a href="/sitemap.xml">Sitemap</a></p>
			</div>
			<div class="col-lg-3 col-sm-6 pt-3">
				<h3>Legal</h3>
				<p><a href="/terms/">Terms and conditions</a></p>
				<p><a href="/privacy/">Privacy Policies</a></p>
				<p><a href="/disclaimer/">Disclaimers</a></p>
				<p><a href="/community-guidelines/">Community Guidelines</a></p>
			</div>
			<div class="col-lg-3 col-sm-6 pt-3">
				<h3>Connect With Us</h3>
				<p><a href="https://t.me/cryptocanaryapp/" class="text-black"><i class="fab fa-telegram-plane fa-lg" aria-hidden="true"></i> Telegram</a></p>
				<p><a href="https://twitter.com/cryptocanaryapp" class="text-black"><i class="fab fa-twitter fa-lg" aria-hidden="true"></i> Twitter</a></p>
				<p><a href="https://linkedin.com/company/cryptocanary/" class="text-black"><i class="fab fa-linkedin-in fa-lg" aria-hidden="true"></i> Linkedin</a></p>
				<p><a href="https://blog.cryptocanary.app" class="text-black"><i class="fab fa-wordpress fa-lg" aria-hidden="true"></i> Blog</a></p>
				<p><a href="/feed/" class="text-black"><i class="fas fa-rss fa-lg" aria-hidden="true"></i> RSS feed</a></p>
			</div>
		</div>
	</div>
</footer>
<?php if (strpos(__DIR__,'/var/www/html') === 0) {
	echo '<div class="fixed-bottom bg-info cookie-message" id="cookie">
	<p class="mb-1 pl-1">By using this website you allow us to place cookies on your computer.<a class="btn btn-sm btn-warning cbc" href="#" onClick="$(\'#cookie\').hide();">Accept</a></p>
</div>';
}

