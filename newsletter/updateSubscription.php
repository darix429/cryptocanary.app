<?php
// update mailchimp subscription > member info


$email = strtolower(filter_input(INPUT_POST,'email',FILTER_SANITIZE_EMAIL, FILTER_FLAG_EMAIL_UNICODE));
$fname = ucfirst(filter_input(INPUT_POST,'FNAME',FILTER_SANITIZE_STRING));
$lname = ucfirst(filter_input(INPUT_POST,'LNAME',FILTER_SANITIZE_STRING));
$subscriberHash = md5($email);

$authToken = "f2a8c6fa8730a275f4ee5ae6741df8c0-us16";
$list_id = "d4322e8c1b";
$dc = explode("-",$authToken)[1];
$postData = array(
	"email_address" => "$email",
    "status" => "subscribed",
    "merge_fields" => [
        "FNAME" => $fname,
        "LNAME" => $lname
    ]
);

$ch = curl_init('https://'.$dc.'.api.mailchimp.com/3.0/lists/'.$list_id.'/members/'.$subscriberHash);
curl_setopt_array($ch, array(
	CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_CUSTOMREQUEST => 'PATCH',
	CURLOPT_POSTFIELDS => json_encode($postData),
	CURLOPT_HTTPHEADER => array(
		'Authorization: apikey '.$authToken,
		'Content-Type: application/json'
	)
));

$response = curl_exec($ch);
$responseJson = json_decode($response);
$responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
curl_close($ch);

// var_dump($response);
// var_dump($responseJson);

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php'); ?>
</head>
<body>
	<?php include('../inc/nav.php'); ?>
	<main class="container pt-3 pb-3">
		<h4>Thank you</h4>
        <?php
            if ($responseCode == 404) {
                echo ' <div class="alert alert-danger" role="alert"><p>Your email is not yet subscribed to our newsletter.</p></div>';
            } else if ($responseCode == 200) {
                echo ' <div class="alert alert-success" role="alert"><p>Your information is successfully updated.</p></div>';
            } else {
                echo $rseponseCode;
            }
        ?>
        <p><a href="/"><button type="button" class="btn btn-primary">Back to home</button></a></p>
    </main>
	<?php include('../inc/telegram.php'); ?>
	<?php include('../inc/footer.php'); ?>
	<?php include('../inc/endScripts.php'); ?>
</body>
</html>
