<?php
require('db.php');
try {
	$stmt = $conn->prepare('SELECT * FROM entity WHERE approved_flag = 1');
	$stmt->execute();
	$count = $stmt->rowCount(); // get the number of row
	$stmt->setFetchMode(PDO::FETCH_ASSOC); // set the resulting to returns associative array
	$data = $stmt->fetchAll(); // store the result in a array that can be used after
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$conn = null;
$title = 'Crypto Reviews | Shady or Legit? | CryptoCanary';
$description = 'CryptoCanary consolidates community reviews and research to help beginners avoid crypto scams. Submit, rate, and review crypto projects to get started!';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('inc/head.php'); ?>
</head>
<body>
	<div id="Map">
		<?php include('inc/landingNav.php'); ?>
		<div id="intro">
			<div class="container text-center">
				<div class="row justify-content-center">
					<div class="col-12 col-md-8 col-lg-6 text-white">
						<h1 style="font-weight:800">SICK of all the <span class="yellow">scams</span> hurting the crypto world?</h1>
						<p class="pt-3"><strong>Help clean them up with your reviews!</strong></p>
					</div>
				</div>
				<div class="row justify-content-center">
					<div class="col-12 col-md-8 col-lg-6 pt-5">
						<form action="../search/" accept-charset="UTF-8" method="POST" role="FORM">
							<div class="input-group">
								<input list="cryptocurrency" name="search" id="search" placeholder="e.g. Bitcoin (BTC)" class="form-control searchText">
								<?php include('data/datalist.html'); ?>
								<span class="input-group-btn">
									<button type="submit" class="btn btn-primary searchBT"><i class="fa fa-search" aria-hidden="true"></i> Search</button>
								</span>
							</div>
						</form>
					</div>
				</div>
				<div class="row justify-content-center pt-3">
					<div class="col-12 col-md-6">
						<p>Cannot find what you are looking for?<br><a href="add" class="yellow">Submit new project</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="container pt-5 pb-5">
		<h2 class="pb-3">Browse cryptocurrency reviews</h2>
		<table id="table" class="table table-striped">
			<thead>
				<tr><th>#</th><th>Name</th><th>Symbol</th><th>Country</th><th>industry</th><th>Confidence</th></tr>
			</thead>
			<tbody>
				<?php
				foreach ($data as $key => $row) {
					$rating = $row['overall_rating'];
					$confidence = number_format(100-(($rating)*25), 0);
					if ($confidence <= 25) {$pbcolor = 'alert-danger';}
					if ($confidence > 25 AND $confidence <=50) {$pbcolor = 'alert-warning';}
					if ($confidence > 50 AND $confidence <=75) {$pbcolor = 'alert-info';}
					if ($confidence > 75) {$pbcolor = 'alert-success';}
					echo '<tr><td><a href="/review/'.urlencode(trim($row['entity_name'])).'"><img style="width: 50px;" class="img-fluid" src="/images/entities/'.$row['entity_id'].'/'.$row['image'].'" alt="'.$row['entity_name'].'"></a></td><td><a href="/review/'.urlencode(trim($row['entity_name'])).'">'.$row['entity_name'].'</a></td><td>'.$row['symbol'].'</td><td>'.$row['country'].'</td><td>'.$row['industry'].'</td><td class="'.$pbcolor.'">'.$confidence.' %</td></tr>';
				}
				?>
			</tbody>
			<tfoot>
				<tr><th>#</th><th>Name</th><th>Symbol</th><th>Country</th><th>industry</th><th>Confidence</th></tr>
			</tfoot>
		</table>
	</div>
</main>
<?php include('inc/telegram.php'); ?>
<?php include('inc/footer.php'); ?>
<?php include('inc/endScripts.php'); ?>
<script type="text/javascript">
if (location.origin + location.pathname === 'https://cryptocanary.app') {
	location.search += '?v1';
}
</script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#table').DataTable( {
			"pageLength": 25,
			"order": [[ 1, "asc" ]]
		});
	});
</script>
</body>
</html>
