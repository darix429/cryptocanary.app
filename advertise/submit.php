<?php
session_start();
date_default_timezone_set('America/Lima');
// $tokenVerif =  md5('cryptocanary'.date('Ymd'));
$tokenVerif =  $_SESSION['tokenSubmit'];
$token = filter_input(INPUT_POST,'token',FILTER_SANITIZE_STRING);
$email = trim(filter_input(INPUT_POST,'email',FILTER_SANITIZE_STRING));
if ($token != $tokenVerif) {
	header('Location: /advertise/?error=Wrong Token');
	exit();
}
if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	header('Location: /advertise/?error=Email not Valid');
	exit();
}
$data = json_encode($_POST);
// log this to a log file
$outputFile = 'logs.txt';
$log = $data.PHP_EOL;
file_put_contents($outputFile,$log,FILE_APPEND);
// send an email
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
// Load Composer's autoloader
require '../vendor/autoload.php';

// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);

//Server settings
$mail->SMTPDebug = 0;                                       // Enable verbose debug output
$mail->isSMTP();                                            // Set mailer to use SMTP
$mail->Host       = 'email-smtp.us-east-1.amazonaws.com';   // Specify main and backup SMTP servers
$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
$mail->Username   = 'AKIAJX4KAZZ54CMFZGXA';                 // SMTP username
$mail->Password   = 'BFSCM0P5xx0KXrGktqMQZ4fSUOPWWMyNB3+Jf128cnHM'; // SMTP password
$mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
$mail->Port       = 587;                                    // TCP port to connect to

//Recipients
$mail->setFrom('team@cryptocanary.app', 'CryptoCanary');
$mail->addAddress('team@cryptocanary.app', 'CryptoCanary');     // Add a recipient
$mail->addReplyTo('team@cryptocanary.app', 'CryptoCanary');

// Content
$mail->isHTML(true);                                  // Set email format to HTML
$mail->Subject = 'Someone fill out the advertising form under: https://cryptocanary.app/advertise/';
$mail->Body    = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<title>Your account on CryptoCanary has been created</title>
<style type="text/css">body{margin:0;color:#333;background-color:#10173b;font-size:16px}.container{max-width:600px;margin:auto;background-color:#fff;padding:15px}</style>
</head>
<body>
<div class="container">
<p>Someone fill out the advertising form under: https://cryptocanary.app/advertise/</p>
<p>Here are the Details</p>
<p>'.$log.'</p>
</div>
</body>
</html>';
$mail->AltBody = 'Someone fill out the advertising form under: https://cryptocanary.app/advertise/. Here are the details: '.$log;
$mail->send();

header('Location: index.php?success=success');
