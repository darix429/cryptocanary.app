<?php
if (strpos(__DIR__,'/var/www/html') !== false) {
	require_once '/var/www/html/vendor/autoload.php';
} else {
	require_once '../vendor/autoload.php';
}
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;


class Util {

	private static $_instance;

	public static function getInstance() {
		if(!self::$_instance) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	function __construct() {
	}

	function setConnection($db) {
		if (!isset($this->db))
			$this->db = $db;
	}

	function getUserDetailsByEmail($email) {
		try {
			$stmt = $this->db->prepare('SELECT user_id, username, email, avatar, score FROM user WHERE email = ?');
			$stmt->execute(array($email));
			$stmt->setFetchMode(PDO::FETCH_ASSOC);
			$user = $stmt->fetch();

			if (!empty($user)) {
				return $user;
			} else {
				return null;
			}
		}
		catch(PDOException $e) {
			echo 'Error: ' . $e->getMessage();
		}
		$this->db = null;
	}

	function mailerInit() {
		$this->mail = new PHPMailer();
		$this->mail->SMTPDebug = 0;
		$this->mail->isSMTP();
		$this->mail->Host       = 'email-smtp.us-east-1.amazonaws.com';
		$this->mail->SMTPAuth   = true;
		$this->mail->Username   = 'AKIAJX4KAZZ54CMFZGXA';
		$this->mail->Password   = 'BFSCM0P5xx0KXrGktqMQZ4fSUOPWWMyNB3+Jf128cnHM';
		$this->mail->SMTPSecure = 'tls';
		$this->mail->Port       = 587;
		$this->mail->setFrom('team@cryptocanary.app', 'CryptoCanary');
		$this->mail->addReplyTo('team@cryptocanary.app', 'CryptoCanary');
		$this->mail->isHTML(true);
	}

	function sendEmail($data) {
		if (strpos(__DIR__, "/var/www/html") === 0)  {
			$to = $data["email"];
			$subject = $data["subject"];
			$template = $data["template"];
			$body = file_get_contents("/var/www/html/email/$template");

			$variables = !empty($data["data"])? $data["data"] : [];
			foreach($variables as $key=>$value) {
				$body = str_replace("{{".$key."}}", $value, $body);
			}

			$this->mailerInit();
			$this->mail->addAddress($to);
			$this->mail->Subject = $subject;
			$this->mail->MsgHTML($body);

			$res = false;
			$log = date('Y-m-d H:i:s') . "-";
			if(!$this->mail->send()) {
				$log .= "Failed: " . json_encode($data);
			} else {
				$log .= "Sent: " . json_encode($data);
				$res = true;
			}
			file_put_contents("/var/www/html/logs/email.log", $log . PHP_EOL, FILE_APPEND);

			return $res;
		}
	}

	function queueEmail($data) {
		$json = json_encode($data);
		$json .= "," . PHP_EOL;
		file_put_contents("/var/www/html/email/email.queue.json", $json, FILE_APPEND);
	}
}
