<?php
session_start();
$id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT);
require('../db.php');
// Get all the review from the Entity
try {
	$stmt1 = $conn->prepare("SELECT * FROM review INNER JOIN entity ON review.entity_id = entity.entity_id INNER JOIN `user` ON review.user_id = `user`.user_id WHERE review_id = :id");
	$stmt1->bindParam(':id', $id, PDO::PARAM_INT);
	$stmt1->execute();
	$stmt1->setFetchMode(PDO::FETCH_ASSOC);
	$countData = $stmt1->rowCount();
	$data = $stmt1->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$conn = null;
if ($countData < 1) {
	header('Location: /');
}
// var_dump($data);
$review = trim($data[0]['review']);
$entity_name = $data[0]['entity_name'];
$entity_id = $data[0]['entity_id'];
$symbol = $data[0]['symbol'];
$username = $data[0]['username'];
$avatar = $data[0]['avatar'];
$user_id = $data[0]['user_id'];
$links = $data[0]['links'];
$logo = '/'.$entity_id.'/'.$data[0]['image'];
$postingDate = date('F j, Y, g:i a', strtotime($data[0]['update_time']));
$title = 'On '.$postingDate.', '.$username.' wrote...';
$description = substr ($review, 0, 160);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<script type="text/javascript">
		window.dataLayer = window.dataLayer || [];
		window.dataLayer.push({
			'event': 'formSubmission',
			'formType': 'Search',
			'searchText': '<?php echo $entity_name;?>',
			'formPosition': 'NavBar'
		});
	</script>
	<?php include('../inc/head.php'); ?>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/social-share-kit/1.0.15/css/social-share-kit.css">
</head>
<body>
	<?php include('../inc/nav.php'); ?>
	<div class="greyBG">
		<div class="container pt-5">
			<div class="row ">
				<div class="d-sm-flex flex-fill justify-content-center pb-3">
					<div class="align-self-center text-center"><a href="/review/<?php echo urlencode($entity_name);?>"><img src="/images/entities/<?php echo $logo; ?>" alt="<?php echo $entity_name; ?>" class="img-fluid pl-2 pr-2" style="height:50px;"></a></div>
					<div class="align-self-center text-center"><h1 class="pl-2 pr-2"><a href="/review/<?php echo urlencode($entity_name);?>" class="text-dark"><?php echo $entity_name;?></a></h1></div>
					<div class="align-self-center text-center"><p><span class="badge badge-secondary pl-4 pr-4" style="background-color:#b7b4b3;font-size:25px;line-height:30px; border-radius: 25px;"><?php echo $symbol; ?></span></p></div>
				</div>
			</div>
		</div>
	</div>
	<div class="container pt-3 pb-3">
		<div class="row">
			<div class="col-lg-9" id="reviewText">
				<div class="row d-lg-none pt-3 pb-3">
					<p class="pt-5"><a href="/advertise/"><img src="/img/banner_mobile.svg" class="img-fluid" alt="CryptoCanary banner"></a></p>
				</div>
				<hr>
				<div id="reviews">
					<p>On <?php echo $postingDate.', <a href="/user/'.urlencode($username).'">'.$username.'</a>'; ?> wrote:</p>
					<p><em>"<?php echo $review; ?>"</em></p>
					<p><a href="/review/<?php echo urlencode($entity_name); ?>">Learn More...</a></p>
					<hr>
					<span>Share this review on:</span>
					<div class="ssk-group ssk-count">
						<a href="https://cryptocanary.app/review/singleReview.php?id=<?php echo $id; ?>" class="ssk ssk-facebook"></a>
						<a href="https://cryptocanary.app/review/singleReview.php?id=<?php echo $id; ?>" class="ssk ssk-twitter"></a>
						<a href="https://cryptocanary.app/review/singleReview.php?id=<?php echo $id; ?>" class="ssk ssk-pinterest"></a>
						<a href="https://cryptocanary.app/review/singleReview.php?id=<?php echo $id; ?>" class="ssk ssk-linkedin"></a>
						<a href="https://cryptocanary.app/review/singleReview.php?id=<?php echo $id; ?>" class="ssk ssk-reddit"></a>
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<?php include('../inc/rightSideBar.php'); ?>
			</div>
		</div>
	</div>
	<?php include('../inc/footer.php'); ?>
	<?php include('../inc/endScripts.php'); ?>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/social-share-kit/1.0.15/js/social-share-kit.min.js"></script>
	<script type="text/javascript">SocialShareKit.init();</script>
	<script  type="text/javascript">
		var myTextEl = document.getElementById('reviewText');
		myTextEl.innerHTML = Autolinker.link( myTextEl.innerHTML, {
			stripPrefix: false,
			newWindow: true
		});
	</script>
	<script type="text/javascript">
		$('.kv-rtl-theme-svg-star').rating({
			hoverOnClear: false,
			theme: 'krajee-svg',
			step: 1,
			disabled: false,
			readonly: false,
			showClear: false,
			starCaptions: {1: 'Very Poor', 2: 'Poor', 3: 'Neutral', 4: 'Good', 5: 'Very Good'}
		});
		$('.kv-rtl-theme-svg-star2').rating({
			hoverOnClear: false,
			theme: 'krajee-svg',
			showClear: false,
			disabled: true,
			readonly: true,
			showCaption: false
		});
	</script>
	<script type="application/ld+json">
		{
			"@context": "http://schema.org/",
			"@type": "Product",
			"aggregateRating": {
			"@type": "AggregateRating",
			"ratingValue": <?php echo round($confidence); ?>,
			"reviewCount": <?php if($iNumber>0) {echo $iNumber -1;} else {echo 0;} ?>,
			"bestRating": 100,
			"worstRating": 0
		},
		"brand": "<?php echo $entity_name; ?>",
		"name": "<?php echo $entity_name; ?>",
		"alternateName": "<?php echo $symbol; ?>",
		"description": "<?php echo $description; ?>",
		"image": "https://cryptocanary.app/images/entities/<?php echo $logo; ?>",
		"url": "<?php echo $websiteURL; ?>",
		"review": {
		"reviewRating": {
		"@type": "Rating",
		"ratingValue": <?php echo round($confidence); ?>,
		"bestRating": 100,
		"worstRating": 0
	},
	"author": {
	"@type": "Organization",
	"name": "CryptoCanary",
	"logo": "https://cryptocanary.app/images/Cryptocanary-logo.png",
	"url": "https://cryptocanary.app/",
	"description": "CryptoCanary consolidates community reviews and research to help beginners avoid crypto scams. Submit, rate, and review crypto projects to get started!."
}
}
}
</script>
<script type="text/javascript">$(function () {$('[data-toggle="tooltip"]').tooltip()})</script>
</body>
</html>
