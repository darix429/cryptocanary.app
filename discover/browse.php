<?php
require('init.php');
$category = (int)filter_input(INPUT_GET,'category',FILTER_SANITIZE_NUMBER_INT);
$ecosystem = filter_input(INPUT_GET,'ecosystem',FILTER_SANITIZE_STRING);
$filter = '';
if (!empty($category)) {
	$filter = 'WHERE entity_category.entity_category_id = '.$category;
}
if (!empty($ecosystem)) {
	$filter .= (!empty($category)? " AND " : " WHERE ") . " ecosystem = '$ecosystem'";
}

require_once('../db.php');
try {
	$stmt = $conn->prepare("SELECT product_id, product_name, entity_category.category,ecosystem, logo,product.`create_time`,overall_rating,`short_desc`,review_count FROM `product` INNER JOIN entity_category ON entity_category.entity_category_id = product.category $filter AND product.approved_flag = 1 ORDER BY `product_id` DESC LIMIT 1000");
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$data = $stmt->fetchAll(); // store the result in a array that can be used after
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$conn = null;
// SEO Title and description
$title = 'Browse The Top Bitcoin/Crypto Products And Services';
$description = 'Browse a list of the top crypto and Bitcoin related products and services that you can actually use. All products are rated and reviewed by the community.';
$img = "https://cryptocanary.app/images/Cryptocanary-logo-v2.png";

?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php');?>
</head>
<body style="background-color: #f6f6f5;">
	<?php
	if (strpos(__DIR__,'/var/www/html/') !== false) {include('/var/www/html/inc/googleTag.php');}
	include('nav.php');
	?>
	<div id="latest">
		<div class="container pt-5 pb-5">
			<div class="latest">
				<h2 class="text-center pb-4">Browse All Products</h2>
				<div class="row">
					<div class="col-lg-9 whiteBG p-4">
						<form action="#" method="GET" role="form" class="form-inline pb-3">
							<label class="mb-1 mr-2" for="category">Filter:</label>
							<select name="category" id="category" class="form-control mb-2 mr-sm-2">
								<option value="0">All Categories</option>
								<option value="10" <?php if ($category == '10') { echo "selected";} ?>>DApps</option>
								<option value="11" <?php if ($category == '11') { echo "selected";} ?>>Blockchain Games</option>
								<option value="12" <?php if ($category == '12') { echo "selected";} ?>>DeFi</option>
								<option value="7" <?php if ($category == '7') { echo "selected";} ?>>Exchanges</option>
								<option value="13" <?php if ($category == '13') { echo "selected";} ?>>Media</option>
								<option value="14" <?php if ($category == '14') { echo "selected";} ?>>Mining/Staking</option>
								<option value="15" <?php if ($category == '15') { echo "selected";} ?>>Payments</option>
								<option value="16" <?php if ($category == '16') { echo "selected";} ?>>Tools/Utilities</option>
								<option value="8" <?php if ($category == '8') { echo "selected";} ?>>Wallets</option>
								<option value="18" <?php if ($category == '18') { echo "selected";} ?>>Education</option>
								<option value="17" <?php if ($category == '27') { echo "selected";} ?>>Others</option>
							</select>
							<select name="ecosystem" id="ecosystem" class="form-control mb-2 mr-sm-2">
								<option value="">All Ecosystems</option>
								<?php
									$ecosystems = ["General","Ethereum","Bitcoin","Bitcoin SV","Steem","EOS","Tron","Vechain","Blockstack","Other"];
									foreach($ecosystems as $i) {
										echo "<option value='$i' ". ($i == $ecosystem? "selected": "") .">$i</option>";
									}
								?>
							</select>
							<p class="mb-1 ml-2"><a href="https://cryptoprospectingtool.kickoffpages.com/?ref=discover" class="btn btn-danger" target="_blank">Advanced filter</a></p>
						</form>
						<table class="table table-striped" id="table">
							<thead>
								<tr>
									<th class="hidden-md-down" style="width: 50px;">#</th><th>Name</th><th class="hidden-md-down">Category</th><th>Rating</th><th>Reviews</th>
								</tr>
							</thead>
							<tbody>
								<?php
								foreach ($data as $key => $row) {
									$product_id = $row['product_id'];
									$logo = '/images/products/'.$row['product_id'].'/'.$row['logo'];
									$product_name = $row['product_name'];
									$short_desc = $row['short_desc'];
									$overall_rating= number_format($row['overall_rating'],0);
									if ($overall_rating == 0) {
										$overall_rating = 'N/A';
										$bgcolor = '';
									} elseif ($overall_rating <25) {
										$bgcolor = 'alert alert-danger ';
										$overall_rating = $overall_rating.'%';
									} elseif($overall_rating <50) {
										$bgcolor = 'alert alert-warning ';
										$overall_rating = $overall_rating.'%';
									} elseif($overall_rating <75) {
										$bgcolor = 'alert alert-success ';
										$overall_rating = $overall_rating.'%';
									} else {
										$bgcolor = 'alert alert-primary ';
										$overall_rating = $overall_rating.'%';
									}
									echo '<tr>
									<td class="hidden-md-down"><a href="/discover/'.urlencode($product_name).'"><img src="'.$logo.'" alt="'.$product_name.'" class="img-fluid" style="width: 50px; height: auto;"></a></td>
									<td><a href="/discover/'.urlencode($product_name).'">'.$product_name.'</a><br><span class="d-none d-sm-block"><small>'.$short_desc.'</small></span></td>
									<td class="hidden-md-down">'.$row['category'].'</td>
									<td class="'.$bgcolor.'text-right">'.$overall_rating.'</td>
									<td>'.$row['review_count'].'</td>
									</tr>';
								}
								?>
							</tbody>
						</table>
					</div>
					<div class="col-lg-3 pt-4">
						<p>Are we missing something awesome?</p>
						<p><a href="/add/" class="btn btn-block btn-primary">Submit New Entry</a></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?php include('footer.php'); ?>
	<?php include('../inc/endScripts.php'); ?>
	<script>
		$(function() {
			$('#category').change(function() {
				this.form.submit();
			});
			$('#ecosystem').change(function() {
				this.form.submit();
			});
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#table').DataTable( {
				"pageLength": 50,
				"order": [[ 0, "desc" ]]
			});
		});
	</script>
</body>
</html>
