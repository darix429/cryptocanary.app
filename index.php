<?php
date_default_timezone_set('America/Lima');
if (!empty($_GET)) {
	$dir = $name = '';
	$route = filter_input(INPUT_GET,'q',FILTER_SANITIZE_STRING);
	$dissect = explode('/', $route);
	// log the request in a log file
	if (strpos(__DIR__,'/var/www/html') !== false) {
		$log = date('Y-m-d H:i:s').','.$_SERVER['REMOTE_ADDR'].','.$route.PHP_EOL;
		file_put_contents('/var/www/html/logs/route.log',$log,FILE_APPEND);
	}
	if (isset($dissect['1'])) {
		$dir = $dissect['1'];
	}
	if (isset($dissect['2'])) {
		$name = $dissect['2'];
	}
	if ($dir == 'review') {
		$name = substr($route, 8);
		require("views/review.php");
		exit();
	}
	if ($dir == 'user') {
		require("views/user.php");
		exit();
	}
	if ($dir == 'symbol') {
		$symbol = $dissect['2'];
		require("views/symbol.php");
		exit();
	}
	if ($dir == 'discover') {
		$product_name = $dissect['2'];
		require('views/discover.php');
		exit();
	}
	if ($dir == 'random') {
		if (strpos(__DIR__,'/var/www/html') !== false) {
			$log = date('Y-m-d H:i:s').', '.$_SERVER['REMOTE_ADDR'].', '.$route.PHP_EOL;
			file_put_contents('/var/www/html/logs/random.log',$log,FILE_APPEND);
		}
		require("views/random.php");
		exit();
	}
	else {
		require('landing/index.php');
		exit();
	}
} else {
	require('landing/index.php');
}
