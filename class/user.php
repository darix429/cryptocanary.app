<?php

class User {

    function __construct($db) {
        $this->db = $db;
    }

    function getUserDetailsByEmail($email) {
        try {         
                $stmt = $this->db->prepare('SELECT user_id, username, email, avatar, score FROM user WHERE email = ?');
                $stmt->execute(array($email));
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                $user = $stmt->fetch();
    
                if (!empty($user)) {
                    return $user;
                } else {
                    return null;
                }
        }
        catch(PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
        $this->db = null;
    }
}
