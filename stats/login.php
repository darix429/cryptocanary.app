<?php
session_start();
session_unset();
session_destroy();
$title = 'Login | CryptoCanary';
$description = 'Login';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('head.php'); ?>
</head>
<body>
	<main class="container pt-5">
		<div class="row">
			<div class="col-lg-8">
				<form action="loginAction.php" method="POST" role="form">
			<legend>Please Login</legend>
			<div class="form-group">
				<label class="form-control-label" for="username">Username</label>
				<input type="text" name="username" id="username" class="form-control">
			</div>
			<div class="form-group">
				<label class="form-control-label" for="password">Password</label>
				<input type="password" name="password" id="password" class="form-control">
			</div>
			<button type="submit" class="btn btn-primary">Login</button>
		</form>
			</div>
		</div>

	</main>
	<?php include('endScripts.php'); ?>
</body>
</html>
