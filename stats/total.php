<?php
date_default_timezone_set('America/Lima');
session_start();
if ($_SESSION['ccstatistics'] != 'LoggedIn') {
	header('Location: login.php');
}
require_once('../db.php');
try {
	$stmt = $conn->prepare("SELECT COUNT(*) FROM `user`");
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_NUM);
	$users = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
try {
	$stmt = $conn->prepare("SELECT COUNT(*) FROM `entity`");
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_NUM);
	$entities = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
try {
	$stmt = $conn->prepare("SELECT COUNT(*) FROM `review`");
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_NUM);
	$review = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
try {
	$stmt = $conn->prepare("SELECT COUNT(*) FROM `product`");
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_NUM);
	$product = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
try {
	$stmt = $conn->prepare("SELECT COUNT(*) FROM `product_review`");
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_NUM);
	$product_review = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$conn = null;
// SEO purpose
$title = 'Statistics | CryptoCanary';
$description = 'Login to your CryptoCanary account and review projects...';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('head.php'); ?>
</head>
<body>
	<?php include('nav.php'); ?>
	<main class="container">
		<h1 class="pt-3">Platform Stats Overviews</h1>
		<p>Date: <?php echo date(DATE_RFC2822); ?></p>
		<table class="table table-striped">
			<tbody>
				<tr><td>Users:</td><td class="text-right"><?php echo number_format($users['0']['0'],0); ?></td></tr>
				<tr><td>Coins:</td><td class="text-right"><?php echo number_format($entities['0']['0'],0); ?></td></tr>
				<tr><td>Coins Reviews:</td><td class="text-right"><?php echo number_format($review['0']['0'],0); ?></td></tr>
				<tr><td>Product:</td><td class="text-right"><?php echo number_format($product['0']['0'],0); ?></td></tr>
				<tr><td>Product Reviews:</td><td class="text-right"><?php echo number_format($product_review['0']['0'],0); ?></td></tr>
			</tbody>
		</table>
	</main>
	<?php include('endScripts.php'); ?>
</body>
</html>
