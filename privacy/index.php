<?php
session_start();
$title = 'Privacy Policy | CryptoCanary';
$description = 'By using this Website you express consent with our Privacy Policy and use of cookies. If you do not consent, you are not allowed to use this Website.';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php'); ?>
</head>
<body>
	<?php include('../inc/nav.php'); ?>
	<main class="container pt-3 pb-3">
		<h1>Privacy Policy</h1>
		<p>EFFECTIVE DATE: JANUARY 11, 2019.</p>
		<p>At Cryptocanary.app, we collect and manage user data according to the following Privacy Policy.</p>
		<p>By using this Website you express consent with our Privacy Policy and use of cookies. If you do not consent, you are not allowed to use this Website.</p>
		<h2>Data Collected</h2>
		<p>We collect information you provide directly to us. For example, we collect information when you create an account, subscribe, participate in any interactive features of our services, fill out a form, request customer support or otherwise communicate with us. The types of information we may collect include your name, email address, postal address, credit card information and other contact or identifying information you choose to provide.</p>
		<p>We collect anonymous data from every visitor of the Website to monitor traffic and fix bugs. For example, we collect information like web requests, the data sent in response to such requests, the Internet Protocol address, the browser type, the browser language, and a timestamp for the request.</p>
		<p>We also use various technologies to collect information, and this may include sending cookies to your computer. Cookies are small data files stored on your hard drive or in your device memory that helps us to improve our services and your experience, see which areas and features of our services are popular and count visits. We may also collect information using web beacons (also known as "tracking pixels"). Web beacons are electronic images that may be used in our services or emails and to track count visits or understand usage and campaign effectiveness.</p>
		<h2>Use of the Data</h2>
		<p>We only use your personal information to provide you the Cryptocanary.app services or to communicate with you about the Website or the services.</p>
		<p>We offer users and visitors who provide contact information a means to choose how we use the information provided.</p>
		<ul>
			<li>You may manage your receipt of marketing and non-transactional communications by clicking on the "unsubscribe" link located on the bottom of Cryptocanary.app’s marketing emails.</li>
			<li>We may send you transactional emails that are related to the normal use of our services on the basis of legitimate interest. This may include for example email notifications for the approval of submitted content. You may manage your receipt of these types of (optional) transactional communications in your account settings.</li>
			<li>Additionally, you may send a request specifying your communications preferences if your personal communication preferences aren’t adjustable in the website’s settings. Send this request to <a href="mailto:team@cryptocanary.app">team@cryptocanary.app</a>.</li>
			<li>Customers cannot opt out of receiving transactional emails related to their account with Cryptocanary.app, for example password reset emails.</li>
		</ul>
		<p>We employ industry standard techniques to protect against unauthorized access of data about you that we store, including personal information. If and when there has been a data breach of personal information from our users, we will notify users by email about this breach.</p>
		<p>We do not share personal information you have provided to us without your consent, unless:</p>
		<ul>
			<li>Doing so is appropriate to carry out your own request</li>
			<li>We believe it's needed to enforce our legal agreements or that is legally required</li>
			<li>We believe it's needed to detect, prevent or address fraud, security or technical issues or otherwise protect our property, legal rights, or that of others</li>
		</ul>
		<h2>Sharing of Data</h2>
		<p>We don't share your personal information with third parties. Aggregated, anonymized data is periodically transmitted to external services to help us improve the Website and service.</p>
		<p>We may allow third parties to provide analytics services. These third parties may use cookies, web beacons and other technologies to collect information about your use of the services and other websites, including your IP address, web browser, pages viewed, time spent on pages, links clicked and conversion information.</p>
		<p>We also use social buttons provided by services like Twitter, Google+, LinkedIn and Facebook. Your use of these third party services is entirely optional. We are not responsible for the privacy policies and/or practices of these third party services, and you are responsible for reading and understanding those third party services' privacy policies.</p>
		<p>In the future we may provide third parties with the ability to provide customizable or targeted advertising or messages and in such cases we may allow third party applications to access your user data in determining the messaging or advertising applicable to you. Please check our Privacy Policy on a time to time basis for any adjustments.</p>
		<h2>International Data Collection and Transfer</h2>
		<p>The Company’s Web site and Services are hosted in the United States. If you are accessing the Web site and/or Services from the European Union, Asia, or any other region with laws or regulations governing personal data collection, use and disclosure that differ from United States laws, please note that you are transferring your personal data to the United States which does not have the same data protection laws as the EU and other regions. By providing your personal data you consent to:</p>
		<ul>
			<li>The use of your personal data for the uses identified above in accordance with the Privacy Policy;</li>
			<li>and the transfer of your personal data to the United States as indicated above.</li>
		</ul>
		<p>To facilitate Cryptocanary.app’s operations, the Company may transfer and access Data about users from around the world, including the United States. This Privacy Statement shall apply even if Cryptocanary.app sends data about users to other countries.</p>
		<p>If you reside in the European Union, upon registration for any applicable Cryptocanary.app service or offering you consent that the personal information you have provided may be transferred and stored in countries outside the EU.</p>
		<h2>Data Controller</h2>
		<p>Crypto Business Consulting Corp. is the controller of data for Cryptocanary.app. If you have any inquiries about your data or data protection, please send an email to: <a href="mailto:team@cryptocanary.app">team@cryptocanary.app</a>.</p>
		<h2>Opt-Out, Communication Preferences</h2>
		<p>You may modify your communication preferences and/or opt-out from specific communications at any time. Please specify and adjust your preferences.</p>
		<h2>Security</h2>
		<p>We take reasonable steps to protect personally identifiable information from loss, misuse, and unauthorized access, disclosure, alteration, or destruction. But, you should keep in mind that no Internet transmission is ever completely secure or error-free. In particular, email sent to or from the Sites may not be secure.</p>
		<p>Please note that information you enter for your user profile or in your posts may be accessed and viewed publicly and therefore you should not post information that you consider confidential or that you do not want distributed publicly. Although in some areas you will have the ability to edit or delete information, please note that even this information may have been cached on servers, cut and pasted or otherwise incorporated into other posts or for other uses, and so once information is posted you should consider that it may be publicly available perpetually, and in different locations and/or media than your original submission.</p>
		<h2>About Children</h2>
		<p>The Website is not intended for children under the age of 13. We do not knowingly collect personally identifiable information via the Website from visitors in this age group. We will purge any information we receive from people we believe to be children under 13 from our database and cancel the corresponding accounts.</p>
		<h2>Changes to the Privacy Policy</h2>
		<p>We may amend this Privacy Policy from time to time. This Privacy Policy was created by privacy-policy-template.com for www.cryptocanary.app. Use of information we collect now is subject to the Privacy Policy in effect at the time such information is used.</p>
		<p>If Cryptocanary.app merges with or is acquired by another company, the protections in this privacy policy may be subject to change. In this event, Cryptocanary.app will notify you before information about you is transferred and becomes subject to a different privacy policy.</p>
	</main>
	<?php include('../inc/footer.php'); ?>
	<?php include('../inc/endScripts.php'); ?>
</body>
</html>
