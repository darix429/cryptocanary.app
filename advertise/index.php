<?php
session_start();
date_default_timezone_set('America/Lima');
$token =  md5('cryptocanary'.uniqid());
$_SESSION['tokenSubmit'] = $token;
$success = filter_input(INPUT_GET,'success',FILTER_SANITIZE_STRING);
$error = filter_input(INPUT_GET,'error',FILTER_SANITIZE_STRING);

// SEO purpose
$title = 'Advertise with us | CryptoCanary';
$description = 'Advertising possibilities with CryptoCanary';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php'); ?>
	<style>
		body{background-color: #fff; color: #222;}
	</style>
</head>
<body>
	<?php include('../inc/nav.php'); ?>
	<main class="container pt-5 pb-5">
		<h1>Advertise with us</h1>
		<p>Thanks for your interest in advertising with us!  Please fill out the form below and we'll contact you shortly with more info.</p>
		<div id="result"></div>
		<?php
		if (strlen($success)>1) {
			echo '<div class="alert alert-success alert-dismissible fade show" role="alert" id="advertiseSuccess"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h3>Thank you</h3><p>We will contact you soon...</p></div>';
		}
		if (strlen($error)>1) {
			echo '<div class="alert alert-danger alert-dismissible fade show" role="alert" id="advertiseError"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h2>Error!</h2><p>'.$error.'<br>Please Try Again...</p></div>';
		}
		?>
		<form action="submit.php" method="POST" role="form">
			<div class="form-group">
				<label class="form-control-label" for="email"><strong>Your Email address *</strong></label>
				<input type="email" name="email" id="email" class="form-control" required="required" minlength="3" autofocus="">
				<input type="hidden" name="time" value="<?php echo time(); ?>">
				<input type="hidden" name="token" value="<?php echo $token; ?>">
			</div>
			<div class="form-group">
				<label class="form-control-label" for="name"><strong>Your Name *</strong></label>
				<input type="text" name="name" id="name" class="form-control" required="required" minlength="3">
			</div>
			<div class="form-group">
				<label class="form-control-label" for="companyName"><strong>Your Company Name / Website *</strong></label>
				<input type="text" name="companyName" id="companyName" class="form-control" required="required" minlength="3">
			</div>
			<div class="form-group">
				<p><strong>Which option(s) are you interested in? *</strong></p>
				<?php
				$checkbox = array('Targeted ads on CryptoCanary.app','General banner ads on CryptoCanary.app','Shout out in weekly newsletter','Other');
				foreach ($checkbox as $key => $value) {
					echo '<div class="form-check"><label class="form-check-label"><input type="checkbox" class="form-check-input" name="'.$value.'" id="'.$value.'" value="'.$value.'">'.$value.'</label></div>';
				}
				?>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="budget"><strong>What is your approximate budget? *</strong></label>
				<input type="number" name="budget" id="budget" class="form-control" required="required" min="1">
			</div>
			<div class="form-group">
				<p><strong>Additional Options</strong></p>
				<?php
				$checkbox = array('I am looking to target specific demographics or geographical regions.','I am an advertising agency looking to bring potential clients to you','I am interested in paying with crypto (BTC,ETH)');
				foreach ($checkbox as $key => $value) {
					echo '<div class="form-check"><label class="form-check-label"><input type="checkbox" class="form-check-input" name="'.$value.'" id="'.$value.'" value="'.$value.'">'.$value.'</label></div>';
				}
				?>
			</div>
			<div class="form-group">
				<label class="form-control-label" for="comments"><strong>Additional Comments/Questions</strong></label>
				<textarea class="form-control" name="comments" id="comments" rows="3"></textarea>
			</div>
			<button type="submit" class="btn btn-primary">Submit</button> <i id="spinner" class="fas fas-spinner fa-spin" style="display:none"></i>
		</form>
	</main>
	<?php include('../inc/footer.php'); ?>
	<?php include('../inc/endScripts.php'); ?>
</body>
</html>
