<div id="top10Risky">
	<h2>Riskiest cryptocurrencies</h2>
<?php
$count=0;
	foreach ($data as $key => $row) {
		$json = json_decode($row['links'], true);
		$link = strtolower($json[0]['link']);
		$count++;
		if ($count % 4 == 1){echo '<div class="row pb-3 pt-3">';}
		echo '<div class="col-md-6 col-xl-3">
			<div class="media">
				<div class="media-body">
				<table style="width:100%"><tr><td><img src="/images/entities/'.$row['entity_id'].'/'.$row['image'].'" alt="'.$row['entity_name'].'" class="img-fluid" style="height: 30px;"></td><td><strong>'.$row['entity_name'].' ('.$row['symbol'].')</strong></td><td class="alert alert-success text-center">51%</td></tr></table>
					<p class="alert alert-secondary text-justify"><small>'.substr($row['entity_desc'],0,200).'...</small><br><small><a href="'.$link.'">'.$link.'</a></small></p>
					<table style="width:100%; font-size: 80%;"><tr><td><i class="fas fa-calendar-alt"></i> 2018</td><td>ICO: <i class="fas fa-check text-success"></i></td><td>FORK: <i class="fas fa-times text-danger"></i></td></tr></table>
				</div>
			</div>
		</div>';
		if ($count % 4 == 0){echo '</div>';}
	}
?>
</div>
