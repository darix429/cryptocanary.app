<?php
session_start();
require_once './config.php';

if (!empty($_GET['error'])) {
	exit('Got error: ' . htmlspecialchars($_GET['error'], ENT_QUOTES, 'UTF-8'));
} else {
	$token = $google->getAccessToken('authorization_code', [
		'code' => $_GET['code']
	]);
	try {
		$user = $google->getResourceOwner($token);

		$USER = new User($conn);
		$profile = $USER->getUserDetailsByEmail($user->getEmail());

		if (!$profile) {
			if (isset($_SESSION['referer'])) {
				$outputFile = '../logs/ph.txt';
				$email = $user->getEmail();
				$link = 'https://cryptocanary.app/activate/?id='.base64_encode($email);
				$log = date(DATE_RFC2822).', '.$email.', '.$link.', '.$_SESSION['referer'].', google'.PHP_EOL;
				file_put_contents($outputFile,$log,FILE_APPEND);
			}
			header('Location: /activateSocial/?auth_method=google&id='.base64_encode($user->getEmail()));
		} else {
			$_SESSION['profile'] = $profile;
			$_SESSION['auth_method'] = 'google';
			$_SESSION['LoggedIn'] = 'LoggedIn';
			$_SESSION['user_id'] = $profile['user_id'];
			$_SESSION['username'] = $profile['username'];
			$_SESSION['avatar'] = $profile['avatar'];
			$_SESSION['chirps'] = $profile['score'];
			$outputFile = 'authentication.log';
			$log = time().', '.$profile['user_id'].', google'.PHP_EOL;
			file_put_contents($outputFile,$log,FILE_APPEND);
			$http_referer = $_SESSION['http_referer'];
			if (strpos($http_referer, 'cryptocanary') === false OR !filter_var($http_referer, FILTER_VALIDATE_URL)) {
				$http_referer = 'https://cryptocanary.app/browse/';
			}
			header("Location: $http_referer");
		}
	} catch (Exception $e) {
		exit('Something went wrong: ' . $e->getMessage());
	}
}

