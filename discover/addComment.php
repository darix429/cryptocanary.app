<?php
session_start();
if (!isset($_SESSION['user_id'])) {
	exit('<div class="alert alert-danger" role="alert" id="commentAddError"><p>You should login to your account first.</p><p><a href="/login/" class="btn btn-primary">Login here</a></p></div>');
}
$user_id = (int)filter_var($_SESSION['user_id'],FILTER_SANITIZE_NUMBER_INT);
if ($user_id<1) {
	exit('<div class="alert alert-danger" role="alert" id="commentAddError"><p>You should login to your account first.</p><p><a href="/login/" class="btn btn-primary">Login here</a></p></div>');
}
$username = filter_var($_SESSION['username'],FILTER_SANITIZE_STRING);
$comment = trim(preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', filter_input(INPUT_POST,'comment',FILTER_SANITIZE_STRING)));
$review_id = filter_input(INPUT_POST,'review_id',FILTER_SANITIZE_NUMBER_INT);
$iNumber = filter_input(INPUT_POST,'iNumber',FILTER_SANITIZE_NUMBER_INT);
require('../db.php');
try {
	$stmt1 = $conn->prepare('SELECT `comments` FROM `product_review` WHERE `review_id` = :review_id LIMIT 1');
	$stmt1->bindParam(':review_id', $review_id, PDO::PARAM_INT);
	$stmt1->execute();
	$stmt1->setFetchMode(PDO::FETCH_NUM);
	$comments = $stmt1->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$arr = json_decode($comments[0][0], true);
$newID = (int)count($arr)+1;
$arr[] = ['id' => $newID, 'comment' => $comment, 'user_id' => $user_id, 'timestamp' => time(), 'username' => $username];
$json = json_encode($arr);
try {
	$stmt = $conn->prepare('UPDATE `product_review` SET `comments` = :comments WHERE `review_id` = :review_id');
	$stmt->bindParam(':comments', $json, PDO::PARAM_STR);
	$stmt->bindParam(':review_id', $review_id, PDO::PARAM_INT);
	$stmt->execute();
	$count = $stmt->rowCount();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}

// get reviewer info
try {
	$getReviewer = $conn->prepare('SELECT product_review.user_id, user.email, username, notifyComment, product_name FROM product_review LEFT JOIN user ON product_review.user_id=user.user_id LEFT JOIN product ON product_review.product_id=product.product_id WHERE review_id = :review_id');
	$getReviewer->bindParam(':review_id', $review_id, PDO::PARAM_INT);
	$getReviewer->execute();
	$reviewer = $getReviewer->fetch();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}

// send comment notification to reviewer
if ($reviewer["user_id"] != $user_id && $reviewer["notifyComment"] == 1) {
	require("../class/util.php");
	$util = Util::getInstance();
	$snippet = $comment;
	if (strlen($comment) > 120) {
		$snippet = substr($comment, 0, 120);
		$snippet .= "...";
	}
	$link = 'https://cryptocanary.app/discover/'.urlencode($reviewer['product_name']);
	$link2 = 'https://cryptocanary.app/emailNotification/?id='.base64_encode($reviewer['email']);
	$emailData = array(
		"email"=>$reviewer["email"],
		"template"=>"new-comment.html",
		"subject"=>"$username Commented On Your Review.",
		"data" => array(
			"user"=>$username,
			"snippet"=>$snippet,
			"project"=>$reviewer["product_name"],
			"link"=>$link,
			"link2"=>$link2
		)
	);
	if (strpos(__DIR__,'/var/www/html') !== false) {
    	$util->queueEmail($emailData);
	}
}
$conn = null;
echo '<div class="alert alert-success alert-dismissible fade show" role="alert" id="commentAddSuccess"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><p>Thank you for your comment!</p></div>';
include('reviewVoting.php');
exit();
foreach ($arr as $key => $row) {
	foreach ($row as $key => $value) {
		if ($key == 'comment') {
			echo '<p class="comment">'.$row['comment'].'<br><small><strong><a href="/user/'.urlencode($row['username']).'">'.$row['username'].'</a></strong> - '.date('Y-m-d H:m:s',$row['timestamp']).'</small></p>';
		}
	}
}

