<div id="top10review">
	<h2>Latest Reviews:</h2>
	<div class="row">
		<div class="col-md-3">
			<p>These reviews are making a difference</p>
			<p class="small">Contribute in making cryptocurrencies a safe space for everyone.</p>
			<p><a href="#" class="btn btn-primary">View more</a></p>
		</div>
		<?php
		foreach ($review as $key => $row) {
			echo '<div class="col-md-3">
			<table style="width:100%"><tr><td><img src="/images/entities/'.$row['entity_id'].'/'.$row['image'].'" alt="'.$row['entity_name'].'" class="img-fluid" style="height: 30px;"></td><td><strong>'.$row['entity_name'].' ('.$row['symbol'].')</strong></td><td class="alert alert-success text-center">51%</td></tr></table>
			<p class="alert alert-secondary text-justify"><small>'.substr($row['review'],0,120).' ...<br><a href="/review/?id='.$row['entity_id'].'">Read full review...</a></small></p>
			<table style="width:100%"><tr>
			<td><img src="/images/avatars/'.$row['user_id'].'/'.$row['avatar'].'" class="img-fluid" alt="'.$row['avatar'].'" style="height: 30px;"></td>
			<td><strong>'.$row['username'].'</strong> gave: '.$row['rating'].' <i class="fas fa-star"></i></td>
			</tr></table>
			</div>';
		}
		?>
	</div>
</div>
