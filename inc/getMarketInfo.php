<?php
$csv = array_map('str_getcsv', file('data/coinList.csv'));
foreach ($csv as $key => $value) {
	if (trim(strtolower($value[2])) == trim(strtolower($symbol)) || trim(strtolower($value[1])) == trim(strtolower($entity_name))) {
		$coinloreID = $value[0];
	}
}
if ($coinloreID > 1) {
	$coinloreURL = 'https://api.coinlore.com/api/ticker/?id='.$coinloreID;
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $coinloreURL);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	$response = curl_exec($ch);
	curl_close($ch);
	$coinloreData = json_decode($response,true);
	if (!empty($coinloreData)) {
		$coinloreSymbol = $coinloreData[0]["symbol"];
		$coinlorePrice_usd = $coinloreData[0]["price_usd"];
		$coinlorePercent_change_24h = $coinloreData[0]["percent_change_24h"].' %';
		$coinlorePercent_change_1h = $coinloreData[0]["percent_change_1h"].' %';
		$coinloreMarket_cap_usd = (float)$coinloreData[0]["market_cap_usd"];
		$coinloreVolume_24 = (float)$coinloreData[0]["volume24"];
		$coinloreMarket_cap_usd = (float)$coinloreData[0]["market_cap_usd"];
	}
	else {
		$coinlorePrice_usd = $coinlorePercent_change_24h = $coinlorePercent_change_1h = $coinloreMarket_cap_usd = '';
	}
} else {
	$coinlorePrice_usd = $coinlorePercent_change_24h = $coinlorePercent_change_1h = $coinloreMarket_cap_usd = '';
}
require('db.php');
try {
	$getNoExchanges = $conn->prepare("SELECT entity_id, exchange_count FROM entity WHERE entity_id = :id");
	$getNoExchanges->bindParam(":id", $id, PDO::PARAM_INT);
	$getNoExchanges->execute();
	$getNoExchangesRes = $getNoExchanges->fetch();
	$numberOfExchanges = $getNoExchangesRes["exchange_count"];
} catch (PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
