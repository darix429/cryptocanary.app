<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php'); ?>
</head>
<body>
	<?php include('../inc/nav.php'); ?>
	<main class="container pt-5 pb-5">
		<div class="text-center"><img src="/img/logo.svg" style="width: 400px;" class="img-fluid" alt="CryptoCanary logo"></div>
		<?php
		if (strlen($error)>1) {
			echo '<div class="alert alert-danger" role="alert"><h3>Error!</h3><p>'.$error.'</br>Try Again!</p></div>';
		}
		?>
		<div id="result"></div>
		<p>A password reset code was sent to your email at:<br><strong><?php echo $email; ?></strong></p>
		<form ic-post-to="password2.php" ic-target="#result" ic-indicator="#spinner" method="POST" role="form">
			<legend>Password Reset</legend>
			<div class="form-group">
				<label class="form-control-label" for="reset">Your reset Code:</label>
				<input type="text" name="reset" id="reset" class="form-control" required="required" minlength="5">
				<input type="hidden" name="email" value="<?php echo $email; ?>">
				<input type="hidden" name="token" value="<?php echo $otken; ?>">
			</div>
			<div class="form-group">
				<label class="form-control-label" for="newPassword">Your new password:</label>
				<input type="text" name="newPassword" id="newPassword" class="form-control" required="required" minlength="5">
			</div>
			<button type="submit" class="btn btn-primary"><i class="fas fa-sign-in-alt"></i> Reset your password</button> <i id="spinner" class="fa fa-spinner fa-spin" style="display:none"></i>
		</form>
		<hr>
		<p>By logging in, you are agreeing to our <a href="/terms/">Terms of Service</a> and <a href="/privacy/">Privacy Policy</a>.</p>
	</main>
	<?php include('../inc/footer.php'); ?>
	<?php include('../inc/endScripts.php'); ?>
</body>
</html>
