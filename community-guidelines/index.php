<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php'); ?>
</head>
<body>
	<?php include('../inc/nav.php'); ?>
	<main class="container pt-5 pb-5">
		<h1>Community Guidelines:</h1>
		<p>The goal of CryptoCanary is to provide crypto enthusiasts a place to research, discuss, and discover various projects and entities via user-generated content.</p>
		<p>In order to deliver on this mission, it's vital that the site offers users with legitimate reviews spotlighting both good and bad crypto projects.</p>
		<p>It's equally important users feel their opinions are welcomed and valued.</p>
		<p>With this in mind, we ask that everyone in the CryptoCanary community adhere to the following guidelines:</p>
		<p><strong>First,</strong> reviews should be original, high-quality, cleary written, and free of disinformation. Reviews should include as much supporting detail as possible and offer examples to back up assertions.</p>
		<p>Reviews that reek of spam, shilling, brigading, or appear to duplicate the work of others will be deleted.</p>
		<p>Here's an example of a low quality review that would likely be deleted:<br><em>"ABC coin is my favorite coin, I have followed it for a while now. I love the project and the team. It will go to the moon for sure!"</em></p>
		<p>An example of a better review would be (notice the specific details included):<br><em>"I have been following the ABC coin project since early 2017. The team has PhDs from Harvard and their new consensus algorithm solves the scalability problem brilliantly. I'm also a big fan of their token issuance via airdrop and how they only did a 10% premine. Very fair in my opinion!"</em></p>
		<p><strong>Secondly,</strong> CryptoCanary has a zero tolerance for any content that encourages violence, hate, belittlement, personal attacks, or general toxicity. </p>
		<p>By using CryptoCanary, you confirm your agreement to these terms and conditions.</p>
		<p>Content or materials violating these terms will be removed from the site and the author will be notified. Repeat offenders may be permanently banned from contributing to CryptoCanary.</p>
		<p>Because we are a community, the onus of conduct enforcement is a community effort. We would greatly appreciate if you could report any violations to <a href="mailto:team@cryptocanary.app">team@cryptocanary.app</a>.</p>
	</main>
	<?php include('../inc/footer.php'); ?>
	<?php include('../inc/endScripts.php'); ?>
</body>
</html>
