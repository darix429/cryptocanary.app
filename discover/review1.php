<?php
date_default_timezone_set('America/Lima');
$token =  md5('cryptocanary'.uniqid());
$_SESSION['tokenSubmit'] = $token;
if (isset($_SESSION['LoggedIn'])) {
	$sessionLoggedIN = 'LoggedIn';
} else {
	$sessionLoggedIN = '';
}
$rating = 0;
if ($sessionLoggedIN == 'LoggedIn') {
	$user_id = $_SESSION['user_id'];
	if (strpos(__DIR__,'/Applications/MAMP/') !== false) {
		require('/Applications/MAMP/htdocs/cryptocanary/db.php');
	} else {
		require('/var/www/html/db.php');
	}
	try {
		$stmt2 = $conn->prepare('SELECT review_id, user_id, product_id, review, review_title, rating FROM `product_review` WHERE `product_id` = :product_id AND `user_id` = :user_id ORDER BY review_id DESC LIMIT 1');
		// Bind Parameter
		$stmt2->bindParam(':product_id', $product_id, PDO::PARAM_INT);
		$stmt2->bindParam(':user_id', $user_id, PDO::PARAM_INT);
		$stmt2->execute();
		$stmt2->setFetchMode(PDO::FETCH_ASSOC);
		$data2 = $stmt2->fetchAll();
	}
	catch(PDOException $e) {
		echo 'Error: ' . $e->getMessage();
	}
	$conn = null;
	if (!empty($data2)) {
		$rating = $data2[0]['rating'];
		$userReview= $data2[0]['review'];
		$review_id = $data2[0]['review_id'];
		$updateCode = '<input type="hidden" name="updateCode" value="'.$review_id.'">';
		$buttonText = 'Change your Review';
	} else {
		$userReview = $updateCode = '';
		$buttonText = 'Post your Review';
	}
} else {
	$userReview = $updateCode = '';
	$buttonText = 'Post your Review';
}
if (isset($jsonUserReview)) {
	$userReview = $jsonUserReview;
}
// Get the data from the user information before he was logged in
if (isset($_SESSION['jsonData'])) {$jsonData = $_SESSION['jsonData'];}
if (!empty($jsonData)) {
	$jsonDataArray = json_decode($jsonData, true);
	$rating = $jsonDataArray['rating'];
	$userReview = $jsonDataArray['review'];
	$diplayReview = 'display:block';
} else {
	$diplayReview = 'display:none';
}

?>
<p><button type="button" class="btn btn-primary" onClick="$('#addReview').show();$('#claimButton').hide();"><i class="fas fa-plus"></i> Write your review</button> <button type="button" class="ml-3 btn btn-success" onClick="$('#claimButton').show();$('#addReview').hide();"><i class="fas fa-exclamation-circle"></i> Claim this product</button></p>
<div class="row" id="addReview" style="<?php echo $diplayReview; ?>;background-color: #fff; margin-top: 20px; padding: 20px 0px;">
	<div class="col-12">
		<h2>Write your review</h2>
		<form action="addReview.php" method="POST" role="form">
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label class="form-control-label" for="rating">Rate this project: <button type="button" class="btn btn-link" data-toggle="tooltip" data-placement="top" title="What is your overall experience with this product?"><i class="fas fa-question-circle"></i></button></label>
						<input type="text" id="rating" name="rating" class="kv-rtl-theme-svg-star rating-loading" value="<?php echo $rating; ?>" data-size="xs" required="required">
					</div>
				</div>
			</div>
			<p class="pt-3"><i>Please explain your rating in detail</i></p>
			<div class="form-group">
				<textarea class="form-control" name="review" id="review" rows="5" required="required" minlength="160" placeholder="Share your personal experience about this project! We appreciate honest, transparent, and detailed reviews."><?php echo $userReview; ?></textarea>
				<small id="reviewlHelp" class="form-text text-muted">Minimum 160 characters.</small>
				<input type="hidden" name="product_id" value="<?php echo $product_id; ?>">
				<input type="hidden" name="token" value="<?php echo $token; ?>">
				<?php echo $updateCode; ?>
			</div>
			<button type="submit" class="btn btn-primary"><?php echo $buttonText; ?></button>
			<p><small><a href="/community-guidelines/" target="_blank">Reviews violating our Community Guidelines may be removed.</a></small></p>
		</form>
	</div>
</div>
<div class="row" id="claimButton" style="display:none; background-color: #fff; margin-top: 20px; padding: 20px 0px;">
	<div class="col-12">
		<h1>Claim this product</h1>
		<p>Would you like to customize this page to get visitors interested in your product?</p>
		<p>Please submit your company email address below:</p>
		<form action="claim.php" method="POST" role="form">
			<div class="form-group">
				<input type="email" name="email" id="email" class="form-control" required="required" minlength="6" placeholder="Your Email Address" value="<?php if (isset($userEmail)) {echo $userEmail;} ?>">
				<small id="emailHelp" class="form-text text-muted">Email is to verify that you are actually affiliated with the company. Usually we expect marketing/community managers or founders to claim a product.</small>
				<input type="hidden" name="http_referer" value="https://cryptocanary.app/discover/<?php echo $product_name; ?>">
			</div>
			<button type="submit" class="btn btn-success">Request your claim</button>
		</form>
	</div>
</div>
