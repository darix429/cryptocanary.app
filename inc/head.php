<?php
$theme = filter_input(INPUT_GET,'theme',FILTER_SANITIZE_NUMBER_INT);
$themes = array('1'=>'cerulean','2'=>'cosmo','3'=>'cyborg','4'=>'darkly','5'=>'flatly','6'=>'journal','7'=>'litera','8'=>'lumen','9'=>'lux','10'=>'materia','11'=>'minty','12'=>'pulse','13'=>'sandstone','14'=>'simplex','15'=>'sketchy','16'=>'slate','17'=>'solar','18'=>'spacelab','19'=>'superhero','20'=>'united','21'=>'yeti');
if (!isset($title)) { $title = 'CryptoCanary - Let\'s stop crypto scams with community-powered reviews!';}
if (!isset($description)) { $description = 'CryptoCanary consolidates user reviews/ratings in order to better warn the broader cryptocurrency world of potentially shady projects, coins, etc...';}
$brand = 'CryptoCanary';
$domain = 'https://cryptocanary.app';
if (isset($img)) {
	$img = $img;
} elseif (isset($logo)) {
	$img = 'https://cryptocanary.app/images/entities/'.$logo;
} else {
	$img = 'https://cryptocanary.app/images/Cryptocanary-logo-v2.png';
}
$url = $domain.$_SERVER['REQUEST_URI'];
if (!isset($keywords)) {
	$keywords = 'crypto reviews, cryptocurrency reviews, altcoin reviews, crypto scams, list of crypto scams, unbiased cryptocurrency reviews, honest cryptocurrency reviews, cryptocurrency expert ratings, altcoin ratings';
}
?>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title><?php echo $title; ?></title>
<meta name="description" content="<?php echo $description; ?>">
<meta name="keywords" content="<?php echo $keywords; ?>">
<meta name="robots" content="index, follow">
<meta name="googlebot" content="index,follow">
<meta name="author" content="Canary Reviews LLC">
<meta name="subject" content="<?php echo $title; ?>">
<meta name="copyright" content="Canary Reviews LLC">
<meta name="rating" content="General">
<link rel="canonical" href="<?php echo $url; ?>">

<!-- Twitter Card data -->
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@<?php echo $brand; ?>">
<meta name="twitter:creator" content="@<?php echo $brand; ?>">
<meta name="twitter:url" content="<?php echo $url; ?>">
<meta name="twitter:title" content="<?php echo $title; ?>">
<meta name="twitter:description" content="<?php echo $description; ?>">
<meta name="twitter:image" content="<?php echo $img; ?>">

<!-- Open Graph data -->
<meta property="og:type" content="article">
<meta property="og:title" content="<?php echo $title; ?>">
<meta property="og:url" content="<?php echo $url; ?>">
<meta property="og:image" content="<?php echo $img; ?>">
<meta property="og:description" content="<?php echo $description; ?>">
<meta property="fb:app_id" content="1396440270491537">

<!-- Add to Home Screen -->
<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta name="apple-mobile-web-app-title" content="<?php echo $brand; ?>">

<!-- Favicon -->
<link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192" href="/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
<link rel="manifest" href="/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">

<meta name="google-site-verification" content="d18S25iSgzjCs3bAmAlabSyV9ZQ4P1pRkOUqCC4TrqI" />
<meta name="msvalidate.01" content="898FBD32CF2FA635717D0FB94A747134" />

<!-- Style -->

<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/social-share-kit/1.0.15/css/social-share-kit.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/easy-autocomplete/1.3.5/easy-autocomplete.themes.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.19/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/1.0.7/css/responsive.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.6/css/star-rating.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-star-rating/4.0.6/themes/krajee-svg/theme.min.css">
<link rel="stylesheet" type="text/css" href="/font/Inter/inter.css">
<link rel="stylesheet" type="text/css" href="/font/sonnyGothic/stylesheet.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.4.1/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="/custom.css?ver=<?php echo date('ymd'); ?>">
<?php
if ($theme>0 AND $theme < 22) {
	echo '<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/4.4.1/'.$themes[$theme].'/bootstrap.css">';
	echo '<link rel="stylesheet" type="text/css" href="/custom2.css">';
}
if (isset($_SESSION['LoggedIn'])) {
	$trackLoggedIn = 'Logged In';
	$trackUser_id = 1000+(int)$_SESSION['user_id'];
	$trackCode = ",'user_id': '".$trackUser_id."'";
} else {
	$trackLoggedIn = 'Logged Out';
	$trackUser_id = 0;
	$trackCode ='';
}
if (strpos(__DIR__,'/var/www/html') === 0) {
	echo "
	<script>
	window.dataLayer = window.dataLayer || [];
	window.dataLayer.push({
	'authentication': '".$trackLoggedIn."'".$trackCode."
	});
	</script>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-MCPGV59');</script>
	<!-- End Google Tag Manager -->";
}

