<?php
date_default_timezone_set('America/Lima');
$token = md5('cryptocanary'.date('Ymd'));
if (isset($_SESSION['LoggedIn'])) {
	$sessionLoggedIN = 'LoggedIn';
} else {
	$sessionLoggedIN = '';
}
if ($sessionLoggedIN == 'LoggedIn') {
	$user_id = $_SESSION['user_id'];
	if (strpos(__DIR__,'/Applications/MAMP/') !== false) {
		require('/Applications/MAMP/htdocs/cryptocanary/db.php');
	} else {
		require('/var/www/html/db.php');
	}
	try {
		$stmt2 = $conn->prepare('SELECT review_id, user_id, entity_id, review, review_title, rating, team_quality, info_quality, track_record FROM `review` WHERE entity_id = :entity_id AND `user_id` = :user_id ORDER BY review_id DESC LIMIT 1');
		// Bind Parameter
		$stmt2->bindParam(':entity_id', $entity_id, PDO::PARAM_INT);
		$stmt2->bindParam(':user_id', $user_id, PDO::PARAM_INT);
		$stmt2->execute();
		$stmt2->setFetchMode(PDO::FETCH_ASSOC);
		$data2 = $stmt2->fetchAll();
	}
	catch(PDOException $e) {
		echo 'Error: ' . $e->getMessage();
	}
	if (!empty($data2)) {
		$rating = $data2[0]['rating'];
		$userReview= $data2[0]['review'];
		$review_id = $data2[0]['review_id'];
		$team_quality = $data2[0]['team_quality'];
		$info_quality = $data2[0]['info_quality'];
		$track_record = $data2[0]['track_record'];
		$updateCode = '<input type="hidden" name="updateCode" value="'.$review_id.'">';
		$buttonText = 'Change your Review';
	} else {
		$userReview = $updateCode = '';
		$buttonText = 'Post your Review';
	}
} else {
	$userReview = $updateCode = '';
	$buttonText = 'Post your Review';
}
if (isset($jsonUserReview)) {
	$userReview = $jsonUserReview;
}
if (!empty($jsonData)) {
	echo '<div class="row" id="addReview" style="background-color: #fff; margin-top: 20px;">';
} else {
	echo '<p class="pt-3" id="addReviewButton"><button type="button" class="btn btn-primary" onClick="$(\'#addReview\').show(); $(\'#addReviewButton\').hide()"><i class="fas fa-plus"></i> Write your review</button></p>
<div class="row" id="addReview" style="display:none; background-color: #fff; margin-top: 20px;">';
}
?>
	<div class="col-12">
		<h2>Write your review</h2>
		<form action="/review/addReviewNoAjax.php" method="POST" role="form">
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
						<label class="form-control-label" for="teamQuality">Team Quality: <button type="button" class="btn btn-link" data-toggle="tooltip" data-placement="top" title="How strong is the team and do they have the relevant experience and integrity to execute on their vision?"><i class="fas fa-question-circle"></i></button></label>
						<input type="text" id="teamQuality" name="teamQuality" class="kv-rtl-theme-svg-star rating-loading" value="<?php echo $team_quality; ?>" data-size="xs" required="required">
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label class="form-control-label" for="infoQuality">Info Quality: <button type="button" class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Is information about this project accessible, high-quality, and truthful? (e.g. whitepaper, website, etc.)"><i class="fas fa-question-circle"></i></button></label>
						<input type="text" id="infoQuality" name="infoQuality" class="kv-rtl-theme-svg-star rating-loading" value="<?php echo $info_quality; ?>" data-size="xs" required="required">
					</div>
				</div>
				<div class="col-sm-4">
					<div class="form-group">
						<label class="form-control-label" for="trackRecord">Track Record: <button type="button" class="btn btn-link" data-toggle="tooltip" data-placement="top" title="Have they delivered on prior promises and do you have confidence that they will deliver on future ones?"><i class="fas fa-question-circle"></i></button></label>
						<input type="text" id="trackRecord" name="trackRecord" class="kv-rtl-theme-svg-star rating-loading" value="<?php echo $track_record; ?>" data-size="xs" required="required">
					</div>
				</div>
			</div>
			<p class="pt-3"><i>Please explain your rating in detail</i></p>
			<div class="form-group">
				<textarea class="form-control" name="review" id="review" rows="5" required="required" minlength="160" placeholder="Share your personal experience about this project! We appreciate honest, transparent, and detailed reviews."><?php echo $userReview; ?></textarea>
				<small id="reviewlHelp" class="form-text text-muted">Minimum 160 characters.</small>
				<input type="hidden" name="entity_id" value="<?php echo $id; ?>">
				<input type="hidden" name="token" value="<?php echo $token; ?>">
				<?php echo $updateCode; ?>
			</div>
			<button type="submit" class="btn btn-primary"><?php echo $buttonText; ?></button>
			<p><small><a href="/community-guidelines/" target="_blank">Reviews violating our Community Guidelines may be removed.</a></small></p>
		</form>
	</div>
</div>
