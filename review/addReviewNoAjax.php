<?php
ob_start();
session_start();
// echo '<p>'.var_dump($_POST).'</p>'; exit();
// echo '<p>Session:<br>'.var_dump($_SESSION).'</p>';
$jsonPost = json_encode($_POST);
$_SESSION['jsonData'] = $jsonPost;
// echo '<p>'.$jsonPost.'</p>';
$http_referer = $_SERVER['HTTP_REFERER'];
// echo $http_referer;

// sanitize the session
$user_id = filter_var($_SESSION['user_id'], FILTER_SANITIZE_NUMBER_INT);
$username = filter_var($_SESSION['username'], FILTER_SANITIZE_STRING);
if (strpos(__DIR__,'/Applications/MAMP/') !== false) {
} else {
	$outputFile = '/var/www/html/logs/reviews.txt';
	$log = time().','.$user_id.','.json_encode($_POST).PHP_EOL;
	file_put_contents($outputFile,$log,FILE_APPEND);
}
$review_id = 0;
// sanitize user post
$team_quality = filter_input(INPUT_POST,'teamQuality',FILTER_SANITIZE_NUMBER_INT);
$info_quality = filter_input(INPUT_POST,'infoQuality',FILTER_SANITIZE_NUMBER_INT);
$track_record = filter_input(INPUT_POST,'trackRecord',FILTER_SANITIZE_NUMBER_INT);
$review = filter_input(INPUT_POST,'review',FILTER_SANITIZE_STRING);
$rating = 0;
$entity_id = filter_input(INPUT_POST,'entity_id',FILTER_SANITIZE_NUMBER_INT);
$token = filter_input(INPUT_POST,'token',FILTER_SANITIZE_STRING);
$review_id = filter_input(INPUT_POST,'updateCode',FILTER_SANITIZE_NUMBER_INT);
// some check
date_default_timezone_set('America/Lima');
$tokenVerif = md5('cryptocanary'.date('Ymd'));
if ($token != $tokenVerif) {
	$error = 'There was an unexpected token Error problem';
	header("Location: $http_referer?error=$error");
	exit();
}
if (strlen($review )<160) {
	$error = 'You need at least 160 characters in your review';
	header("Location: $http_referer?error=$error");
	exit();
}
if ($entity_id<1) {
	$error = 'Entity_ID is missing, please contact the support';
	header("Location: $http_referer?error=$error");
	exit();
}
if ($user_id<1) {
	$error = 'user_ID is missing, please contact the support';
	// header("Location: $http_referer?error=$error&jsonData=$jsonPost");
	header("Location: /login?message=You need to be logged in to submit a review. Don't worry, what you wrote already will still be there when you return to the page.");
	exit();
}
// store it in the database
$review_title = 'My Review';

require('../db.php');
// Check if the user has already entered something for this review
try {
	$stmt4 = $conn->prepare('SELECT review_id FROM `review` WHERE `entity_id` = :entity_id AND `user_id` = :user_id');
	$stmt4->bindParam(':entity_id', $entity_id, PDO::PARAM_INT);
	$stmt4->bindParam(':user_id', $user_id, PDO::PARAM_INT);
	$stmt4->execute();
	$count = $stmt4->rowCount();
	$stmt4->debugDumpParams();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
// if the user didn't enter any review yet, create a new record
// echo '<p>'.$count.' - '.$entity_id.' - '.$user_id.'</p>'; exit();
if ($count == 0) {
	// echo '<p>create a new entry.</p>';
	try {
		$stmt = $conn->prepare('INSERT INTO `review` (`review`, `user_id`, `entity_id`, `rating`, `review_title`, `team_quality`, `info_quality`, `track_record`) VALUES (:review, :user_id, :entity_id, :rating, :review_title, :team_quality, :info_quality, :track_record)');
		$stmt->bindParam(':review', $review, PDO::PARAM_STR);
		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
		$stmt->bindParam(':entity_id', $entity_id, PDO::PARAM_INT);
		$stmt->bindParam(':rating', $rating, PDO::PARAM_INT);
		$stmt->bindParam(':review_title', $review_title, PDO::PARAM_STR);
		$stmt->bindParam(':team_quality', $team_quality, PDO::PARAM_INT);
		$stmt->bindParam(':info_quality', $info_quality, PDO::PARAM_INT);
		$stmt->bindParam(':track_record', $track_record, PDO::PARAM_INT);
		$stmt->execute();
		$stmt->debugDumpParams();

		// send first review notification email to reviewer
		require("../class/util.php");
		$util = Util::getInstance();
		$getReviews = $conn->prepare("SELECT review_id FROM review WHERE user_id = :user_id");
		$getReviews->bindParam(":user_id", $user_id, PDO::PARAM_INT);
		$getReviews->execute();
		$getProductReviews = $conn->prepare("SELECT review_id FROM product_review WHERE user_id = :user_id");
		$getProductReviews->bindParam(":user_id", $user_id, PDO::PARAM_INT);
		$getProductReviews->execute();

		$firstReview = $getReviews->rowCount() + $getProductReviews->rowCount();
		if ($firstReview == 1) {
			$getReviewer = $conn->prepare("SELECT review_id, user.user_id, user.email, entity.entity_name FROM review LEFT JOIN user ON review.user_id = user.user_id LEFT JOIN entity ON review.entity_id = entity.entity_id WHERE user.user_id = :user_id");
			$getReviewer->bindParam(":user_id", $user_id, PDO::PARAM_INT);
			$getReviewer->execute();
			$reviewer = $getReviewer->fetch();
			$subject = "Thank you for writing your 1st review!";
			$link = 'https://cryptocanary.app/review/'.urlencode($reviewer['entity_name']);
			$link2 = 'https://cryptocanary.app/emailNotification/?id='.base64_encode($reviewer['email']);
			$emailData = array(
				"email"=>$reviewer["email"],
				"template"=>"first-review.html",
				"subject"=> $subject,
				"data" => array(
					"user"=>$username,
					"link"=>$link,
					"link2"=>$link2
				)
			);
			if (strpos(__DIR__,'/var/www/html') !== false) {
				$util->queueEmail($emailData);
			}
		}

		// send notification email to other reviewers
		$getReviewers = $conn->prepare("SELECT review.user_id, user.email, notifyReview, entity_name FROM review LEFT JOIN user ON review.user_id = user.user_id LEFT JOIN entity ON review.entity_id = entity.entity_id WHERE review.entity_id = :entity_id");
		$getReviewers->bindParam(":entity_id", $entity_id, PDO::PARAM_INT);
		$getReviewers->execute();
		$reviewers = $getReviewers->fetchAll();
		foreach($reviewers as $reviewer) {
			if ($reviewer["user_id"] != $user_id && $reviewer["notifyReview"] == 1) {
				$snippet = $review;
				if (strlen($review) > 200) {
					$snippet = substr($review, 0, 200);
					$snippet .= "...";
				}
				$subject = $reviewer["entity_name"] . " has a new review";
				$link = 'https://cryptocanary.app/review/'.urlencode($reviewer['entity_name']);
				$link2 = 'https://cryptocanary.app/emailNotification/?id='.base64_encode($reviewer['email']);
				$emailData = array(
					"email"=>$reviewer["email"],
					"template"=>"new-review.html",
					"subject"=> $subject,
					"data" => array(
						"user"=>$username,
						"snippet"=>$snippet,
						"project"=>$reviewer["entity_name"],
						"link"=>$link,
						"link2"=>$link2
					)
				);
				if (strpos(__DIR__,'/var/www/html') !== false) {
					$util->queueEmail($emailData);
				}
			}
		}
	}
	catch(PDOException $e) {
		echo 'Error: ' . $e->getMessage();
	}
	$message = 'Your review has been added.';
	unset($_SESSION['jsonData']);
	unset($_SESSION['review']);
} else {
	// echo '<p>Update an existing entry.</p>';
	try {
		$stmt = $conn->prepare('UPDATE `review` SET `review` = :review, `user_id` = :user_id, `entity_id` = :entity_id, `rating` = :rating, `review_title` = :review_title, `team_quality` = :team_quality, `info_quality` = :info_quality, `track_record` = :track_record WHERE `review_id` = :review_id');
		$stmt->bindParam(':review', $review, PDO::PARAM_STR);
		$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
		$stmt->bindParam(':entity_id', $entity_id, PDO::PARAM_INT);
		$stmt->bindParam(':rating', $rating, PDO::PARAM_INT);
		$stmt->bindParam(':review_title', $review_title, PDO::PARAM_STR);
		$stmt->bindParam(':review_id', $review_id, PDO::PARAM_INT);
		$stmt->bindParam(':team_quality', $team_quality, PDO::PARAM_INT);
		$stmt->bindParam(':info_quality', $info_quality, PDO::PARAM_INT);
		$stmt->bindParam(':track_record', $track_record, PDO::PARAM_INT);
		$stmt->execute();
		$stmt->debugDumpParams();
	}
	catch(PDOException $e) {
		echo 'Error: ' . $e->getMessage();
	}
	$message = 'Your review has been modified.';
	unset($_SESSION['jsonData']);
	unset($_SESSION['review']);
}
$conn = null;
header("Location: $http_referer?message=$message");
ob_end_flush();
