<?php
date_default_timezone_set('America/Lima');
session_start();
$error = '';
function domain_exists($email, $record = 'MX'){
	list($user, $domain) = explode('@', $email);
	return checkdnsrr($domain, $record);
}
$email = strtolower(filter_input(INPUT_POST,'email',FILTER_SANITIZE_EMAIL, FILTER_FLAG_EMAIL_UNICODE));
$update = strtolower(filter_input(INPUT_GET,'update',FILTER_SANITIZE_EMAIL, FILTER_FLAG_EMAIL_UNICODE));

$outputFile = '../logs/newsletterSubscription.log';
$log = date('Y-m-d H:i:s').','.$email.PHP_EOL;
file_put_contents($outputFile,$log,FILE_APPEND);
if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	$error = 'The email: <strong>'.$email.'</strong> is NOT a valid email address.';
} elseif (!domain_exists($email)) {
	$error = 'No MX record exists.<br>The email <strong>'.$email.'</strong> is NOT valid.';
}

// subscribe to mailchimp
if (empty($error)) {
	$authToken = "f2a8c6fa8730a275f4ee5ae6741df8c0-us16";
	$list_id = "d4322e8c1b";
	$dc = explode("-",$authToken)[1];
	$postData = array(
		"email_address" => "$email",
		"status" => "subscribed"
	);

	$ch = curl_init('https://'.$dc.'.api.mailchimp.com/3.0/lists/'.$list_id.'/members/');
	curl_setopt_array($ch, array(
		CURLOPT_POST => TRUE,
		CURLOPT_RETURNTRANSFER => TRUE,
		CURLOPT_HTTPHEADER => array(
			'Authorization: apikey '.$authToken,
			'Content-Type: application/json'
		),
		CURLOPT_POSTFIELDS => json_encode($postData)
	));
	$response = curl_exec($ch);
	$responseCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	$responseJson = json_decode($response);
	curl_close($ch);


	if ($responseCode == 400) {
		$msg = $responseJson->title;
		if ($responseJson->title == "Member Exists") {
			$msg = " Your email is already subscribed to our newsletter.";
		}
		if ($responseJson->title == "Invalid Resource") {
			$error = 'The email: <strong>'.$email.'</strong> is NOT a valid email address.';
		}
	} else if ($responseCode == 200) {
		$msg = " has been successfully subscribed to our newsletter.";

		require_once "../class/util.php";
		$util = Util::getInstance();

		$emailData = array(
			"email"=>$email,
			"template"=>"newsletter-confirm.html",
			"subject"=>"Newsletter Confirmation"
		);
		$util->queueEmail($emailData);
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php'); ?>
</head>
<body>
	<?php include('../inc/nav.php'); ?>
	<main class="container pt-5 pb-5">
		<?php
		if (strlen($error)>1 && empty($update)) {
			echo '<div class="alert alert-danger" role="alert"><h3>Error!</h3><p>'.$error.'</p><p>Please try again.</p></div>';
		}
		if ($update == "success") {
			echo '<div class="alert alert-success" role="alert"><p>Your information is successfully updated.</p></div>';
		}
		?>
		<h4>Thank you</h4>
		<p><?php
			if ($responseCode == 400 && empty($error)) {
				echo "$msg";
			} else if (empty($error)) {
				echo "Your email: <strong>$email</strong> has been subscribed to our newsletter.";
			}
		?></p>
		<hr>
		<form action="updateSubscription.php" method="POST" role="form">
			<input type="hidden" name="email" id="email" value="<?php echo $email ?>">
			<p>Before you go, (optional) You can personalize your newsletter by adding further information about yourself below.</p>
			<div class="form-group">
				<label class="form-control-label" for="FNAME">First Name</label>
				<input type="text" name="FNAME" id="FNAME" class="form-control">
			</div>
			<div class="form-group">
				<label class="form-control-label" for="LNAME">Last Name</label>
				<input type="text" name="LNAME" id="LNAME" class="form-control">
			</div>
			<p><button type="submit" class="btn btn-primary">Finish my Subscription</button></p>
		</form>
	</main>
	<?php include('../inc/telegram.php'); ?>
	<?php include('../inc/footer.php'); ?>
	<?php include('../inc/endScripts.php'); ?>
</body>
</html>
