<?php
session_start();
date_default_timezone_set('America/Lima');
$token =  md5('cryptocanary'.date('Ymd'));
if (isset($_SESSION['referer'])) {
	$referer = $_SESSION['referer'];
} else {
	$referer = '';
}

if (!isset($_SESSION["http_referer"]) && isset($_SERVER['HTTP_REFERER'])) {
	$_SESSION["http_referer"] = $_SERVER['HTTP_REFERER'];
}

// social login
require_once ('../login/config.php');

// SEO purpose
$title = 'Join Our Flock | CryptoCanary';
$description = 'Create your account on CryptoCanary and join the discussion with a community of thousands of crypto enthusiasts!';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php'); ?>
</head>
<body>
	<div id="Map">
		<?php include('../inc/nav2.php'); ?>
		<div class="container pt-5 pb-5">
			<div class="row">
				<div class="col-md-6">
					<div class="whiteBG" style="padding: 30px; border-radius: 15px;">
						<h1 class="text-center">Join Our Flock</h1>
						<p class="text-center text-muted">Discover new projects and join the discussion with a community of thousands of crypto enthusiasts!</p>
						<div id="result"></div>
						<form ic-post-to="signup.php" ic-target="#result" ic-indicator="#spinner" method="POST" role="form">
							<div class="form-group">
								<input type="email" name="email" id="email" class="form-control form-control-lg" required="required" minlength="5" placeholder="Your Email" autofocus="autofocus">
								<input type="hidden" name="token" value="<?php echo $token; ?>">
								<input type="hidden" name="referer" value="<?php echo $referer; ?>">
							</div>
							<button type="submit" class="btn btn-block btn-lg btn-primary">Create Your Account</button> <i id="spinner" class="fa fa-spinner fa-spin" style="display:none"></i>
						</form>
						<p class="text-center text-muted pt-3">OR</p>
						<div class="row">
							<div class="col-12 ssk-block">
								<p><a href="<?php echo $fbAuthUrl ?>" class="btn btn-block btn-lg btn-outline-primary"><i class="fab fa-facebook-square"></i> Login with Facebook</a></p>
							</div>
							<div class="col-12 ssk-block">
								<p><a href="<?php echo $googleAuthUrl ?>" class="btn btn-block btn-lg btn-outline-primary"><i class="fab fa-google"></i> Login with Google</a></p>
							</div>
						</div>
					</div>
					<p class="pt-3 pb-3 text-center">Already have an account? <a href="/login/" class="text-dark"><u>Login to your account</u></a></p>
				</div>
				<div class="col-md-6 d-none d-md-block" style="padding-top: 20px;">
					<p class="text-center"><img src="/img/login_illus.svg" class="img-fluid" alt="Image"></p>
				</div>
			</div>
		</div>
	</div>
	<?php include('../inc/footer.php'); ?>
	<?php include('../inc/endScripts.php'); ?>
</body>
</html>
