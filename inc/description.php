<hr>
<div id="description">
	<div class="container text-muted text-justify">
		<h2 class="text-center">The canary in the crypto coal mine</h2>
		<p class="text-center"><img src="/img/logo.svg" class="img-fluid" alt="Image" style="max-width: 300px;"></p>
		<p>One giant roadblock preventing cryptocurrency mass adoption is the sheer number of scams and shady projects in the space. CryptoCanary serves as the "canary in the coal mine" to help you identify projects or coins that might be shady or potentially scams.</p>
		<p id="seeMore" class="text-center"><a href="#more" onClick="$('#more').show();$('#seeMore').hide();">See More</a></p>
		<div id="more" style="display:none">
			<p>In the old days, coal miners took a canary in a cage with them underground. As long as the canary was alive, the air in the mine was not toxic. If however, the canary died, the miners knew they had to get out of there immediately. Similarly, CryptoCanary aims to expose cryptocurrency projects that may be shady so that you as an investor can be alerted before it's too late. Members of CryptoCanary help each other out by reviewing various cryptocurrency projects on their shadiness/legitimacy. By considering the community's collective opinions, you can make a more informed decision on whether a project or coin that you are interested in may require further research and caution. If you were considering investing, this might save you a lot of money by helping you catch potential scams before they affect you.</p>
			<p>People in the cryptocurrency world have lost a lot of money by falling for notorious scams such as Bitconnect or Plexcoin, as well as smaller exit scams such as Confido. If they just had easy access to quality reviews about those projects, then they could have saved a lot of money. Reduce your risk of falling for cryptocurrency scams by signing up for CryptoCanary!</p>
			<p>If you are visiting our website cryptocanary.app, you will find many cryptocurrency projects and their respective ratings/reviews. If you wish to help create a better crypto world, you can sign up for a free account and then start rating/reviewing projects. As a member, you can also request a review from the community by submitting a project if it isn't already on our platform. You do this by clicking "Add Project". After you submit the new project, you can already add your own review if you have an opinion to share.</p>
			<p>We would like to encourage our members to follow these basic guidelines:</p>
			<ul>
				<li>Only post honest reviews based on personal experience or fact-based information</li>
				<li>Include a reasonable explanation and relevant context in your review so that others can form their opinion on how you decided on your particular rating</li>
				<li>Help improve the quality of reviews and ratings by upvoting (Helpful) or downvoting (Suspicious) reviews in a fair manner</li>
				<li>Report any abuse, spam, or attempts of cheat the system</li>
				<li>When submitting new projects, please provide as much factual information about the project as possible in a neutral way</li>
				<li>Before submitting a new project, please check first if it already exists in our database</li>
			</ul>
			<p>If we as members of CryptoCanary actively add cryptocurrency projects and review them, we can collectively increase the knowledge foundation of the entire space. Hopefully, this will lead to better awareness of suspicious projects so that less people will get scammed, lose their money, and become jaded with the cryptocurrency world forever. This should benefit the whole cryptocurrency industry by creating more investors who are better equipped to push out the malicious projects that give the whole industry a bad name. Ultimately, more trust in the cryptocurrency ecosystem should lead to more adoption. We can all contribute to this process by making the cryptocurrency world safer and healthier! And with cryptocanary.app, we aim to play an integral role in that process!</p>
			<p>So if you haven't already, please sign up and start reviewing! Good luck and enjoy the benefits of using CryptoCanary as part of your cryptocurrency investor's toolbox!</p>
			<p class="text-center"><a href="#seeMore" onClick="$('#more').hide();$('#seeMore').show();">See Less</a></p>
		</div>
	</div>
</div>
