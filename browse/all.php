<?php
session_start();
$id = (int)filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT);
if (!isset($id) OR $id<0) {
	$id = 0;
}
$limit = 100*$id;
$limit100 = $limit+100;
require('../db.php');
try {
	$stmt2 = $conn->prepare("SELECT COUNT(`entity_name`) FROM `entity` WHERE approved_flag = 1 AND `entity_category_id` = 1");
	$stmt2->execute();
	$stmt2->setFetchMode(PDO::FETCH_NUM);
	$data2 = $stmt2->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$total = $data2[0][0];
try {
	$stmt = $conn->prepare("SELECT entity_name, entity_id, image, symbol, create_time, latest_review_time, industry, overall_rating, review_count FROM entity WHERE approved_flag = 1 AND `entity_category_id` = 1 ORDER BY entity_name LIMIT $limit,100");
	$stmt->execute();
	$count = $stmt->rowCount();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$data = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$conn = null;
$title = 'Browse '.$limit.' to '.$limit100.' of a total of '.number_format($total).' Coins reviews | Cryptocanary';
$description = 'Browse list of all cryptocurrency ratings and reviews. Categories include coins, tokens, ICOs, exchanges, wallets, influencers, tools, projects, and more!';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php'); ?>
</head>
<body>
	<?php include('../inc/nav.php'); ?>
	<div class="greyBG">
		<div class="container">
			<?php
			if (!empty($_SESSION['name'])) {
				echo '<h2>Hello '.$_SESSION['name'].'</h2><hr>';
			}
			include_once "../inc/notification.php";
			?>
			<h1 class="text-center pt-5 pb-5">Browse all Coins</h1>
		</div>
	</div>
	<main class="container pt-3 pb-3">
		<p class="text-right">Show: <a href="/browse/all.php">All Coins</a> / <a href="/browse/">Coins with Reviews</a></p>
		<p><?php echo $limit.' to '.$limit100.' of a total of '.number_format($total); ?> Coins on this page</p>
		<nav aria-label="Page navigation example">
			<ul class="pagination justify-content-end">
				<li class="page-item"><a class="page-link" href="all.php?type=<?php echo $typeID; ?>&id=<?php echo $id-1; ?>"><span aria-hidden="true">&laquo;</span> Previous</a></li>
				<li class="page-item"><a class="page-link" href="all.php?type=<?php echo $typeID; ?>&id=<?php echo $id+1; ?>">Next <span aria-hidden="true">&raquo;</span></a></li>
			</ul>
		</nav>
		<table id="table" class="table table-striped" style="width:100%">
			<thead><tr><th>#</th><th>Name</th><th>Symbol</th><th>Submitted</th><th>Confidence</th><th>Reviews</th></tr></thead>
			<tbody>
				<?php
				foreach ($data as $key => $row) {
					$rating = $row['overall_rating'];
					$confidence = "N/A";
					if($rating != null){
						$confidence = number_format((($rating)), 0);
						if ($confidence <= 25) {$pbcolor = 'alert-danger';}
						if ($confidence > 25 AND $confidence <=50) {$pbcolor = 'alert-warning';}
						if ($confidence > 50 AND $confidence <=75) {$pbcolor = 'alert-info';}
						if ($confidence > 75) {$pbcolor = 'alert-success';}
						$confidence.="%";
					} else {
						$pbcolor="";
					}

					$score = number_format((4-$rating)/4 * 100,0);
					echo '<tr><td style="width: 100px;"><a href="/review/'.urlencode($row['entity_name']).'"><img style="width: 50px;" class="img-fluid" src="/images/entities/'.$row['entity_id'].'/'.$row['image'].'" alt="'.$row['entity_name'].'"></a></td><td><a href="/review/'.urlencode($row['entity_name']).'">'.$row['entity_name'].'</a></td><td>'.$row['symbol'].'</td><td>'.date('Y-m-d',strtotime($row['latest_review_time'])).'</td><td class="'.$pbcolor.' text-right">'.$confidence.'</td><td class="text-right">'.$row['review_count'].'</td></tr>';
				}
				?>
			</tbody>
			<tfoot><tr><th>#</th><th>Name</th><th>Symbol</th><th>Submitted</th><th>Confidence</th><th>Reviews</th></tr></tfoot>
		</table>
		<nav aria-label="Page navigation example">
			<ul class="pagination justify-content-end">
				<li class="page-item"><a class="page-link" href="all.php?type=<?php echo $typeID; ?>&id=<?php echo $id-1; ?>"><span aria-hidden="true">&laquo;</span> Previous</a></li>
				<li class="page-item"><a class="page-link" href="all.php?type=<?php echo $typeID; ?>&id=<?php echo $id+1; ?>">Next <span aria-hidden="true">&raquo;</span></a></li>
			</ul>
		</nav>
	</main>
	<?php include('../inc/telegram.php') ?>
	<?php include('../inc/footer.php') ?>
	<?php include('../inc/endScripts.php') ?>
</body>
</html>
