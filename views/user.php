<?php
session_start();
require('db.php');
if (isset($name)) {
	try {
		$stmt3 = $conn->prepare('SELECT `user_id`,`username`,`avatar`,`bio`,`score`,`create_time`,`youtube`,`twitter`,`facebook`,`linkedin` FROM `user` WHERE `username` LIKE :username');
		$stmt3->bindParam(':username', $name, PDO::PARAM_STR);
		$stmt3->execute();
		$stmt3->setFetchMode(PDO::FETCH_NUM);
		$data3 = $stmt3->fetchAll();
	}
	catch(PDOException $e) {
		echo 'Error: ' . $e->getMessage();
	}
	$user_id = $data3[0][0];
	$username = $data3[0][1];
	$userAvatar = $data3[0][2];
	$bio = $data3[0][3];
	$score = $data3[0][4];
	$create_time = $data3[0][5];
	$youtube = $data3[0][6];
	$twitter = $data3[0][7];
	$facebook = $data3[0][8];
	$linkedin = $data3[0][9];
} else {
	try {
		$stmt2 = $conn->prepare('SELECT `username`,`avatar`,`bio`,`score`,`create_time`,`youtube`,`twitter`,`facebook`,`linkedin` FROM `user` WHERE `user_id` = :user_id');
		$stmt2->bindParam(':user_id', $user_id, PDO::PARAM_INT);
		$stmt2->execute();
		$stmt2->setFetchMode(PDO::FETCH_NUM);
		$data2 = $stmt2->fetchAll();
	}
	catch(PDOException $e) {
		echo 'Error: ' . $e->getMessage();
	}
	$username = $data2[0][0];
	$userAvatar = $data2[0][1];
	$bio = $data2[0][2];
	$score = $data2[0][3];
	$create_time = $data2[0][5];
	$youtube = $data2[0][6];
	$twitter = $data2[0][7];
	$facebook = $data2[0][8];
	$linkedin = $data2[0][9];
}
try {
	$stmt = $conn->prepare('SELECT review.*, entity_name, symbol, image, average_team_quality, average_track_record, average_info_quality FROM `review` INNER JOIN entity ON review.entity_id = entity.entity_id WHERE review.`user_id` = :user_id AND review.`delete_time` IS NULL ORDER BY review.`review_id` DESC');
	$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
	$stmt->execute();
	$count = $stmt->rowCount();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$data = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
try {
	$stmt = $conn->prepare('SELECT `product_review`.review_id, `product_review`.user_id, `product_review`.product_id, review, rating, upvote_tally, downvote_tally, `product_review`.create_time, `product_review`.update_time, category, product_name, overall_rating, review_count, website, image, logo, approved_flag FROM `product_review` INNER JOIN product ON product_review.product_id = product.product_id WHERE product_review.`user_id` = :user_id AND product_review.`delete_time` IS NULL ORDER BY product_review.`review_id` DESC');
	$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
	$stmt->execute();
	$count = $stmt->rowCount();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$discoveryReviews = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$conn = null;
if (strlen($bio)<1) {
	$bio = 'No biography available for this user...';
}
// clean the avatar picture
$randomAvatar = '/images/avatars/random/avatar'.rand(1,19).'.png';
if(strpos($userAvatar, 'random') !== false) {
	$userAvatar = $randomAvatar;
}
elseif(strlen($userAvatar) <1) { $userAvatar = $randomAvatar;}
else { $userAvatar = '/images/avatars/'.$user_id.'/'.$userAvatar;}
$youtubeLogo = $twitterLogo = $facebookLogo = $linkedinLogo = '';
if (strlen($youtube) >1) {
	if (strpos ($youtube,'http') === false){$youtube = 'https://'.$youtube;}
	$youtubeLogo = '<a href="'.$youtube.'"><i class="fab fa-youtube fa-2x"></i></a>';
}
if (strlen($twitter) >1) {
	if (strpos ($twitter,'http') === false){$twitter = 'https://'.$twitter;}
	$twitterLogo = '<a href="'.$twitter.'"><i class="fab fa-twitter fa-2x"></i></a>';
}
if (strlen($facebook) >1) {
	if (strpos ($facebook,'http') === false){$facebook = 'https://'.$facebook;}
	$facebookLogo = '<a href="'.$facebook.'"><i class="fab fa-facebook fa-2x"></i></a>';
}
if (strlen($linkedin) >1) {
	if (strpos ($linkedin,'http') === false){$linkedin = 'https://'.$linkedin;}
	$linkedinLogo = '<a href="'.$linkedin.'"><i class="fab fa-linkedin fa-2x"></i></a>';
}
// get all social media
// $socialMedia = $youtube.' '.$facebook.' '.$linkedin.' '.$twitter;
$title = $username.'\'s CryptoCanary profile and cryptocurrency reviews | CryptoCanary';
$description = 'View '.$username.'\'s CryptoCanary profile and previous reviews. See '.$username.'\'s opinion on various cryptocurrency projects. Canary Chirps: '.$score;
$img = 'https://cryptocanary.app'.$userAvatar;
// echo $img;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('inc/head.php'); ?>
</head>
<body>
	<?php include('inc/nav.php'); ?>
	<main class="container pt-3 pb-3">
		<?php include('inc/notification.php'); ?>
		<?php
		if (strlen($username) < 1) {
			echo '<div class="alert alert-danger pt-3 pb-3" role="alert"><h3>Error!</h3><p>No username: "<strong>'.$name.'</strong>" found.</p></div>';
		}
		?>
		<div class="row">
			<div class="col-md-2">
				<p><img src="<?php echo $userAvatar; ?>" class="img-fluid" alt="<?php echo $username ?>"></p>
			</div>
			<div class="col-md-10">
				<h1><?php echo $username; ?>&rsquo;s profile <small><span class="badge badge-secondary" style="font-size: 14px;vertical-align: top;"><?php echo $score; ?></span></small></h1>
				<p><i><?php echo $bio; ?></i></p></p>
				<p>Member since: <?php echo date('F j, Y', strtotime($create_time)).' '.$youtubeLogo.' '.$facebookLogo.' '.$linkedinLogo.' '.$twitterLogo; ?></p>
			</div>
		</div>
		<h2>Reviews:</h2>
		<hr>
		<div id="reviewText">
			<?php
			foreach ($data as $key => $row) {
				$logo = $row['entity_id'].'/'.$row['image'];
				echo '<div class="row">
				<div class="col-xl-1 col-md-2 d-none d-md-block">
				<p><a href="/review/'.urlencode(trim($row['entity_name'])).'"><img src="/images/entities/'.$logo.'" class="img-fluid" alt="'.$row['entity_name'].'"></a></p>
				</div>
				<div class="col-12 col-md-10 col-xl-11">
				<div class="d-md-none">
					<p><a href="/review/'.urlencode(trim($row['entity_name'])).'"><img src="/images/entities/'.$logo.'" class="img-fluid" alt="'.$row['entity_name'].'" style="height: 50px;"></a></p>
				</div>
				<h5 class="card-title"><a href="/review/'.urlencode(trim($row['entity_name'])).'">'.trim($row['entity_name']).' - '.$row['symbol'].'</a></h5>
				<div class="row">
				<div class="col-sm-4 text-center">
				Team Quality<br><input type="text" id="team_quality" name="team_quality" class="kv-rtl-theme-svg-star2 rating-loading" value="'.$row['team_quality'].'" data-size="xs">
				</div>
				<div class="col-sm-4 text-center">
				Info Quality<br><input type="text" id="info_quality" name="info_quality" class="kv-rtl-theme-svg-star2 rating-loading" value="'.$row['info_quality'].'" data-size="xs">
				</div>
				<div class="col-sm-4 text-center">
				Track record<br><input type="text" id="track_record" name="track_record" class="kv-rtl-theme-svg-star2 rating-loading" value="'.$row['track_record'].'" data-size="xs">
				</div>
				</div>
				<p>Review Date: '.date('F d, Y H:i:s', strtotime($row['update_time'])).'</p>
				<p>'.trim($row['review']).'</p>
				</div>
				</div>
				<hr>
				';
			}
			foreach ($discoveryReviews as $key => $row) {
				$logo = $row['product_id'].'/'.$row['image'];
				echo '<div class="row">
				<div class="col-xl-1 col-md-2 d-none d-md-block">
				<p><a href="/discover/'.urlencode(trim($row['product_name'])).'"><img src="/images/products/'.$logo.'" class="img-fluid" alt="'.$row['product_name'].'"></a></p>
				</div>
				<div class="col-12 col-md-10 col-xl-11">
				<div class="d-md-none">
					<p><a href="/discover/'.urlencode(trim($row['product_name'])).'"><img src="/images/products/'.$logo.'" class="img-fluid" alt="'.$row['product_name'].'" style="height: 50px;"></a></p>
				</div>
				<h5 class="card-title"><a href="/discover/'.urlencode(trim($row['product_name'])).'">'.trim($row['product_name']).'</a></h5>
				Rating:<br><input type="text" id="rating" name="rating" class="kv-rtl-theme-svg-star2 rating-loading" value="'.$row['rating'].'" data-size="xs">
				<p>Review Date: '.date('F d, Y H:i:s', strtotime($row['update_time'])).'</p>
				<p>'.trim($row['review']).'</p>
				</div>
				</div>
				<hr>
				';
			}
			?>
		</div>
	</main>
	<?php include('inc/telegram.php'); ?>
	<?php include('inc/footer.php'); ?>
	<?php include('inc/endScripts.php'); ?>
	<script  type="text/javascript">
		var myTextEl = document.getElementById('reviewText');
		myTextEl.innerHTML = Autolinker.link( myTextEl.innerHTML, {
			stripPrefix: false,
			newWindow: true
		});
	</script>
	<script type="text/javascript">
		$('.kv-rtl-theme-svg-star2').rating({
			hoverOnClear: false,
			theme: 'krajee-svg',
			showClear: false,
			disabled: true,
			readonly: true,
			showCaption: false
		});
	</script>
</body>
</html>
