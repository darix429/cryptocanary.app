<?php
session_start();
if (isset($_SESSION['username'])) {
    $username = $_SESSION['username'];
} else {
    header('Location: /login/');
}
date_default_timezone_set('America/Lima');
// SEO purpose
$title = 'Welcome to CryptoCanary | CryptoCanary';
$description = 'Would you like to familiarize yourself with the platform?';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include('../inc/head.php'); ?>
</head>
<body>
    <div id="Map">
        <?php include('../inc/nav2.php'); ?>
        <div class="container" style="padding-top: 20vh; padding-bottom: 20vh; min-height: 300px;">
            <div class="row">
                <div class="col-12 text-center">
                <h1>Welcome to CryptoCanary "<?php echo $username; ?>"</h1>
                <p class="lead">Would you like to familiarize yourself with the platform?</p>
                <p class="pt-3 pb-3"><a href="start.php" type="button" class="btn btn-lg btn-primary pr-4 pl-4">Yes I would</a> <a href="/profile/" type="button" class="btn btn-lg btn-outline-dark pr-4 pl-4">No thank you</a></p>
                </div>
            </div>
        </div>
    </div>
    <?php include('../inc/footer.php'); ?>
    <?php include('../inc/endScripts.php'); ?>
</body>
</html>

