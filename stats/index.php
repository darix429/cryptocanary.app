<?php
session_start();
if ($_SESSION['ccstatistics'] != 'LoggedIn') {
	header('Location: login.php');
}
// initialisation
$total = 0;
$d = $d1 = array();
$datasets = array();
$interval = 60;
$interval = (int)filter_input(INPUT_GET,'interval',FILTER_SANITIZE_NUMBER_INT);
$table = (int)filter_input(INPUT_GET,'table',FILTER_SANITIZE_NUMBER_INT);
switch ($table) {
	case '1':
	$table = 'user';
	break;
	case '2';
	$table = 'review';
	break;
	case '3';
	$table = 'vote';
	break;
	case '4';
	$table = 'entity';
	break;
	case '5';
	$table = 'product_review';
	break;
	default:
	$table = 'user';
	break;
}
if ($interval<1 OR $interval > 999) {
	$interval = 60;
}
for($i = 0; $i < $interval; $i++) {
	$d1[] = date("Y-m-d", strtotime('-'. $i .' days'));
}
$d = array_reverse($d1);
require('../db.php');
try {
	$stmt = $conn->prepare("SELECT
	                       DATE(`create_time`) AS dates,
	                       COUNT(*) AS records
	                       FROM `$table`
	                       WHERE `create_time` BETWEEN CURDATE() - INTERVAL $interval DAY AND CURDATE()
	                       GROUP BY dates
	                       ORDER BY dates DESC");
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$data = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$conn = null;
// var_dump($data);
foreach ($data as $key => $row) {
	$number1 = $row['records'];
	$total = $total + $number1;
}
// SEO purpose
$title = 'Statistics | CryptoCanary';
$description = 'Login to your CryptoCanary account and review projects...';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('head.php'); ?>
</head>
<body>
	<?php include('nav.php'); ?>
	<main class="container">
		<form action="#" method="GET" role="form">
			<legend>New Request</legend>
			<div class="row">
				<div class="col-md-3">
					<div class="form-group">
						<label class="form-control-label" for="interval">interval</label>
						<input type="number" name="interval" id="interval" class="form-control" value="<?php echo $interval; ?>" min="1" max="999">
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label class="form-control-label" for="table">Table</label>
						<select name="table" id="table" class="form-control">
							<option value="1"<?php if($table == 'user'){echo 'selected';} ?>>User Sign-up</option>
							<option value="2"<?php if($table == 'review'){echo 'selected';} ?>>Coins Review</option>
							<option value="5"<?php if($table == 'product_review'){echo 'selected';} ?>>Product Review</option>
							<option value="3"<?php if($table == 'vote'){echo 'selected';} ?>>Votes</option>
							<option value="4"<?php if($table == 'entity'){echo 'selected';} ?>>Coins submitted</option>
						</select>
					</div>
				</div>
				<div class="col-md-2 pt-4">
					<p><button type="submit" class="btn btn-primary">Submit</button></p>
				</div>
				<div class="col-md-2 pt-4">
					<p><a href="searches.php"># of searches per day</a></p>
				</div>
			</div>
		</form>
		<hr>
		<h1><?php echo ucfirst($table); ?>s / day (last <?php echo $interval; ?> days)</h1>
		<p>Total: <?php echo '<strong>'.number_format($total,0).'</strong> '.$table.'s for the last '.$interval.' days';?></p>
		<canvas id="myChart"></canvas>
		<hr>
		<table class="table table-striped table-bordered" id="tableData">
			<thead>
				<tr>
					<th>Date</th>
					<th><?php echo ucfirst($table); ?>s / day</th>
				</tr>
			</thead>
			<tbody>
				<?php
				foreach ($d as $key => $datestamp) {
					$number = 0;
					foreach ($data as $key => $row) {
						if ($datestamp == $row['dates']) {
							$number = $row['records'];
							if ($table == 'entity' AND $number > 48) {
								$number = 0;
							}
						}
					}
					array_push($datasets, $number);
					echo '<tr><td>'.$datestamp.'</td><td>'.$number.'</td></tr>'.PHP_EOL;
				}
				?>
			</tbody>
		</table>
		<hr>
		<h2 class="pb-3">Total: <?php echo $total.' '.ucfirst($table).'s for the last '.$interval.' days';?></h2>
	</main>
	<?php include('endScripts.php'); ?>
	<script type="text/javascript">
		var ctx = document.getElementById('myChart').getContext('2d');
		var myChart = new Chart(ctx, {
			type: 'line',
			data: {
				labels: [<?php foreach ($d as $key => $datestamp) {
					echo '\''.$datestamp.'\', ';
				} ?>],
				datasets: [{
					label: '<?php echo ucfirst($table); ?>s / day',
					borderColor: "#2999ff",
					backgroundColor: "rgba(41,153,255,0.5)",
					data: [<?php foreach ($datasets as $key => $dataset) {
						echo $dataset.', ';
					} ?>]
				}]
			},
			options: {
				scales: {
					yAxes: [{
						ticks: {
							min:0,
							max:<?php echo max($datasets)+1; ?>
						}
					}]
				}
			}
		});
	</script>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tableData').DataTable( {
				"pageLength": 50,
				"order": [[ 0, "desc" ]]
			});
		});
	</script>
</body>
</html>
