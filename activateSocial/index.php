<?php
date_default_timezone_set('America/Lima');
$token =  md5('cryptocanary'.date('Ymd'));
$id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_STRING);
$email = base64_decode($id);
// include ('../inc/session.php');
// SEO purpose
$title = 'Activate your account | CryptoCanary';
$description = 'Activate your account on CryptoCanary';
$auth_method = 'system';
if (isset($_GET['auth_method'])) {
    $auth_method = $_GET['auth_method'];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include('../inc/head.php'); ?>
</head>
<body>
    <?php include('../inc/nav.php'); ?>
    <main class="container pt-5 pb-5">
        <div class="text-center"><img src="/img/canary-white.png" style="width: 400px;" class="img-fluid" alt="CryptoCanary logo"></div>
        <?php
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            echo '<div class="alert alert-danger" role="alert"><h3>Error!</h3><p>You did not specify a valid email address in the activation link.</p></div>';
        } ?>
        <div id="result"></div>
        <form action="activate.php" method="POST">
            <legend>Create your account:</legend>
            <div class="form-group">
                <label class="form-control-label" for="email">Your Email Address:</label>
                <input type="email" name="email" id="email" class="form-control" required="required" minlength="5" value="<?php echo $email; ?>" readonly>
                <input type="hidden" name="token" value="<?php echo $token; ?>">
                <small id="emailHelp" class="form-text text-muted">We will use it to reset your password if needed.</small>
            </div>
            <div class="form-group">
                <label class="form-control-label" for="name">Your Name:</label>
                <input type="text" name="name" id="name" class="form-control" required="required" minlength="3">
                <small id="nameHelp" class="form-text text-muted">(minimum 3 characters)</small>
            </div>
            <div class="form-group">
                <label class="form-control-label" for="username">Pick a Username:</label>
                <input type="text" name="username" id="username" class="form-control" required="required" minlength="3">
                <small id="usernameHelp" class="form-text text-muted">The username is what will appear on the website. (minimum 3 characters)</small>
            </div>
            <input type="hidden" name="auth_method" value="<?php echo $auth_method ?>">

            <button type="submit" class="btn btn-primary"><i class="fas fa-sign-in-alt"></i> Activate your account</button> <i id="spinner" class="fa fa-spinner fa-spin" style="display:none"></i>
        </form>
        <p class="pt-3 pb-3">Already have an account? <a href="/login/">Login here</a></p>
    </main>
    <?php include('../inc/footer.php'); ?>
    <?php include('../inc/endScripts.php'); ?>
</body>
</html>

