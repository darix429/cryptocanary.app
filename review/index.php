<?php
$id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_NUMBER_INT);
$entity_name = filter_input(INPUT_GET,'name',FILTER_SANITIZE_STRING);
$symbol = filter_input(INPUT_GET,'symbol',FILTER_SANITIZE_STRING);
require('../db.php');
if (strlen($id)>0) {
	if ($id<29 AND $id>11) { $id = 29;}
	if ($id < 1) {
		header('Location: ../');
		exit();
	}
	try {
		$stmt = $conn->prepare('SELECT entity_name FROM `entity` WHERE `entity_id` = :id LIMIT 1');
		$stmt->bindParam(':id', $id, PDO::PARAM_INT);
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_NUM);
		$entity = $stmt->fetchAll();
	}
	catch(PDOException $e) {
		echo 'Error: ' . $e->getMessage();
	}
	$conn = null;
	$entity_name = urlencode(trim($entity[0][0]));
	header("Location: /review/$entity_name");
	exit();
} elseif (strlen($entity_name)>1) {
	$conn = null;
	$entity_name = urlencode(trim($entity_name));
	header("Location: /review/$entity_name");
	exit();
} elseif (strlen($symbol)>1) {
	try {
		$stmt = $conn->prepare('SELECT entity_name FROM `entity` WHERE `symbol` LIKE :symbol LIMIT 1');
		$stmt->bindParam(':symbol', $symbol, PDO::PARAM_STR);
		$stmt->execute();
		$stmt->setFetchMode(PDO::FETCH_NUM);
		$entity = $stmt->fetchAll();
	}
	catch(PDOException $e) {
		echo 'Error: ' . $e->getMessage();
	}
	$conn = null;
	$entity_name = urlencode(trim($entity[0][0]));
	header("Location: /review/$entity_name");
	exit();
}
$conn = null;
if (empty($entity)) {
	header('Location: empty.php');
	exit();
}
