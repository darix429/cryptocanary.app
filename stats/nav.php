<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
	<div class="container">
		<a class="navbar-brand" href="#">CryptoCanary Stats</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item"><a class="nav-link" href="index.php">Graphs</a></li>
				<li class="nav-item"><a class="nav-link" href="total.php">Total</a></li>
				<li class="nav-item"><a class="nav-link" href="advertise.php">Advertise</a></li>
				<li class="nav-item"><a class="nav-link" href="login.php">Logout</a></li>
			</ul>
		</div>
	</div>
</nav>
