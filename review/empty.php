<?php
session_start();
$search = filter_input(INPUT_GET,'search',FILTER_SANITIZE_STRING);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php'); ?>
	<style type="text/css">
	input[type='search'] {text-align: center;font-size: 1.25rem;line-height: 1.5;width: 100%;height: 48px;padding-left: 30px !important;border-radius: 20px !important;font-family: 'Inter', Helvetica, sans-serif !important;font-style: normal !important;}.easy-autocomplete {max-width: 100%;}
	</style>
</head>
<body>
	<?php include('../inc/nav.php'); ?>
	<div class="container pt-5 pb-5 text-center">
			<h2>Nothing was found for: "<strong><?php echo $search; ?></strong>"</h2>
			<p>If you think this project deserves to be on CryptoCanary, then <a href="/add/">submit it here</a>.</p>
			<p>Otherwise, please make a new search below.</p>
		<div class="row pt-3 pb-5">
			<div class="col-12 text-center">
				<h4 class="pl-4">Enter the name of a coin:</h4>
				<form action="/search.php" accept-charset="UTF-8" method="GET" role="FORM">
					<div class="form-group">
						<input type="search" id="coinList2" name="search" placeholder="Find A Great Project (e.g. “Bitcoin”)" spellcheck="false">
					</div>
				</form>
			</div>
		</div>
	</div>
	<?php include('../inc/footer.php'); ?>
	<?php include('../inc/endScripts.php'); ?>
	<script type="text/javascript">
		var options = {
			url: "/data/coins.json",
			getValue: "name",
			cssClasses: "coinList",
			adjustWidth: false,
			template: {
				type: "iconRight",
				fields: {
					iconSrc: "icon"
				}
			},
			list: {
				match: {
					enabled: true
				},
				maxNumberOfElements: 10,
				onChooseEvent: function() {
					var value = $("#coinList2").getSelectedItemData();
					var name = value['name'];
					console.log(name);
					window.location.href='/search.php?search='+name;
				}
			},
			theme: "square"
		};
		$("#coinList2").easyAutocomplete(options);
	</script>
</body>
</html>
