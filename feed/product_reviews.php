<?php
date_default_timezone_set('America/Lima');
$output = '<?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
xmlns:content="http://purl.org/rss/1.0/modules/content/"
xmlns:wfw="http://wellformedweb.org/CommentAPI/"
xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:atom="http://www.w3.org/2005/Atom"
xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
>'.PHP_EOL;
$output .='<channel>
<title>CryptoCanary Latest review</title>
<atom:link href="https://cryptocanary.app/feed/product_reviews.xml" rel="self" type="application/rss+xml" />
<link>https://cryptocanary.app/</link>
<lastBuildDate>'.date(DATE_RFC2822).'</lastBuildDate>
<language>en-US</language>
<description>CryptoCanary consolidates community reviews and research to help beginners avoid crypto scams. Submit, rate, and review crypto projects to get started!</description>'.PHP_EOL;
require ('/var/www/html/db.php');
try {
	$stmt = $conn->prepare('SELECT product_name, username, product_review.create_time, review, short_desc
	                       FROM product_review
	                       INNER JOIN `user`
	                       ON product_review.user_id = `user`.user_id
	                       INNER JOIN product
	                       ON product_review.product_id = product.product_id
	                       WHERE product_review.delete_time IS NULL
	                       ORDER BY product_review.product_id DESC LIMIT 50');
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$data = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$conn = null;
foreach ($data as $key => $row) {
	$review = strip_tags(trim($row['review']));
	$description = htmlspecialchars($review, ENT_NOQUOTES, "UTF-8");
	$output .='<item>'.PHP_EOL;
	$output .='<title>'.$row['product_name'].'  was reviewed by "'.$row['username'].'"</title>'.PHP_EOL;
	$output .='<link>https://cryptocanary.app/discover/'.urlencode(trim($row['product_name'])).'</link>'.PHP_EOL;
	$output .='<pubDate>'.date(DATE_RFC2822, strtotime($row['create_time'])).'</pubDate>'.PHP_EOL;
	$output .='<description>'.$description.'</description>'.PHP_EOL;
	$output .='<guid>https://cryptocanary.app/discover/'.urlencode(trim($row['product_name'])).'?id='.rand(0,1000).'</guid>'.PHP_EOL;
	$output .='</item>'.PHP_EOL;
}
$output .='</channel>'.PHP_EOL.'</rss>';
$filename = '/var/www/html/feed/product_reviews.xml';
file_put_contents($filename, $output);
