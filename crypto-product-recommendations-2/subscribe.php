<?php

$email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);
$token = filter_input(INPUT_POST, 'g-recaptcha-response', FILTER_SANITIZE_STRING);
$sitekey = "6Le0fsQUAAAAAFVNFLnbcfgna6sqQFPM1pe23nRi";
$secret = "6Le0fsQUAAAAAM39YsRn8VcVTnTeS-K-UAFFy82U";
$ip = $_SERVER['REMOTE_ADDR'];

if(!$token) {
    header("Location: /crypto-product-recommendations-2/?subscribed=false&msg=captcha error");
    exit();
}

$url = 'https://www.google.com/recaptcha/api/siteverify';
$request = array('secret' => $secret, 'response' => $token);


$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($ch);
curl_close($ch);
$recaptcha = json_decode($response,true);

var_dump($recaptcha);

echo "<br>SUCCESS " . $recaptcha['success'];

if($recaptcha['success']) {
    /* mailchimp
    7aeac40cc91fde1f272c1381820c0dd6-us16
    f2a8c6fa8730a275f4ee5ae6741df8c0-us16
    */
    $authToken = 'f2a8c6fa8730a275f4ee5ae6741df8c0-us16';
    $list_id = 'd4322e8c1b';
    $postData = array(
    "email_address" => "$email",
    "status" => "subscribed",
    "tags" => ["CryptoRecs-Newsletter"]
    );

    $dc = explode("-",$authToken)[1];
    $ch = curl_init('https://'.$dc.'.api.mailchimp.com/3.0/lists/'.$list_id.'/members/');
    curl_setopt_array($ch, array(
    CURLOPT_POST => TRUE,
    CURLOPT_RETURNTRANSFER => TRUE,
    CURLOPT_HTTPHEADER => array(
        'Authorization: apikey '.$authToken,
        'Content-Type: application/json'
    ),
    CURLOPT_POSTFIELDS => json_encode($postData)
    ));
    $mailchimp_response = curl_exec($ch);
    curl_close($ch);
    $mcr = json_decode($mailchimp_response);
    if ($mcr->title == 'Member Exists') {
        $msg = "Thank you! We'll send you an email when this is ready.";
        header("Location: /crypto-product-recommendations-2/?subscribed=true&msg=$msg");
    } else if($mcr->title == 'Invalid Resource') {
        $msg = "Looks like you entered an invalid email address.";
        header("Location: /crypto-product-recommendations-2/?subscribed=false&msg=$msg");
    } else {
        $msg = "Thank you! We'll send you an email when this is ready.";
        header("Location: /crypto-product-recommendations-2/?subscribed=true&msg=$msg");
    }
} else {
    $msg = "Captcha failed!";
    header("Location: /crypto-product-recommendations-2/?subscribed=false&msg=$msg");
}

die();
