<?php
$brand = "cryptoCanary";
$meta_title = "Discover New Crypto Products";
$meta_url = "https://cryptocanary.app/crypto-product-recommendations-2";
$meta_img = "https://cryptocanary.app/img/recommendation.jpg";
$meta_description = "Discover cool new products in the crypto world. Examples include: crypto debit cards, mining devices, hardware wallets, DeFi interest protocols, blockchain games, podcasts, and much more!";
$sitekey = "6Le0fsQUAAAAAFVNFLnbcfgna6sqQFPM1pe23nRi";
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title><?php echo $meta_title ?></title>
	<meta name="description" content="<?php echo $meta_description ?>">
	<meta name="robots" content="index, follow">
	<meta name="googlebot" content="index,follow">
	<meta name="author" content="<?php echo $brand; ?>">
	<meta name="subject" content="<?php echo $meta_title; ?>">
	<meta name="rating" content="General">
	<!-- Open Graph data -->
	<meta property="og:type" content="website">
	<meta property="og:title" content="<?php echo $meta_title; ?>">
	<meta property="og:url" content="<?php echo $meta_url; ?>">
	<meta property="og:image" content="<?php echo $meta_img; ?>">
	<meta property="og:description" content="<?php echo $meta_description; ?>">
	<!-- Twitter Card data -->
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@<?php echo $brand; ?>">
	<meta name="twitter:creator" content="@<?php echo $brand; ?>">
	<meta name="twitter:url" content="<?php echo $meta_url; ?>">
	<meta name="twitter:title" content="<?php echo $meta_title; ?>">
	<meta name="twitter:description" content="<?php echo $meta_description; ?>">
	<meta name="twitter:image" content="<?php echo $meta_img; ?>">
	<!-- Favicon -->
	<link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192" href="/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
	<link rel="manifest" href="/favicon/manifest.json">
	<link rel="canonical" href="<?php echo $meta_url; ?>">
	<link type="text/css" rel="stylesheet" href="style.css?v=<?php echo date('ymd'); ?>">
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MCPGV59');</script>
</head>
<body>
	<?php
	if(isset($_GET['subscribed'])) {
		if ($_GET['subscribed'] == 'true') {
			$msg = "Success!";
			if(!empty($_GET['msg']))
				$msg = $_GET['msg'];
			echo "<div class='alert-container'><a href='/crypto-product-recommendations-2/'><div class='alert'>$msg</div></a></div>";
		} else {
			$msg = "Failed!";
			if(!empty($_GET['msg']))
				$msg = $_GET['msg'];
			echo "<div class='alert-container'><a href='/crypto-product-recommendations-2/'><div class='alert-error'>$msg</div></a></div>";
		}
	}
	?>
	<div class="wrap">
		<div class="container">
			<a id="headerLogo" href="/" class=""><img src="/img/logo_hz-inverse.svg" alt="logo" width="250px"></a>
		</div>
		<div class="container">
			<div class="contents">
				<div class="flex-container">
					<div class="section texts">
						<h1>Discover the coolest new products in the crypto world.</h1>
						<div>
							<p>Examples include: crypto debit cards, mining devices, hardware wallets, DeFi interest protocols, blockchain games, podcasts, and much more!</p>
							<p style="padding-top: 10px;">Sign up for our weekly newsletter below!</p>
						</div>
						<div>
							<form id="subscribe-form-1" action='/crypto-product-recommendations-2/subscribe.php' method='post'>
								<div class="form">
									<input type="email" id="if_email_izxo7v2a6un" name='email' style="padding: 10px; border: none; border-radius: 3px; width: 220px; font-size: 14px; font-weight: 300;" placeholder="email@example.com">
									<button id="if_submit_izxo7v2a6un" class="subscribe-form-btn" style="color:#fff; padding: 10px; border: none; border-radius: 3px; background: #F1B14D; margin-left: 10px; font-weight: 500; font-size: 18px; font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Helvetica,Arial,sans-serif" type='submit'>Sign Me Up</button>
								</div>
							</form>
						</div>
					</div>
					<div style="width: 75px;" class="mob-hide">&nbsp;</div>
					<div class="section images">
						<br>
						<img src="undraw_newsletter_vovu.svg" alt="undraw_newsletter_vovu.svg">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer">
		<div class="container">
			Copyright &copy; <?php echo date('Y'); ?> Canary Reviews LLC. All Rights Reserved.
		</div>
	</div>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?render=<?php echo $sitekey ?>"></script>
	<script type="text/javascript">
		$("document").ready(function(){
			$('.subscribe-form-btn').on("click",function(e) {
				$(this).attr("disabled", true);
				e.preventDefault();
				var form = $(this).parents('form');
				grecaptcha.ready(function() {
					grecaptcha.execute('<?php echo $sitekey ?>', {action: 'subscribe'}).then(function(token) {
						form.prepend('<input type="hidden" name="g-recaptcha-response" value="' + token + '">');
						form.unbind('submit').submit();
					});
				});
			});
		});
	</script>
</body>
</html>
