<?php
session_start();
$user_id = $_SESSION['user_id'];
if ($user_id < 1) {
	header('Location: ../login/');
	exit();
}
$username = trim(filter_input(INPUT_POST,'username',FILTER_SANITIZE_STRING));
$first_name = trim(filter_input(INPUT_POST,'first_name',FILTER_SANITIZE_STRING));
$last_name = trim(filter_input(INPUT_POST,'last_name',FILTER_SANITIZE_STRING));
$email = strtolower(filter_input(INPUT_POST,'email',FILTER_SANITIZE_EMAIL, FILTER_FLAG_EMAIL_UNICODE));
$bio = filter_input(INPUT_POST,'bio',FILTER_SANITIZE_STRING);
$facebook = filter_input(INPUT_POST,'facebook',FILTER_SANITIZE_STRING);
$youtube = filter_input(INPUT_POST,'youtube',FILTER_SANITIZE_STRING);
$twitter = filter_input(INPUT_POST,'twitter',FILTER_SANITIZE_STRING);
$code = trim(filter_input(INPUT_POST,'code',FILTER_SANITIZE_STRING));
$codeVerif = trim(filter_input(INPUT_POST,'codeVerif',FILTER_SANITIZE_STRING));
if ($code != $codeVerif) {
	echo "<div class='alert alert-danger alert-dismissible fade show' role='alert' id='profileUpdateError'>
	<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
	<h3>Error!</h3><p>Your Code: $code was wrong, please try again.</p>
	</div>
	<script>$('#code').hide();$('#showUpdate').show();</script>";
	exit();
}
require('../db.php');
if (strlen($username)<3) {
	echo "<div class='alert alert-danger alert-dismissible fade show' role='alert' id='profileUpdateError'>
	<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>
	<h3>Error!</h3><p>Your Username: $username has less than 3 characters.</p>
	</div>
	<script>$('#code').hide();$('#showUpdate').show();</script>";
	exit();
}
try {
	$stmt = $conn->prepare('UPDATE `user` SET `username` = :username, `first_name` = :first_name, `last_name` = :last_name, `email` = :email, `bio` = :bio, `facebook` = :facebook, `youtube` = :youtube, `twitter` = :twitter WHERE `user_id` = :user_id');
	$stmt->bindParam(':username', $username, PDO::PARAM_STR);
	$stmt->bindParam(':first_name', $first_name, PDO::PARAM_STR);
	$stmt->bindParam(':last_name', $last_name, PDO::PARAM_STR);
	$stmt->bindParam(':first_name', $first_name, PDO::PARAM_STR);
	$stmt->bindParam(':email', $email, PDO::PARAM_STR);
	$stmt->bindParam(':bio', $bio, PDO::PARAM_STR);
	$stmt->bindParam(':facebook', $facebook, PDO::PARAM_STR);
	$stmt->bindParam(':youtube', $youtube, PDO::PARAM_STR);
	$stmt->bindParam(':twitter', $twitter, PDO::PARAM_STR);
	$stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
	$stmt->execute();
}
catch(PDOException $e) {
	echo 'Error: '.$e->getMessage();
}
$conn = null;
?>
<div class="alert alert-success alert-dismissible fade show" role="alert"  id="profileUpdateSuccess">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<h3>Congratulation!</h3><p>Your Profile has been updated</p>
</div>
<script>$('#code').hide();$('#showUpdate').show();$(document).ready(function(){$(window).scrollTop(0);});</script>
