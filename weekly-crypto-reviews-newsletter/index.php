<?php

	$meta_title = "A curated list of crypto reviews";
	$meta_url = "http://weeklycryptoreviews.com/";
	$meta_img = "./WEEKLY COIN REVIEWS.jpg";
	$meta_description = "Get a weekly email roll-up of the best crypto reviews. Courtesy of CryptoCanary!";

	$sitekey = "6Lfk7rEUAAAAAPmvpXebAgauJtlhuwAfLuchyf0x";
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title><?php echo $meta_title ?></title>
	<meta name="description" content="<?php echo $meta_description ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	
	<!-- Open Graph data -->
	<meta property="og:type" content="article">
	<meta property="og:title" content="<?php echo $meta_title; ?>">
	<meta property="og:url" content="<?php echo $meta_url; ?>">
	<meta property="og:image" content="<?php echo $meta_img; ?>">
	<meta property="og:description" content="<?php echo $meta_description; ?>">

	<!-- Favicon -->
	<link rel="apple-touch-icon" sizes="57x57" href="/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192" href="/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png">
	<link rel="manifest" href="/favicon/manifest.json">


	<link rel="canonical" href="<?php echo $meta_url ?>">
	<link rel="stylesheet" href="./www.weeklycryptoreviews.com/main.a0bb03b4.css">
	<style tyle="text/css">body{font-family:'Ubuntu',sans-serif;font-size:14px;font-weight:400;--color-dark:#364236;--color-dark-secondary:#5c725d;--color-dark-link:#56a2ff;--wrapper-width:500px}.section:not(.dark),.section:not(.dark) .color-text{color:#eaefea;--color-secondary:#eaefea}.section.dark,.section.dark .color-text{color:#5c725d;--color-secondary:#5c725d}.section:not(.dark) .color{color:#fff}.section.dark .color{color:#364236}.section:not(.dark){--color:#fff;--color-link:#f8faf8}.section.dark{--color:#364236;--color-link:#56a2ff}.weight-text{font-weight:400}.weight-text-m{font-weight:500}.weight-text-h{font-weight:700}.weight-title{font-weight:400}.weight-title-m{font-weight:500}.weight-title-h{font-weight:700}.font-title{font-family:'Ubuntu',sans-serif}a:not(.btn),.link{color:#f8faf8;--link-hover-bg:rgba(248,250,248,0.05);--link-hover:#bccfbc}.dark a:not(.btn),.dark .link{color:#56a2ff;--link-hover-bg:rgba(86,162,255,0.05);--link-hover:#006bef}a,.link{text-decoration:none;font-weight:500}a:not(.btn):hover{text-decoration:underline}.wr{max-width:500px;max-width:var(--wr-max)}.alert-container{width:100vw;font-weight:bolder;margin:0 auto;position:fixed;top:10px;z-index:999}.alert{border:solid 1px #888;background:#bfb;width:300px;padding:10px 20px;margin:0 auto;cursor:pointer;color:#555}.alert-error{border:solid 1px #888;background:#fbb;width:300px;padding:10px 20px;margin:0 auto;cursor:pointer;color:#555}.alert-container a{text-decoration:none!important}.subscribe-form-btn{transition:none}.subscribe-form-btn:disabled,.subscribe-form-btn:hover{background:#555}</style>
	<link href="./www.weeklycryptoreviews.com/css" rel="stylesheet">
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-107267990-5"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-107267990-5');
	</script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://www.google.com/recaptcha/api.js?render=<?php echo $sitekey ?>"></script>

	<style>
		body {
			font-size: 14px;
		}
		.texts {
			color: #fff;
			width: 70vw;
			text-align: center;
			margin: 0 auto;
		}
		h1 {
			font-size: 3.5em;
			line-height: 60px;
		}
		h2 {
			font-size: 1.7em;
			line-height: 40px;
			font-weight: 300;
			opacity: 0.8;
		}

		@media all and (max-width: 480px) {
			.texts {
				width: 90vw;
			}
			h1 {
				font-size: 2em;
				line-height: 1.8em;
			}
			h2 {
				font-size: 1.3;
				line-height: 1.2em;
				font-weight: 200;
			}
		}

	</style>
</head>
<body>
	<script>
		$("document").ready(function(){
			$('.subscribe-form-btn').on("click",function(e) {
				$(this).attr("disabled", true);
				e.preventDefault();
				var form = $(this).parents('form');
				grecaptcha.ready(function() {
					grecaptcha.execute('<?php echo $sitekey ?>', {action: 'subscribe'}).then(function(token) {
						form.prepend('<input type="hidden" name="g-recaptcha-response" value="' + token + '">');
						form.unbind('submit').submit();
					});
				});
			});


		});
	</script>
	<?php
	if(isset($_GET['subscribed'])) {
		if ($_GET['subscribed'] == 'true') {
			$msg = "Success!";
			if(!empty($_GET['msg']))
				$msg = $_GET['msg'];
			echo "<div class='alert-container'><a href='/weekly-crypto-reviews-newsletter/'><div class='alert'>$msg</div></a></div>";
		} else {
			$msg = "Failed!";
			if(!empty($_GET['msg']))
				$msg = $_GET['msg'];
			echo "<div class='alert-container'><a href='/weekly-crypto-reviews-newsletter/'><div class='alert-error'>$msg</div></a></div>";
		}
	}
	?>
	
	<div style="background-color:#2587ff; width: 100vw; min-height: 100vh; position: absolute; top: 0; left: 0;">
		<div style="text-align: center; margin: 35px auto;">
			<a id="headerLogo" href="/" class=""><img src="4bzzsran.png" width="65" style="" alt="canary-black-icon.png"></a>
		</div>
		<div class="texts">
			<h1>3-5 interesting CryptoCanary reviews delivered to your inbox every week!</h1>
			<h2>This newsletter is perfect for those of you who are cautiously curious around the crypto world but are NOT Bitcoin maximalists.</h2>
			<div style="margin: 35px auto 0 auto;">
				<form id="subscribe-form-1" action='/weekly-crypto-reviews-newsletter/subscribe.php' method='post'>
					<div class="form" style="margin: 0 auto; display: flex; justify-content: center;">
						<input type="email" id="if_email_izxo7v2a6un" name='email' style="padding: 10px; border: none; border-radius: 3px; width: 220px; font-size: 14px; font-weight: 300;" placeholder="email@example.com">
						<button id="if_submit_izxo7v2a6un" class="subscribe-form-btn" style="color:#364236; padding: 10px; border: none; border-radius: 3px; background: #fff; margin-left: 10px; font-weight: 500; font-size: 18px; font-family: -apple-system,BlinkMacSystemFont,Segoe UI,Helvetica,Arial,sans-serif" type='submit'>Sign Up</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>
