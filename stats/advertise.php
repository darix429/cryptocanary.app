<?php
session_start();
if ($_SESSION['ccstatistics'] != 'LoggedIn') {
	header('Location: login.php');
}
$file = '../advertise/logs.txt';
$lines = file($file);
$data = array_reverse($lines);
// SEO purpose
$title = 'Statistics | CryptoCanary';
$description = 'Login to your CryptoCanary account and review projects...';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('head.php'); ?>
</head>
<body>
	<?php include('nav.php'); ?>
	<main class="container-fluid">
		<h1>Content of the advertising submitted form</h1>
		<hr>
		<h2>Raw Data:</h2>
		<p>The raw data will show everything, every single option that the user has entered...</p>
		<?php
		foreach ($data as $key => $row) {
			$jsdata = json_decode($row,true);
			echo '<p>';
			foreach ($jsdata as $key => $value) {
				if ($key == 'time') {$value = date("l, F j, Y, h:m:s",$value);}
				if ($key == 'budget') {$value = number_format($value,2);}
				echo '<strong>'.ucfirst($key).'</strong>: '.$value.' / ';
			}
			echo '</p>';
		}
		?>
		<hr>
		<h2>Table View</h2>
		<table id="tableData" class="table table-striped">
			<thead>
				<tr><th>TimeStamp</th><th>email</th><th>Name</th><th>companyName</th><th>Budget</th></tr>
			</thead>
			<?php
			foreach(file($file) as $line) {
				$json = json_decode($line, true);
				echo '<tr><td>'.date("Y-m-d h:m:s",$json['time']).'</td><td>'.$json['email'].'</td><td>'.$json['name'].'</td><td>'.$json['companyName'].'</td><td>'.number_format($json['budget'],2).'</td></tr>';
			}
			?>
			<tfoot>
				<tr><th>TimeStamp</th><th>email</th><th>Name</th><th>companyName</th><th>Budget</th></tr>
			</tfoot>
		</table>
	</main>
	<?php include('endScripts.php'); ?>
	<script type="text/javascript">
		$(document).ready(function() {
			$('#tableData').DataTable( {
				"pageLength": 50,
				"order": [[ 0, "desc" ]]
			});
		});
	</script>
</body>
</html>
