<?php
if (strpos(__DIR__,'/Applications/MAMP/') !== false) {
	require('/Applications/MAMP/htdocs/cryptocanary/db.php');
} else{
	require('/var/www/html/db.php');
}
try {
	$stmtR = $conn->prepare('SELECT entity_id, entity_name, symbol, entity_desc, overall_rating, links, image FROM `entity` WHERE approved_flag = 1 ORDER BY RAND() LIMIT 3');
	$stmtR->execute();
	$stmtR->setFetchMode(PDO::FETCH_ASSOC);
	$randEntity = $stmtR->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$conn = null;
?>
<hr>
<div id="RandomProject">
<h4>3 Coins you might not know exist...</h4>
	<div class="row pb-3 pt-3">
		<?php
		foreach ($randEntity as $key => $row) {
			if (isset($entity[0]['links'])) {
				$links = json_decode($entity[0]['links']);
				$websiteURL =  $links-> website;
			}
			$entity_name_URL = '/review/'.urlencode(trim($row['entity_name']));
			echo '<div class="col-md-4">
			<div class="media">
			<div class="media-body">
			<table style="width:100%"><tr><td><a href="'.$entity_name_URL.'"><img src="/images/entities/'.$row['entity_id'].'/'.$row['image'].'" alt="'.$row['entity_name'].'" class="img-fluid" style="height: 25px;"></a></td><td><strong>'.$row['entity_name'].' ('.$row['symbol'].')</strong></td></tr></table>
			<p class="alert alert-secondary"><a href="'.$entity_name_URL.'">Read review...</a></p>
			</div>
			</div>
			</div>';
		}
		?>
	</div>
</div>
