<?php
if (isset($_SESSION['LoggedIn'])) {
	$sessionLoggedIN = 'LoggedIn';
} else {
	$sessionLoggedIN = '';
}
?>
<nav class="navbar navbar-expand-md navbar-light bg-transparent">
	<div class="container">
		<a href="/" class="navbar-brand"><img src="/img/logoMono.svg" class="img-fluid" alt="CryptoCanary logo" style="height:34px;"></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="navbar-collapse collapse" id="navbar">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item"><a href="https://blog.cryptocanary.app/" class="nav-link" style="color:#362e2c;">Blog</a></li>
				<?php if ($sessionLoggedIN == 'LoggedIn') {
					$avatar = '/images/avatars/'.$_SESSION['user_id'].'/'.$_SESSION['avatar'];
					$randomAvatar = '/images/avatars/random/avatar'.rand(1,19).'.png';
					if(strpos($_SESSION['avatar'], 'random') !== false OR strlen($_SESSION['avatar']) <1 ) { $avatar = $randomAvatar;}
					echo ' <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #362e2c;">
					<img src="'.$avatar.'" class="img-fluid rounded" alt="Avatar" style="height: 30px;"> '.$_SESSION['username'].'</a>';
					echo '<div class="dropdown-menu" aria-labelledby="navbarDropdown">';
					echo '<a class="dropdown-item" style="color: #362e2c;" href="/profile/">My Profile</a>';
					echo '<a class="dropdown-item" style="color: #362e2c;" href="/profile/#socialMedia">My Chirps <span class="badge badge-secondary">'.$_SESSION['chirps'].'</span></a>';
					echo '<div class="dropdown-divider"></div>';
					echo '<a class="dropdown-item" style="color: #362e2c;" href="/logout.php"><i class="fas fa-power-off"></i> Logout</a>';
					echo '</div>
					</li>';
				}  else {?>
					<li class="nav-item"><a href="/signup/" class="nav-link text-black" style="color:#362e2c;">Join</a></li>
					<li class="nav-item"><a href="/login/" class="nav-link btn btn-primary text-white">Login</a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
</nav>
