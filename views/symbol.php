<?php
session_start();
require('db.php');
try {
	$stmt = $conn->prepare('SELECT `entity_name` FROM `entity` WHERE `symbol` LIKE :symbol LIMIT 1');
	$stmt->bindParam(':symbol', $symbol, PDO::PARAM_STR);
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_NUM);
	$entity = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$conn = null;
$entity_name = urlencode(trim($entity[0]['0']));
header("Location: ../../review/$entity_name");
