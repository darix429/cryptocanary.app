<?php
include ('../inc/session.php');
date_default_timezone_set('America/Lima');
$token =  md5('cryptocanary'.date('Ymd'));
$error = filter_input(INPUT_GET,'error',FILTER_SANITIZE_STRING);

// SEO purpose
$title = 'Password Reset | CryptoCanary';
$description = 'Reset your forgotten password to your CryptoCanary account';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php'); ?>
</head>
<body>
	<?php include('../inc/nav.php'); ?>
	<main class="container pt-5 pb-5">
		<div class="text-center"><img src="/img/canary-white.png" style="width: 400px;" class="img-fluid" alt="CryptoCanary logo"></div>
		<?php
		if (strlen($error)>1) {
			echo '<div class="alert alert-danger" role="alert"><h3>Error!</h3><p>'.$error.'</br>Try Again!</p></div>';
		}
		?>
		<form action="passwordReset.php" method="POST" role="form">
			<legend>Password Reset</legend>
			<div class="form-group">
				<label class="form-control-label" for="email">Your Email Address:</label>
				<input type="email" name="email" id="email" class="form-control" required="required" minlength="5">
				<input type="hidden" name="token" value="<?php echo $token; ?>">
			</div>
			<button type="submit" class="btn btn-primary"><i class="fas fa-sign-in-alt"></i> Request a password reset</button> <i id="spinner" class="fa fa-spinner fa-spin" style="display:none"></i>
		</form>
		<p class="pt-3 pb-3">Don't have an account yet? <a href="/signup/">Create your account here</a></p>
		<hr>
		<p>By logging in, you are agreeing to our <a href="/terms/">Terms of Service</a> and <a href="/privacy/">Privacy Policy</a>.</p>
	</main>
	<?php include('../inc/footer.php'); ?>
	<?php include('../inc/endScripts.php'); ?>
</body>
</html>
