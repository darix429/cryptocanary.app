<?php
session_start();
require('../db.php');
try {
    $stmt = $conn->prepare('SELECT `user`.user_id, `user`.avatar, `user`.username, `user`.score, IFNULL((select count(review_id) * 2 + sum(upvote_tally) from review where review.user_id = `user`.user_id and review.create_time > now()-interval 30 day),0) + IFNULL((select count(entity_id)*5 from entity where entity.user_id = `user`.user_id and entity.create_time > now()-interval 30 day and approved_flag = 1),0) gained_score FROM `user` WHERE `user`.user_id != 1 ORDER BY gained_score DESC LIMIT 10');
    $stmt->execute();
    $users = $stmt->fetchAll();
}
catch(PDOException $e) {
    echo 'Error: ' . $e->getMessage();
}
$conn = null;
$title = 'Top 10 Users Leaderboard (Last 30 Days) | Cryptocanary';
$description = 'Top 10 Users Leaderboard (Last 30 Days)';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php'); ?>
</head>
<body>
	<?php include('../inc/nav.php'); ?>
	<div class="greyBG">
		<div class="container">
			<?php
			include_once ('../inc/notification.php');
			?>
			<h1 class="text-center pt-5 pb-5">Top 10 Users (Last 30 Days)</h1>
		</div>
	</div>
	<main class="container pt-3 pb-3">
		<table id="table" class="table table-striped">
			<thead>
                <tr class="text-center">
                    <th>Rank</th>
                    <th>Avatar</th>
                    <th>Username</th>
                    <th>Chirps Gained</th>
                    <th>Total Chirps</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $rank = 1;
                foreach($users as $user) {
                    $avatar = "/images/avatars/" . $user["user_id"] . "/" . $user["avatar"];
                    $avatarSplit = explode("-",$user["avatar"]);
                    if($avatarSplit[0] == "random") {
                        $avatar = "/images/avatars/random/avatar".$avatarSplit[2].".png";
                    }
                    echo '<tr class="text-center"><td>'.$rank.'</td><td><a href="/user/'.$user['username'].'"><img src="'.$avatar.'" style="height:50px" class="img-fluid" alt="avatar"></a></td><td><a href="/user/'.$user['username'].'">'.$user['username'].'</a></td><td>'.$user['gained_score'].'</td><td>'.$user['score'].'</td></tr>';
                    $rank+=1;
                }
                ?>
            </tbody>
        </table>
    </main>
    <?php include('../inc/footer.php') ?>
    <?php include('../inc/endScripts.php') ?>
    <?php
    if (strpos(__DIR__,'/var/www/html') !== false) {
        require ('/var/www/html/vendor/mobiledetect/mobiledetectlib/Mobile_Detect.php');
        $detect = new Mobile_Detect;
        if ( $detect->isMobile()) {
            echo '
    <script type="text/javascript">
        $(document).ready(function() {
            $("#table").DataTable( {
                "responsive": true,
                "pageLength": 10,
                "order": [[ 0, "asc" ]]
            });
        });
    </script>';
        }
    }
    ?>
</body>
</html>
