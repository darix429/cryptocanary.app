<?php
$title = 'RSS Feed | CryptoCanary';
$description = 'RSS Feed';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php'); ?>
	<style>
		body{background-color: #fff; color: #222;}
	</style>
</head>
<body>
	<?php include('../inc/nav.php'); ?>
	<main class="container pt-5 pb-5">
		<div class="text-center"><img src="/img/canary-white.png" style="width: 400px;" class="img-fluid" alt="CryptoCanary logo"></div>
		<h3 class="pt-3">CryptoCanary Product Discovery RSS feed</h3>
		<ul>
			<li><a href="/feed/newProduct.xml">Stay updated with all the latest submitted Crypto Product</a></li>
			<li><a href="/feed/product_reviews.xml">Stay updated with all the latest Crypto Product review</a></li>
		</ul>
		<h3>CryptoCanary App RSS feed</h3>
		<ul>
			<li><a href="/feed/cryptocurrency.xml">Stay updated with all the latest submitted Coins</a></li>
			<li><a href="/feed/reviews.xml">Stay updated with all the latest review</a></li>
		</ul>
		<h3>CryptoCanary Blog RSS feed</h3>
		<ul>
			<li><a href="https://blog.cryptocanary.app/feed/">Our CryptoCanary blog entries feed</a></li>
			<li><a href="https://blog.cryptocanary.app/comments/feed/">Our CryptoCanary blog comment feed</a></li>
		</ul>
	</main>
	<?php include('../inc/telegram.php'); ?>
	<?php include('../inc/footer.php'); ?>
	<?php include('../inc/endScripts.php'); ?>
</body>
</html>
