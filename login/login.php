<?php
ob_start();
date_default_timezone_set('America/Lima');
$tokenVerif = md5('cryptocanary'.date('Ymd'));
$email = strtolower(filter_input(INPUT_POST,'email',FILTER_SANITIZE_EMAIL, FILTER_FLAG_EMAIL_UNICODE));
$token = filter_input(INPUT_POST,'token',FILTER_SANITIZE_STRING);
$userPassword = filter_input(INPUT_POST,'password',FILTER_SANITIZE_STRING);
$referer = filter_input(INPUT_POST,'referer',FILTER_SANITIZE_URL);
if ($token != $tokenVerif) {
	header('Location: index.php?error=Something went wrong');
	exit();
}
// check if the referer URL is a valid URL and if it is coming from cryptocanary
if (strpos($referer, 'cryptocanary') === false OR !filter_var($referer, FILTER_VALIDATE_URL)) {
	$referer = '../browse/';
}
require ('../db.php');
try {
	$stmt = $conn->prepare('SELECT user_id, email, password, username, avatar, score FROM `user` WHERE `email` = :email LIMIT 1');
	$stmt->bindParam(':email', $email, PDO::PARAM_STR);
	$stmt->execute();
	$count = $stmt->rowCount();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$data = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$conn = null;
echo $count;
if ($count < 1) {
	header('Location: index.php?error=1&email='.$email);
	exit();
}
$user_id = $data[0]['user_id'];
$username = $data[0]['username'];
$storedHash = $data[0]['password'];
if (password_verify($userPassword, $storedHash)) {
	// Start the session
	session_start();
	$_SESSION['LoggedIn'] = 'LoggedIn';
	$_SESSION['user_id'] = $data[0]['user_id'];
	$_SESSION['username'] = $data[0]['username'];
	$_SESSION['avatar'] =  $data[0]['avatar'];
	$_SESSION['chirps'] = $data[0]['score'];
	$_SESSION['userEmail'] = $data[0]['email'];
	// log the sucessful login
	$outputFile = '../logs/loginglogs.txt';
	$log = date('Y-m-d H:i:s').', '.$user_id.', '.$username.PHP_EOL;
	file_put_contents($outputFile,$log,FILE_APPEND);
	$outputFile2 = 'authentication.log';
	$log2 = time().', '.$user_id.', web'.PHP_EOL;
	file_put_contents($outputFile2,$log2,FILE_APPEND);
	header("Location: $referer");
	exit();
} else {
	header('Location: index.php?error=2&email='.$email);
	exit();
}
ob_end_flush();
