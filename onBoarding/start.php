<?php
session_start();
if (isset($_SESSION['username'])) {
	$username = $_SESSION['username'];
} else {
	header('Location: /login/');
}
date_default_timezone_set('America/Lima');
// SEO purpose
$title = 'Welcome to CryptoCanary | CryptoCanary';
$description = 'Would you like to familiarize yourself with the platform?';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php'); ?>
	<style>
		.carousel-control-prev-icon {
			background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23000' viewBox='0 0 8 8'%3E%3Cpath d='M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z'/%3E%3C/svg%3E") !important;
		}
		.carousel-control-next-icon {
			background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='%23000' viewBox='0 0 8 8'%3E%3Cpath d='M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z'/%3E%3C/svg%3E") !important;
		}
	</style>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light sticky-top" style="background-color: #f6f6f5;">
	<div class="container">
		<a class="navbar-brand" href="/"><img src="/img/logo.svg" class="img-fluid" alt="CryptoCanary logo" style="height: 34px;"></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<ul class="navbar-nav ml-auto">
				<li class="nav-item"><a class="nav-link" style="color: #362e2c;" href="/profile/">Exit</a></li>
				<li class="nav-item"><a class="nav-link btn btn-primary text-white" href="/onBoarding/">Restart</a></li>
			</ul>
		</div>
	</div>
</nav>
	<div class="container">
		<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
			</ol>
			<div class="carousel-inner" role="listbox">
				<div class="carousel-item active">
					<a href="index.php"><img class="img-fluid mx-auto d-block" src="3.png" data-src="3.png" alt="1 slide"></a>
				</div>
				<div class="carousel-item">
					<a href="index.php"><img class="img-fluid mx-auto d-block" src="4.png" data-src="4.png" alt="2 slide"></a>
				</div>
				<div class="carousel-item">
					<a href="index.php"><img class="img-fluid mx-auto d-block" src="5.png" data-src="5.png" alt="3 slide"></a>
				</div>
			</div>
			<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>
	<?php include('../inc/endScripts.php'); ?>
</body>
</html>
