<?php
date_default_timezone_set('America/Lima');
$token = filter_input(INPUT_SERVER,'HTTP_TOKEN',FILTER_SANITIZE_STRING);
if ($token != md5('cryptocanary'.date('Ymd'))) {
	exit('Authentication error.');
}
