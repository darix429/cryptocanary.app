<?php
$q =$_GET['q'];
$explode = explode('/', $q);
$product_name = urldecode($explode['2']);
require_once('db.php');
// Get product information
try {
	$stmt = $conn->prepare('SELECT `product_id` FROM product WHERE product_name LIKE :product_name LIMIT 1');
	// Bind Parameter
	$stmt->bindParam(':product_name', $product_name, PDO::PARAM_STR);
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_NUM); // set the resulting to returns associative array
	$productID = $stmt->fetchAll(); // store the result in a array that can be used after
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$product_id = (int)$productID['0']['0'];
if ($product_id > 1) {
	require_once('discover/review.php');
} else {
	require_once('discover/index.php');
}

