<?php
session_start();
date_default_timezone_set('America/Lima');
$token =  md5('cryptocanary'.uniqid());
$_SESSION['tokenSubmit'] = $token;
$projectType = $cryptosymbol = $cryptoname = $cryptoWebsite = $shortTagline = $ecosystem = '';
if (isset($_SESSION['addProject'])) {
	$addProjectJson = json_decode($_SESSION['addProject'], true);
	// var_dump($addProjectJson);
	$category = $addProjectJson['category'];
	$cryptosymbol = $addProjectJson['cryptosymbol'];
	$cryptoname = $addProjectJson['cryptoname'];
	$cryptoWebsite = $addProjectJson['cryptoWebsite'];
	$shortTagline = $addProjectJson['shortTagline'];
	$ecosystem = $addProjectJson['ecosystem'];
}
echo $category;
$categories = array('');
$success = filter_input(INPUT_GET,'success',FILTER_SANITIZE_NUMBER_INT);
$user_id = 0;
if (isset($_SESSION['user_id'])) { $user_id = $_SESSION['user_id'];}
$error = filter_input(INPUT_GET,'error',FILTER_SANITIZE_STRING);
$success = filter_input(INPUT_GET,'success',FILTER_SANITIZE_STRING);
$title = 'Submit Cryptocurrency Project For Community Review | CryptoCanary';
$description = 'Submit your cryptocurrency or blockchain project that you would like to hear community reviews/opinions about. Anyone can rate/review projects on CryptoCanary.';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php'); ?>
</head>
<body>
	<?php include('../inc/nav.php'); ?>
	<div class="container pt-3 pb-3">
		<?php
		if (strlen($error)>1) {
			echo '<div class="alert alert-danger alert-dismissible fade show" role="alert" id="addError"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h3>Error!</h3><p>'.$error.'<br>Please Try Again!</p></div>';
		}
		if ($success==1) {
			echo '<div class="alert alert-success alert-dismissible fade show" role="alert" id="addSuccess"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h3>Thank you!</h3><p>Your project will be reviewed by an admin and we will email you when it gets approved.</p></div>';
		}
		?>
		<form action="/add/add.php" method="POST" role="form">
			<div class="row" id="ProjectDetails">
				<div class="col-md-6">
					<h1>Submit A New Entry</h1>
					<hr>
					<h4>3 simple steps:</h4>
					<ol style="padding-inline-start: 1.1rem;">
						<li style="margin-bottom: 1rem;">Fill out the following form.</li>
						<li style="margin-bottom: 1rem;">You will get an email when your entry has been approved.</li>
						<li style="margin-bottom: 1rem;">Write a review and ask your friends to as well.<br>More reviews = more exposure.</li>
					</ol>
				</div>
				<div class="col-md-6 whiteBG" style="padding: 20px; border-radius: 5px;">
					<div class="form-group">
						<label class="form-control-label" for="category">Category:</label>
						<select name="category" id="category" class="form-control">
							<option value="1" id="coin" <?php if ($category == 1) {echo 'selected="selected"';} ?>>Coin/Token</option>
							<option value="10" <?php if ($category == 10) {echo 'selected="selected"';} ?>>DApps</option>
							<option value="11" <?php if ($category == 11) {echo 'selected="selected"';} ?>>Blockchain Games</option>
							<option value="12" <?php if ($category == 12) {echo 'selected="selected"';} ?>>DeFi</option>
							<option value="7" <?php if ($category == 7) {echo 'selected="selected"';} ?>>Exchanges</option>
							<option value="13" <?php if ($category == 13) {echo 'selected="selected"';} ?>>Media</option>
							<option value="14" <?php if ($category == 14) {echo 'selected="selected"';} ?>>Mining/Staking</option>
							<option value="15" <?php if ($category == 15) {echo 'selected="selected"';} ?>>Payments</option>
							<option value="16" <?php if ($category == 16) {echo 'selected="selected"';} ?>>Tools/Utilities</option>
							<option value="8" <?php if ($category == 8) {echo 'selected="selected"';} ?>>Wallets</option>
							<option value="18" <?php if ($category == 18) {echo 'selected="selected"';} ?>>Education</option>
							<option value="17" <?php if ($category == 17) {echo 'selected="selected"';} ?>>Others</option>
						</select>
					</div>
					<div class="form-group">
						<label class="form-control-label" for="cryptoname">Entry Name</label>
						<input type="text" name="cryptoname" id="cryptoname" class="form-control" required="required" minlength="3" value="<?php echo $cryptoname; ?>">
						<input type="hidden" name="token" value="<?php echo $token; ?>">
					</div>
					<div class="form-group">
						<label class="form-control-label" for="ecosystem">Ecosystem:</label>
						<select name="ecosystem" id="ecosystem" class="form-control">
							<option value="">---</option>
							<?php
							$ecosystems = ["General","Ethereum","Bitcoin","Bitcoin SV","Steem","EOS","Tron","Vechain","Blockstack", "Other"];
							foreach($ecosystems as $i) {
								echo "<option value='$i' ". ($i == $ecosystem? "selected": "") .">$i</option>";
							}
							?>
						</select>
					</div>
					<div class="form-group" id="stickerField">
						<label class="form-control-label" for="cryptosymbol">Ticker Symbol</label>
						<input type="text" name="cryptosymbol" id="cryptosymbol" class="form-control" minlength="2" value="<?php echo $cryptosymbol; ?>">
					</div>
					<div class="form-group">
						<label class="form-control-label" for="cryptoWebsite">Website Link</label>
						<input type="text" name="cryptoWebsite" id="cryptoWebsite" class="form-control" required="required" minlength="5" value="<?php echo $cryptoWebsite; ?>">
					</div>
					<div class="form-group">
						<label class="form-control-label" for="shortTagline">Short Tagline (max 120 char)</label>
						<textarea name="shortTagline" id="shortTagline" rows="3" class="form-control" palceholder="Tell us about this project" required="required" maxlength="120"><?php echo $shortTagline; ?></textarea>
					</div>
					<p><button class="btn btn-block btn-primary">Submit Entry</button></p>
				</div>
			</div>
		</form>
	</div>
	<?php include('../inc/footer.php'); ?>
	<?php include('../inc/endScripts.php'); ?>
	<script type="text/javascript">
		$(function() {
			$("#category").change(function() {
				if ($("#coin").is(":selected")) {
					$("#stickerField").show();
					console.log("show");
				} else {
					$("#stickerField").hide();
					console.log("hide");
				}
			}).trigger('change');
		});
	</script>
</body>
</html>
