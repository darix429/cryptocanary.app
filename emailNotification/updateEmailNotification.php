<?php
date_default_timezone_set('America/Lima');
session_start();
$success = filter_input(INPUT_GET,'success',FILTER_SANITIZE_STRING);
$notifyComment = $notifyReview = $notifySubmitNewCrypto = 0;
$notifyComment = filter_input(INPUT_POST,'notifyComment',FILTER_SANITIZE_STRING);
$notifyReview = filter_input(INPUT_POST,'notifyReview',FILTER_SANITIZE_STRING);
$email = trim(strtolower(filter_input(INPUT_POST,'email',FILTER_SANITIZE_STRING)));
$token = filter_input(INPUT_POST,'token',FILTER_SANITIZE_STRING);
$tokenVerif = md5(date('Ymd'));
if ($token != $tokenVerif) {
	header('Location: /404.html');
}
// $notifySubmitNewCrypto = filter_input(INPUT_POST,'notifySubmitNewCrypto',FILTER_SANITIZE_STRING);
if ($notifyComment == 'on') { $notifyComment = 1;} else {$notifyComment = 0;}
if ($notifyReview == 'on') { $notifyReview = 1;} else {$notifyReview = 0;}
// if ($notifySubmitNewCrypto == 'on') { $notifySubmitNewCrypto = 1;} else {$notifySubmitNewCrypto = 0;}
$notifySubmitNewCrypto = 0;
// echo $notifyComment.' - '.$notifyReview.' - '.$notifySubmitNewCrypto;
require ('../db.php');
try {
	$stmt = $conn->prepare('UPDATE `user` SET `notifyComment` = :notifyComment, `notifyReview` = :notifyReview, `notifySubmitNewCrypto` = :notifySubmitNewCrypto WHERE `email` = :email');
	$stmt->bindParam(':notifyComment', $notifyComment, PDO::PARAM_INT);
	$stmt->bindParam(':notifyReview', $notifyReview, PDO::PARAM_INT);
	$stmt->bindParam(':notifySubmitNewCrypto', $notifySubmitNewCrypto, PDO::PARAM_INT);
	$stmt->bindParam(':email', $email, PDO::PARAM_STR);
	$stmt->execute();
}
catch(PDOException $e) {
	echo 'Error: '.$e->getMessage();
}
$conn = null;
?>
<div class="alert alert-success alert-dismissible fade show" role="alert">
	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	<p>Your email notification have been saved.</p>
</div>
