<?php
$jsonCount = 0;
if (isset($row['comments'])) {
	$jsonComments = json_decode($row['comments'], true);
	$jsonCount = count($jsonComments);
}
if ($jsonCount > 0) {
	$commentText = 'View comments ('.$jsonCount.')';
	$hideText = 'Hide comments ('.$jsonCount.')';
} else {
	$commentText = 'Be the first to comment';
	$hideText = 'Hide comment';
}

?>
<div id="result<?php echo $iNumber; ?>">
	<div class="d-flex pb-2">
		<div class="pr-2"><button ic-post-to="vote.php?vote=up&review_id=<?php echo $review_id; ?>&entity_id=<?php echo $entity_id; ?>&entity_name=<?php echo $entity_name; ?>&iNumber=<?php echo $iNumber; ?>" ic-target="#result<?php echo $iNumber; ?>" ic-indicator="#spinner<?php echo $iNumber; ?>" ic-replace-target="true" class="btn btn-link text-dark"><i class="fas fa-thumbs-up"></i> <?php echo $upvote_tally; ?> Upvote</button></div>
		<div class="pr-2"><button ic-post-to="vote.php?vote=down&review_id=<?php echo $review_id; ?>&entity_id=<?php echo $entity_id; ?>&entity_name=<?php echo $entity_name; ?>&iNumber=<?php echo $iNumber; ?>" ic-target="#result<?php echo $iNumber; ?>" ic-indicator="#spinner<?php echo $iNumber; ?>" ic-replace-target="true" class="btn btn-link text-dark"><i class="fas fa-thumbs-down"></i> <?php echo $downvote_tally; ?> Downvote</button></div>
		<div class="flex-fill text-right"><button id="showCommentButton<?php echo $iNumber; ?>" class="btn btn-link text-dark" onClick="$('#addComment<?php echo $iNumber; ?>, #hideCommentButton<?php echo $iNumber; ?>').show(); $('#showCommentButton<?php echo $iNumber; ?>').hide();"><i class="fas fa-comments"></i> <?php echo $commentText; ?></button>
		<button id="hideCommentButton<?php echo $iNumber; ?>" style="display:none;" class="btn btn-link text-dark" onClick="$('#addComment<?php echo $iNumber; ?>, #hideCommentButton<?php echo $iNumber; ?>').hide(); $('#showCommentButton<?php echo $iNumber; ?>').show();"><i class="fas fa-comments"></i> <?php echo $hideText; ?></button></div>
	</div>
	<div id="addComment<?php echo $iNumber; ?>" style="display:none;" class="pb-3">
		<div id="comment<?php echo $iNumber; ?>">
			<?php
			if (isset($row['comments'])) {
				$commentid = 1;
				foreach ($jsonComments as $key => $row) {
					$commentUserId = $row['user_id'];
					$key = array_search($commentUserId, array_column($lookupTable, 'user_id'));
					$realUsername  = $lookupTable[$key]['username'];
					$dateTime = date('Y-m-d H:m:s',$row['timestamp']);
					echo '<p class="comment" id="commenting'.$commentid.'">'.$row['comment'].'<br><small><strong><a href="/user/'.urlencode($realUsername).'">'.$realUsername.'</a></strong> - '.$dateTime.'</small></p>';
						$commentid = $commentid+1;
				}
			}
			if ($sessionLoggedIN != 'LoggedIn') {
				echo '<p><a href="/login/" class="btn btn-outline-danger">Please login first to post your comment</a></p>';
			} else {
				?>
				<form ic-post-to="addComment.php" method="POST" role="form" ic-target="#result<?php echo $iNumber; ?>" ic-replace-target="true" ic-indicator="#spinner<?php echo $iNumber; ?>">
					<div class="form-group">
						<input type="text" name="comment" id="comment" class="form-control" placeholder="Write your comment here" minlength="5" required="required">
						<input type="hidden" name="review_id" value="<?php echo $review_id; ?>">
						<input type="hidden" name="iNumber" value="<?php echo $iNumber; ?>">
					</div>
					<button type="submit" class="btn btn-primary">Add your comment</button> <i id="spinner<?php echo $iNumber; ?>" class="fas fa-spinner fa-spin" style="display:none"></i>
				</form>
			<?php } ?>
		</div>
	</div>
</div>
<?php $iNumber = $iNumber+1; ?>
