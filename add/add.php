<?php
session_start();
date_default_timezone_set('America/Lima');
// $tokenVerif =  md5('cryptocanary'.date('Ymd'));
$tokenVerif =  $_SESSION['tokenSubmit'];
$token = filter_input(INPUT_POST,'token',FILTER_SANITIZE_STRING);
if ($token != $tokenVerif) {
    header('Location: /add/?error=Wrong Token');
    exit();
}
$date = date('Y-m-d H:i:s');
if (isset($_SESSION['user_id'])) {
    $user_id = $_SESSION['user_id'];
    $username = $_SESSION['username'];
} else {
    $user_id = 0;
    $username = 'Unknown';
}
$category = filter_input(INPUT_POST,'category',FILTER_SANITIZE_NUMBER_INT);
$cryptoname = filter_input(INPUT_POST,'cryptoname',FILTER_SANITIZE_STRING);
$cryptosymbol = filter_input(INPUT_POST,'cryptosymbol',FILTER_SANITIZE_STRING);
$cryptoWebsite = filter_input(INPUT_POST,'cryptoWebsite',FILTER_SANITIZE_STRING);
$shortTagline = trim(filter_input(INPUT_POST,'shortTagline',FILTER_SANITIZE_STRING));
$ecosystem = trim(filter_input(INPUT_POST,'ecosystem',FILTER_SANITIZE_STRING));
$approved = 0;
if (empty($cryptoname)) {
    header('Location: index.php?error=invalid project name');
    exit();
}
$data = $date.', '.$user_id.', '.$username.', '.$category.', '.$cryptoname.', '.$cryptosymbol.', '.$cryptoWebsite.', '.$shortTagline.', '.$ecosystem.PHP_EOL;
$emailData = $date.'<br>'.$user_id.'<br>'.$username.'<br>'.$category.'<br>'.$cryptoname.'<br>'.$ecosystem.'<br>'.$cryptosymbol.'<br>'.$cryptoWebsite.'<br>'.$shortTagline.PHP_EOL;
$outputFile = '../logs/addProject.txt';
file_put_contents($outputFile,$data,FILE_APPEND);
// Load Composer's autoloader
if (strpos(__DIR__,'/var/www/html') !== false) {
    require '/var/www/html/vendor/autoload.php';
} else {
    require '../vendor/autoload.php';
}
// send an email
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
// Load Composer's autoloader

// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);
if (strpos(__DIR__,'/var/www/html') !== false) {
    try {
        // Send Email
        //Server settings
        $mail->SMTPDebug = 0;                                       // Enable verbose debug output
        $mail->isSMTP();                                            // Set mailer to use SMTP
        $mail->Host       = 'email-smtp.us-east-1.amazonaws.com';   // Specify main and backup SMTP servers
        $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
        $mail->Username   = 'AKIAJX4KAZZ54CMFZGXA';                 // SMTP username
        $mail->Password   = 'BFSCM0P5xx0KXrGktqMQZ4fSUOPWWMyNB3+Jf128cnHM';                               // SMTP password
        $mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
        $mail->Port       = 587;                                    // TCP port to connect to

        //Recipients
        $mail->setFrom('team@cryptocanary.app', 'CryptoCanary');
        $mail->addAddress('team@cryptocanary.app', 'CryptoCanary');    // Add a recipient
        $mail->addReplyTo('team@cryptocanary.app', 'CryptoCanary');

        // Content
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = $cryptoname.' was submitted via https://cryptocanary.app/add/';
        $mail->Body    = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html lang="en">
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <style type="text/css">body{margin:0;color:#333;background-color:#10173b;font-size:16px}.container{max-width:600px;margin:auto;background-color:#fff;padding:15px}</style>
        </head>
        <body>
        <div class="container">
        <p>Someone request to add a cryptocurrency on the cryptocanary website.<br>Here are the details:</p>
        <p>'.$emailData.'</p>
        </div>
        </body>
        </html>';
        $mail->AltBody = 'Someone request to add a cryptocurrency on the cryptocanary website. Here are the details: '.$data;
        $mail->send();
    } catch(PDOException $e) {
        echo 'Error: ' . $e->getMessage();
    }
}
if ($user_id > 1) {
    // Add data into the product table
    require_once('../db.php');
    try {
        $stmt = $conn->prepare('INSERT INTO `product` (`product_name`,`short_desc`,`ecosystem`,`website`,`category`,`approved_flag`,`user_id`) VALUES (:cryptoname, :shortTagline, :ecosystem, :cryptoWebsite, :category, :approved, :user_id)');
        $stmt->bindParam(':cryptoname', $cryptoname, PDO::PARAM_STR);
        $stmt->bindParam(':shortTagline', $shortTagline, PDO::PARAM_STR);
        $stmt->bindParam(':ecosystem', $ecosystem, PDO::PARAM_STR);
        $stmt->bindParam(':cryptoWebsite', $cryptoWebsite, PDO::PARAM_STR);
        $stmt->bindParam(':category', $category, PDO::PARAM_INT);
        $stmt->bindParam(':approved', $approved, PDO::PARAM_INT);
        $stmt->bindParam(':user_id', $user_id, PDO::PARAM_INT);
        $stmt->execute();
        $stmt->debugDumpParams();
    }
    catch(PDOException $e) {
        echo 'Error: ' . $e->getMessage();
    }
    $conn = null;
    header('Location: index.php?success=1');
} else {
    header('Location: ../login/?message=You need to be logged in to finish your submission');
    $_SESSION['addProject'] = json_encode($_POST);
}
