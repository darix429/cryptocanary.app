<?php
date_default_timezone_set('America/Lima');
$output = '<?xml version="1.0" encoding="UTF-8"?><rss version="2.0"
xmlns:content="http://purl.org/rss/1.0/modules/content/"
xmlns:wfw="http://wellformedweb.org/CommentAPI/"
xmlns:dc="http://purl.org/dc/elements/1.1/"
xmlns:atom="http://www.w3.org/2005/Atom"
xmlns:sy="http://purl.org/rss/1.0/modules/syndication/"
xmlns:slash="http://purl.org/rss/1.0/modules/slash/"
>'.PHP_EOL;
$output .='<channel>
<title>CryptoCanary Latest review</title>
<atom:link href="https://cryptocanary.app/feed/reviews.xml" rel="self" type="application/rss+xml" />
<link>https://cryptocanary.app/</link>
<lastBuildDate>'.date(DATE_RFC2822).'</lastBuildDate>
<language>en-US</language>
<description>CryptoCanary consolidates community reviews and research to help beginners avoid crypto scams. Submit, rate, and review crypto projects to get started!</description>'.PHP_EOL;
require ('/var/www/html/db.php');
try {
	$stmt = $conn->prepare('SELECT review.review_id,review.user_id,review.entity_id, review.review, review.create_time,review.update_time,`user`.username, entity.entity_name, entity.symbol
	                       FROM review
	                       INNER JOIN `user`
	                       ON review.user_id = `user`.user_id
	                       INNER JOIN entity
	                       ON review.entity_id = entity.entity_id
	                       WHERE review.delete_time IS NULL
	                       ORDER BY review.review_id DESC LIMIT 50');
	$stmt->execute();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$data = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: ' . $e->getMessage();
}
$conn = null;
foreach ($data as $key => $row) {
	$review = strip_tags(trim($row['review']));
	$description = htmlspecialchars($review, ENT_NOQUOTES, "UTF-8");
	$output .='<item>'.PHP_EOL;
	$output .='<title>'.$row['entity_name'].' ('.$row['symbol'].') was reviewed by "'.$row['username'].'"</title>'.PHP_EOL;
	$output .='<link>https://cryptocanary.app/review/'.urlencode(trim($row['entity_name'])).'</link>'.PHP_EOL;
	$output .='<pubDate>'.date(DATE_RFC2822, strtotime($row['create_time'])).'</pubDate>'.PHP_EOL;
	$output .='<description>'.$description.'</description>'.PHP_EOL;
	$output .='<guid>https://cryptocanary.app/review/'.urlencode(trim($row['entity_name'])).'?id='.rand(0,1000).'</guid>'.PHP_EOL;
	$output .='</item>'.PHP_EOL;
}
$output .='</channel>'.PHP_EOL.'</rss>';
$filename = '/var/www/html/feed/reviews.xml';
file_put_contents($filename, $output);
