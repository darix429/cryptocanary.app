<?php
date_default_timezone_set('America/Lima');
$tokenVerif =  md5('cryptocanary'.date('Ymd'));
$email = strtolower(filter_input(INPUT_POST,'email',FILTER_SANITIZE_EMAIL, FILTER_FLAG_EMAIL_UNICODE));
$token = filter_input(INPUT_POST,'token',FILTER_SANITIZE_STRING);
$referer = strtolower(trim(filter_input(INPUT_POST,'referer',FILTER_SANITIZE_STRING)));
if ($token != $tokenVerif) {
	exit('<div class="alert alert-danger alert-dismissible fade show" role="alert" id="signupError"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     <h3>Holy guacamole!</h3><p>There was an error. Please try again!</p>
	     </div><script>$(document).ready(function(){$(window).scrollTop(0);});</script>');
}
if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	exit('<div class="alert alert-danger alert-dismissible fade show" role="alert" id="signupError"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	     <h3>Holy guacamole!</h3><p>Your email address: '.$email.' does not look like a valid email address.</p>
	     </div><script>$(document).ready(function(){$(window).scrollTop(0);});</script>');
}
$link = 'https://cryptocanary.app/activate/?id='.base64_encode($email);

// log this to a log file
$outputFile = '../logs/signups.txt';
$log = date(DATE_RFC2822).', '.$email.', '.$link.', '.$referer.PHP_EOL;
file_put_contents($outputFile,$log,FILE_APPEND);
if (strlen($referer) > 3) {
    $outputFile2 = '../logs/ph.txt';
    $log2 = date(DATE_RFC2822).', '.$email.', '.$link.', '.$referer.', email'.PHP_EOL;
    file_put_contents($outputFile2,$log2,FILE_APPEND);
}
require_once "../class/util.php";
require_once "../db.php";
$util = Util::getInstance();
$data = array(
    "email"=>$email,
    "template"=>"account-activation.html",
    "subject"=>"Your account on CryptoCanary has been created",
    "data"=> array(
        "link"=>$link
    )
);
if($util->sendEmail($data)) {
    echo '<div class="alert alert-success" role="alert" id="signupSuccess"><h2>Almost There!</h2><p>We just send your activation link to your email: <strong>'.$email.'</strong>.<br>Please check your inbox, (might take up to 2 minutes) and click on the activation link we have send you via email. Only verified email address can submit reviews on our platform.</p><p>Thank you</p></div><script>$(document).ready(function(){$(window).scrollTop(0);});</script>';
} else {
    echo '<div class="alert alert-danger" role="alert" id="signupError"><p>Message could not be sent.</p></div>';
}
$conn = null;
