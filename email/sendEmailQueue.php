<?php

require "/var/www/html/class/util.php";
$util = Util::getInstance();

$queueFile = "/var/www/html/email/email.queue.json";
$logFile = "/var/www/html/email/sendEmailQueue.log";

// prepare queue
$queueFileContent = file_get_contents($queueFile);
$queueFileContent = trim($queueFileContent);
$queue = substr($queueFileContent,0,( strlen($queueFileContent) - 1 ));
$queue = "[".$queue."]";
$json = json_decode($queue, 1);

$log = "==================================================". PHP_EOL;
$log .= "-------". date('Y-m-d H:i:s') . "-------". PHP_EOL;
$log .= "Count: " . count($json) . PHP_EOL;
$sent = 0; $failed = 0;

// run queue
foreach($json as $emailData) {
    if($util->sendEmail($emailData)) {
        $sent += 1;
        $log .= "Sent: " . json_encode($emailData) . PHP_EOL;
    } else {
        $failed += 1;
        $log .= "Failed: " . json_encode($emailData) . PHP_EOL;
    }
} 

// clean up queue
file_put_contents($queueFile,"");

// log result
$log .= "sent total: $sent | failed total: $failed" . PHP_EOL;
if (count($json) > 0)
file_put_contents($logFile,$log,FILE_APPEND);