<?php
session_start();
if (isset($_SESSION['user_id'])){
	if ($_SESSION['user_id'] > 1) {
		header('Location: ../browse/');
	}
}
$jsonData = filter_input(INPUT_GET,'jsonData',FILTER_SANITIZE_STRING);
$message = filter_input(INPUT_GET,'message',FILTER_SANITIZE_STRING);
$requestURI = filter_input(INPUT_GET,'requestURI',FILTER_SANITIZE_STRING);
if (isset($_SERVER['HTTP_REFERER'])) {
	$http_referer = $_SERVER['HTTP_REFERER'];
} elseif (isset($requestURI)) {
	$http_referer = 'https://cryptocanary.app'.$requestURI;
} else {
	$http_referer = '';
}
// echo $http_referer;
if (strpos($http_referer, 'cryptocanary') === false OR !filter_var($http_referer, FILTER_VALIDATE_URL)) {
	$http_referer = 'https://cryptocanary.app/browse/';
}
if (strpos($http_referer, 'cryptocanary.app/activate') !== false) {
	$http_referer = 'https://cryptocanary.app/browse/';
}
if (strpos($http_referer, 'cryptocanary.app/reset') !== false) {
	$http_referer = 'https://cryptocanary.app/browse/';
}
if (strpos($http_referer, 'cryptocanary.app/login') !== false) {
	$http_referer = 'https://cryptocanary.app/browse/';
}
if (strpos($http_referer, 'cryptocanary.app/signup') !== false) {
	$http_referer = 'https://cryptocanary.app/browse/';
}
// include ('../inc/session.php');
date_default_timezone_set('America/Lima');
$token =  md5('cryptocanary'.date('Ymd'));
$error = (int)filter_input(INPUT_GET,'error',FILTER_SANITIZE_NUMBER_INT);
$email = filter_input(INPUT_GET,'email',FILTER_SANITIZE_EMAIL);

$_SESSION['http_referer'] = $http_referer;

// social login
require_once './config.php';

// SEO purpose
$title = 'Login To Your Account | CryptoCanary';
$description = 'Login to your CryptoCanary account and review projects...';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php'); ?>
</head>
<body>
	<div id="Map">
		<?php include('../inc/nav2.php'); ?>
		<div class="container pt-5 pb-5">
			<?php
			switch ($error) {
				case '1':
					echo '<div class="alert alert-danger" role="alert"><h3 style="color:#721c24;">Error!</h3><p>The user <strong>'.$email.'</strong> does not have an account on our platform.</p><p><a href="/signup/" class="btn btn-primary">Create Your Account Here</a></p></div>';
					break;
				case '2':
					echo '<div class="alert alert-danger" role="alert"><h3 style="color:#721c24;">Error!</h3><p>Invalid Password</br>Try Again!</p></div>';
					break;
				default:
					echo '';
					break;
			}
			if (strlen($message)>1) {
				echo '<div class="alert alert-primary" role="alert">'.$message.'<br><strong>Please login.</strong></div>';
			}
			?>
			<div class="row">
				<div class="col-md-6">
					<div class="whiteBG" style="padding: 30px; border-radius: 15px;">
						<h1>Login To Your Account</h1>
						<hr>
						<form action="login.php" method="POST" role="form">
							<div class="form-group">
								<input type="email" name="email" id="email" class="form-control form-control-lg" required="required" minlength="5" placeholder="Your Email" value="<?php if (strlen($email)>3) {echo $email;} ?>">
								<input type="hidden" name="token" value="<?php echo $token; ?>">
								<input type="hidden" name="referer" value="<?php echo $http_referer; ?>">
							</div>
							<div class="form-group">
								<input type="password" name="password" id="password" class="form-control form-control-lg" required="required" minlength="6" placeholder="Your Password">
							</div>
							<button type="submit" class="btn btn-block btn-lg btn-primary">Login</button> <i id="spinner" class="fas fa-spinner fa-spin" style="display:none"></i>
						</form>
						<p class="text-center text-muted pt-3 pb-3">OR</p>
						<div class="row">
							<div class="col-12">
								<p><a href="<?php echo $fbAuthUrl ?>" class="btn btn-block btn-lg btn-outline-primary"><i class="fab fa-facebook-square"></i> Continue with Facebook</a></p>
							</div>
							<div class="col-12">
								<p><a href="<?php echo $googleAuthUrl ?>" class="btn btn-block btn-lg btn-outline-primary"><i class="fab fa-google"></i> Continue with Google</a></p>
							</div>
						</div>
					</div>
					<p class="pt-3 text-center">Having trouble logging in? <u><a href="/reset/" class="text-dark">Forgot your password?</a></u></p>
				</div>
				<div class="col-md-6 text-center">
					<h1 class="pt-3 pl-5 pr-5 pb-3">Don't Have An Account Yet?</h1>
					<p><a href="/signup" class="btn btn-lg btn-primary">Create Your Account Here</a></p>
					<p class="d-none d-md-block pt-3"><img src="/img/login_illus.svg" class="img-fluid" alt="Image"></p>
				</div>
			</div>
		</div>
	</div>
	<?php include('../inc/footer.php'); ?>
	<?php include('../inc/endScripts.php'); ?>
</body>
</html>
