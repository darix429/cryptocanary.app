<?php
date_default_timezone_set('America/Lima');
session_start();
$notifyComment = $notifyReview = $notifySubmitNewCrypto = '';
$token = md5(date('Ymd'));
$success = filter_input(INPUT_GET,'success',FILTER_SANITIZE_STRING);
$id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_STRING);
$email = base64_decode($id);
if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	header('Location: /404.html');
}
require ('../db.php');
try {
	$stmt = $conn->prepare('SELECT `notifyComment`, `notifyReview`, `notifySubmitNewCrypto` FROM `user` WHERE `email` = :email LIMIT 1');
	$stmt->bindParam(':email', $email, PDO::PARAM_STR);
	$stmt->execute();
	$count = $stmt->rowCount();
	$stmt->setFetchMode(PDO::FETCH_ASSOC);
	$data = $stmt->fetchAll();
}
catch(PDOException $e) {
	echo 'Error: '.$e->getMessage();
}
$conn = null;
if ($count <1) {
	header('Location: /404.html');
} else {
	if ($data['0']['notifyComment'] == 1) {
		$notifyComment = 'checked';
	}
	if ($data['0']['notifyReview'] == 1) {
		$notifyReview = 'checked';
	}
	if ($data['0']['notifySubmitNewCrypto'] == 1) {
		$notifySubmitNewCrypto = 'checked';
	}
	$noEmail =0;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php'); ?>
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/css/bootstrap4-toggle.min.css">
</head>
<body>
	<?php include('../inc/nav.php'); ?>
	<main class="container pt-5 pb-5">
		<h1>My Email Notification:</h1>
		<p>My Email Address: <?php echo $email; ?></p>
		<?php
		if (strlen($success)>3) {
			echo '<div class="alert alert-success alert-dismissible fade show" role="alert" id="emailNotificationSuccess"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button><h3>Congratulation!</h3><p>'.$sucess.'</p></div>';
		}
		if ($noEmail == 1) {
			echo '<h2 class="text-danger">Sorry, but this email address is not registered on our platform.</h2>';
		} else {
			?>
			<div id="result"></div>
			<form ic-post-to="updateEmailNotification.php" ic-target="#result" ic-indicator="#spinner" method="POST" role="form">
				<legend>When would you like to be notified by email?</legend>
				<div class="form-group">
					<div class="row">
						<div class="col-8"><label class="form-control-label" for="notifyComment">When someone comments under one of my reviews</label></div>
						<div class="col-4">
							<input type="checkbox" name="notifyComment" class="bootstrapToggle" <?php echo $notifyComment; ?> data-toggle="toggle" data-on="Yes" data-off="No">
							<input type="hidden" name="email" value="<?php echo $email; ?>">
							<input type="hidden" name="token" value="<?php echo $token; ?>">
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-8"><label class="form-control-label" for="notifyReview">When someone writes a new review under a project I’ve previously reviewed</label></div>
						<div class="col-4"><input type="checkbox" name="notifyReview" class="bootstrapToggle" <?php echo $notifyReview; ?> data-toggle="toggle" data-on="Yes" data-off="No"></div>
					</div>
				</div>
				<button type="submit" class="btn btn-primary">Save Changes</button> <i id="spinner" class="fa fa-spinner fa-spin" style="display:none"></i>
			</form>
		<?php } ?>
	</main>
	<?php include('../inc/footer.php'); ?>
	<?php include('../inc/endScripts.php'); ?>
	<script type="text/javascript" src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.5.0/js/bootstrap4-toggle.min.js"></script>
</body>
</html>
