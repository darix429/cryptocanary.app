<?php
session_start();
if (isset($_SESSION['user_id'])){
    if ($_SESSION['user_id'] > 1) {
        header('Location: ../browse/');
    }
}
date_default_timezone_set('America/Lima');
$token =  md5('cryptocanary'.date('Ymd'));
$id = filter_input(INPUT_GET,'id',FILTER_SANITIZE_STRING);
$email = base64_decode($id);
if (strlen($email) < 5) {
    header('Location: ../browse/');
}
// include ('../inc/session.php');
// SEO purpose
$title = 'Activate your account | CryptoCanary';
$description = 'Activate your account on CryptoCanary';
$auth_method = 'system';
if (isset($_GET['auth_method'])) {
    $auth_method = $_GET['auth_method'];
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <?php include('../inc/head.php'); ?>
</head>
<body>
    <div id="Map">
        <?php include('../inc/nav2.php'); ?>
        <div class="container pt-5 pb-5">
            <div class="row">
                <div class="col-md-8 whiteBG" style="padding: 20px; border-radius: 15px;">
                 <?php
                 if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    echo '<div class="alert alert-danger" role="alert"><h3>Error!</h3><p>You did not specify a valid email address in the activation link.</p></div>';
                } ?>
                <div id="result"></div>
                <form ic-post-to="activate.php" ic-target="#result" ic-indicator="#spinner" method="POST" role="form" ic-replace-target="true">
                    <legend>Create your account:</legend>
                    <div class="form-group">
                        <label class="form-control-label" for="email">Your Email Address:</label>
                        <input type="email" name="email" id="email" class="form-control" required="required" minlength="5" value="<?php echo $email; ?>" readonly>
                        <input type="hidden" name="token" value="<?php echo $token; ?>">
                        <small id="emailHelp" class="form-text text-muted">We will use it to reset your password if needed.</small>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label" for="name">Your Name:</label>
                        <input type="text" name="name" id="name" class="form-control" required="required" minlength="3" maxlength="50">
                        <small id="nameHelp" class="form-text text-muted">(minimum 3 characters)</small>
                    </div>
                    <div class="form-group">
                        <label class="form-control-label" for="username">Pick a Username:</label>
                        <input type="text" name="username" id="username" class="form-control text-lowercase" required="required" minlength="3" maxlength="50">
                        <small id="usernameHelp" class="form-text text-muted">The username is what will appear on the website. (minimum 3 characters, no Uppercase allowed)</small>
                    </div>

                    <?php if($auth_method == 'system') { ?>
                        <div class="form-group">
                            <label class="form-control-label" for="password">Your Password:</label>
                            <input type="text" name="password" id="password" class="form-control" required="required" minlength="6" maxlength="50">
                            <small id="passwordHelp" class="form-text text-muted">minimum 6 characters.</small>
                        </div>
                    <?php } else { ?>
                        <input type="hidden" name="password" value="">
                    <?php } ?>
                    <input type="hidden" name="auth_method" value="<?php echo $auth_method ?>">
                    <button type="submit" class="btn btn-primary"><i class="fas fa-sign-in-alt"></i> Activate your account</button> <i id="spinner" class="fa fa-spinner fa-spin" style="display:none"></i>
                </form>
                <hr>
                <p class="pt-3 pb-3">Already have an account? <a href="/login/">Login here</a></p>
            </div>
            <div class="col-md-4 pt-3">
                <h4>Problem? Need help?</h4>
                <p>Please don't hesitate to email our support team at:</p>
                <p><a href="mailto:team@cryptocanary.app?subject=Need help about activating my account!">team@cryptocanary.app</a></p>
                <p>We are happy to help you.</p>
            </div>
        </div>
    </div>
    <?php include('../inc/footer.php'); ?>
    <?php include('../inc/endScripts.php'); ?>
</body>
</html>

