<?php
session_start();
$title = 'CryptoCanary Chirps | Cryptocanary';
$description = 'Top 10 Users Leaderboard (Last 30 Days)';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php'); ?>
</head>
<body>
	<?php include('../inc/nav.php'); ?>
	<div class="greyBG">
		<div class="container">
			<?php
			include_once ('../inc/notification.php');
			?>
            <div class="row">
                <div class="col-12 text-center pt-5 pb-5">
                    <h1 class="pb-3">CryptoCanary Chirps</h1>
                    <p>Chirps are a fun a rewarding way to unlock new features and show your commitment to the crypto community. At each level, you'll unlock new features and rewards.</p>
                    <p>Earn more chirps by commenting, writing reviews or by clicking anywhere you see the icon.</p>
                </div>
            </div>
            <div class="row pt-5 pb-5">
                <div class="col-md-4">
                    <h2>Level 1</h2>
                    <h4 class="text-muted">25+ Chirps</h4>
                    <p><strong>Unlocked Features</strong></p>
                    <ul>
                        <li>Feature 1</li>
                        <li>Feature 2</li>
                        <li>Feature 3</li>
                        <li>Feature 4</li>
                    </ul>
                    <p><strong>Reward</strong></p>
                    <p>Reward description</p>
                </div>
                <div class="col-md-4">
                    <h2>Level 2</h2>
                    <h4 class="text-muted">50+ Chirps</h4>
                    <p><strong>Unlocked Features</strong></p>
                    <ul>
                        <li>Feature 1</li>
                        <li>Feature 2</li>
                        <li>Feature 3</li>
                        <li>Feature 4</li>
                    </ul>
                    <p><strong>Reward</strong></p>
                    <p>Reward description</p>
                </div>
                <div class="col-md-4">
                    <h2>Level 3</h2>
                    <h4 class="text-muted">100+ Chirps</h4>
                    <p><strong>Unlocked Features</strong></p>
                    <ul>
                        <li>Feature 1</li>
                        <li>Feature 2</li>
                        <li>Feature 3</li>
                        <li>Feature 4</li>
                    </ul>
                    <p><strong>Reward</strong></p>
                    <p>Reward description</p>
                </div>
            </div>
            <div class="row pt-5 pb-5">
                <div class="col-12 text-center">
                    <a href="/signup/" class="btn btn-lg btn-primary">Sign up and unlock 15 bonus chirps</a>
                </div>
            </div>
        </div>
    </div>
    <?php include('../inc/footer.php') ?>
    <?php include('../inc/endScripts.php') ?>

</body>
</html>
