<?php
date_default_timezone_set('America/Lima');
$tokenVerif =  md5('cryptocanary'.date('Ymd'));
$email = strtolower(filter_input(INPUT_POST,'email',FILTER_SANITIZE_EMAIL, FILTER_FLAG_EMAIL_UNICODE));
$token = filter_input(INPUT_POST,'token',FILTER_SANITIZE_STRING);
$error = filter_input(INPUT_GET,'error',FILTER_SANITIZE_STRING);
if ($token != $tokenVerif) {
	header('Location: index.php?error=There was an unexpected error.');
	exit();
}
if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
	header('Location: index.php?error=Your email address: '.$email.' does not look like a valid email address.');
	exit();
}

// check if email exists
require_once "../class/util.php";
require '../db.php';
$stmt = $conn->prepare("SELECT email FROM user WHERE email = ?");
$stmt->execute(array($email));
$email_count = $stmt->rowCount();
if ($email_count == 0) {
	header('Location: index.php?error=It appears that '.$email.' is not yet registered in our system.');
	exit();
}


$util = Util::getInstance();
$link = 'crypto'.rand(100,9999);
$emailData = array(
    "email"=>$email,
    "template"=>"reset-password.html",
    "subject"=>"Password reset for your CryptoCanary Account",
    "data"=> array(
        "resetCode"=>$link
    )
);

$util->sendEmail($emailData);

$conn = null;

$outputFile = '../logs/passwordReset.txt';
$log = date(DATE_RFC2822).', '.$email.', '.$link;
file_put_contents($outputFile,$log);
session_start();
$_SESSION['resetCode'] = $link;
$_SESSION['email'] = $email;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<?php include('../inc/head.php'); ?>
</head>
<body>
	<?php include('../inc/nav.php'); ?>
	<main class="container pt-5 pb-5">
		<div class="text-center"><img src="/img/canary-white.png" style="width: 400px;" class="img-fluid" alt="CryptoCanary logo"></div>
		<?php
		if (strlen($error)>1) {
			echo '<div class="alert alert-danger" role="alert"><h3>Error!</h3><p>'.$error.'</br>Try Again!</p></div>';
		}
		?>
		<div id="result"></div>
		<p>A password reset code was sent to your email at:<br><strong><?php echo $email; ?></strong></p>
		<form ic-post-to="password2.php" ic-target="#result" ic-indicator="#spinner" method="POST" role="form">
			<legend>Password Reset</legend>
			<div class="form-group">
				<label class="form-control-label" for="reset">Your reset Code:</label>
				<input type="text" name="reset" id="reset" class="form-control" required="required" minlength="5">
				<input type="hidden" name="email" value="<?php echo $email; ?>">
				<input type="hidden" name="token" value="<?php echo $token; ?>">
			</div>
			<div class="form-group">
				<label class="form-control-label" for="newPassword">Your new password:</label>
				<input type="text" name="newPassword" id="newPassword" class="form-control" required="required" minlength="5">
			</div>
			<button type="submit" class="btn btn-primary"><i class="fas fa-sign-in-alt"></i> Reset your password</button> <i id="spinner" class="fa fa-spinner fa-spin" style="display:none"></i>
		</form>
		<hr>
		<p>By logging in, you are agreeing to our <a href="/terms/">Terms of Service</a> and <a href="/privacy/">Privacy Policy</a>.</p>
	</main>
	<?php include('../inc/footer.php'); ?>
	<?php include('../inc/endScripts.php'); ?>
</body>
</html>
