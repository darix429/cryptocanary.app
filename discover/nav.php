<?php
if (strpos(__DIR__,'/var/www/html/') !== false) {include('/var/www/html/inc/googleTag.php');}
if (isset($_SESSION['LoggedIn'])) {
	$sessionLoggedIN = 'LoggedIn';
} else {
	$sessionLoggedIN = '';
}
if ($sessionLoggedIN == 'LoggedIn') {
	echo '<div id="loggedIn"></div>'.PHP_EOL;
}
?>
<nav class="navbar navbar-expand-lg navbar-light" style="background-color: transparent;">
	<div class="container">
		<a class="navbar-brand" href="/discover/"><img src="/img/logo.svg" class="img-fluid" alt="CryptoCanary logo" style="height: 34px;"> <small><em>discover</em></small></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarSupportedContent">
			<!-- <form action="/search.php" method="GET" role="form" class="searchWidth">
				<input id="coinList" name="search" placeholder="Find A Great Project" style="text-align: center; border-radius: 5px; width: 100%; padding-left: 30px;font-family: 'Inter', Helvetica, sans-serif !important;font-style: normal !important;" spellcheck="false">
			</form> -->
			<ul class="navbar-nav ml-auto">
				<li class="nav-item"><a href="https://blog.cryptocanary.app/" class="nav-link" style="color:#362e2c;">Blog</a></li>
				<?php
				if ($sessionLoggedIN == 'LoggedIn') {
					$randomAvatar = '/images/avatars/random/avatar'.rand(1,19).'.png';
					if (!isset($_SESSION['avatar'])) {
						$avatar = $randomAvatar;
					} else {
						$avatar = '/images/avatars/'.$_SESSION['user_id'].'/'.$_SESSION['avatar'];
					}
					if(strpos($_SESSION['avatar'], 'random') !== false OR strlen($_SESSION['avatar']) <1 ) { $avatar = $randomAvatar;}
					echo ' <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: #362e2c;">
					<img src="'.$avatar.'" class="img-fluid rounded" alt="Avatar" style="height: 30px;"> '.$_SESSION['username'].'</a>';
					echo '<div class="dropdown-menu" aria-labelledby="navbarDropdown">';
					echo '<a class="dropdown-item" style="color: #362e2c;" href="/profile/">My Profile</a>';
					echo '<a class="dropdown-item" style="color: #362e2c;" href="/profile/#socialMedia">My Chirps <span class="badge badge-secondary">'.$_SESSION['chirps'].'</span></a>';
					echo '<div class="dropdown-divider"></div>';
					echo '<a class="dropdown-item" style="color: #362e2c;" href="/logout.php"><i class="fas fa-power-off"></i> Logout</a>';
					echo '</div>
					</li>';
				} else {
					echo '<li class="nav-item"><a class="nav-link" style="color: #362e2c;" href="/signup/">Join</a></li>';
					echo '<li class="nav-item"><a class="nav-link btn btn-primary text-white" href="/login/">Login</a></li>';
				}
				?>
			</ul>
		</div>
	</div>
</nav>
