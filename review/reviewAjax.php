<?php
date_default_timezone_set('America/Lima');
$token = md5('cryptocanary'.date('Ymd'));
if (isset($_SESSION['LoggedIn'])) {
	$sessionLoggedIN = 'LoggedIn';
} else {
	$sessionLoggedIN = '';
}
if ($sessionLoggedIN == 'LoggedIn') {
	$user_id = $_SESSION['user_id'];
	if (strpos(__DIR__,'/Applications/MAMP/') !== false) {
		require('/Applications/MAMP/htdocs/cryptocanary/db.php');
	} else {
		require('/var/www/html/db.php');
	}
	try {
		$stmt2 = $conn->prepare('SELECT review_id, user_id, entity_id, review, review_title, rating FROM `review` WHERE entity_id = :entity_id AND `user_id` = :user_id ORDER BY review_id DESC LIMIT 1');
		// Bind Parameter
		$stmt2->bindParam(':entity_id', $entity_id, PDO::PARAM_INT);
		$stmt2->bindParam(':user_id', $user_id, PDO::PARAM_INT);
		$stmt2->execute();
		$stmt2->setFetchMode(PDO::FETCH_ASSOC);
		$data2 = $stmt2->fetchAll();
	}
	catch(PDOException $e) {
		echo 'Error: ' . $e->getMessage();
	}
	if (!empty($data2)) {
		$rating = $data2[0]['rating'];
		$userReview= $data2[0]['review'];
		$review_id = $data2[0]['review_id'];
		$updateCode = '<input type="hidden" name="updateCode" value="'.$review_id.'">';
		$buttonText = 'Change your Review';
	} else {
		$userReview = $updateCode = '';
		$buttonText = 'Add Review';
	}
	?>
	<div class="row" id="addReview">
		<div class="col-12">
			<div id="result"></div>
			<form ic-post-to="addReview.php" method="POST" role="form" ic-target="#result" ic-indicator="#spinner">
				<p><i>How trustworthy is <?php echo $entity_name; ?></i></p>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="rating" id="shadiness1" value="4" required="required" <?php if ($rating == 4) {echo 'checked="checked"';}?>>
					<label class="form-check-label" for="inlineRadio1" style="color: #c60812;">Very Shady</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="rating" id="shadiness2" value="3" required="required" <?php if ($rating == 3) {echo 'checked="checked"';}?>>
					<label class="form-check-label" for="inlineRadio2" style="color: #ca6f2a;">Lean Shady</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="rating" id="shadiness3" value="2" required="required" <?php if ($rating == 2) {echo 'checked="checked"';}?>>
					<label class="form-check-label" for="inlineRadio3" style="color: #b3951f">Neutral</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="rating" id="shadiness4" value="1" required="required" <?php if ($rating == 1) {echo 'checked="checked"';}?>>
					<label class="form-check-label" for="inlineRadio2" style="color: #97c626;">Lean Legit</label>
				</div>
				<div class="form-check form-check-inline">
					<input class="form-check-input" type="radio" name="rating" id="shadiness5" value="0" required="required" <?php if ($rating == 0) {echo 'checked="checked"';}?>>
					<label class="form-check-label" for="inlineRadio3" style="color: #1ec622;">Very Legit</label>
				</div>
				<p class="pt-3"><i>Please explain your rating in detail</i></p>
				<div class="form-group">
					<textarea class="form-control" name="review" id="review" rows="3" required="required" minlength="160" placeholder="Write your review here..."><?php echo $userReview; ?></textarea>
					<small id="reviewlHelp" class="form-text text-muted">Minimum 160 characters.</small>
					<input type="hidden" name="entity_id" value="<?php echo $id; ?>">
					<input type="hidden" name="token" value="<?php echo $token; ?>">
					<?php echo $updateCode; ?>
				</div>
				<button type="submit" class="btn btn-primary"><i class="fas fa-plus"></i> <?php echo $buttonText; ?></button> <i id="spinner" class="fas fa-spinner fa-spin" style="display:none"></i>
			</form>
		</div>
	</div>
	<?php
}
else {
	echo '<div class="row" id="addReview">
	<div class="col-12">
	<p>Add your review:</p>
	<p><a href="/login/" class="btn btn-primary">Please login to add your review.</a></p>
	</div>
	</div>';
}
